import numpy as np
import torch


class ReplayBuffer(object):
    """Buffer to store environment transitions."""
    def __init__(self, obs_shape, action_shape, capacity, device, nSubtasks = 1):
        self.capacity = capacity
        self.device = device

        # the proprioceptive obs is stored as float32, pixels obs as uint8
        obs_dtype = np.float32 if len(obs_shape) == 1 else np.uint8

        self.obses = np.empty((capacity, *obs_shape), dtype=obs_dtype)
        self.next_obses = np.empty((capacity, *obs_shape), dtype=obs_dtype)
        self.actions = np.empty((capacity, *action_shape), dtype=np.float32)
        self.rewards = np.empty((capacity, 1), dtype=np.float32)
        self.not_dones = np.empty((capacity, 1), dtype=np.float32)
        self.not_dones_no_max = np.empty((capacity, 1), dtype=np.float32)

        self.env_infos = np.array([[{}] for _ in range(capacity)])
        self.agent_infos = np.array([[{}] for _ in range(capacity)])

        self.idx = 0
        self.last_save = 0
        self.full = False

        if nSubtasks > 1:
            self.subtasks = [ReplayBuffer(obs_shape, action_shape, capacity, device) for _ in range(nSubtasks)]
            self.latent_vector = np.empty((capacity, nSubtasks),dtype=np.float32)
        else:
            self.subtasks = 1
            self.latent_vector = None

    def __len__(self):
        return self.capacity if self.full else self.idx

    def add(self, obs, action, reward, next_obs, done, done_no_max, env_infos=None, agent_infos = None):
        np.copyto(self.obses[self.idx], obs)
        np.copyto(self.actions[self.idx], action)
        np.copyto(self.rewards[self.idx], reward)
        np.copyto(self.next_obses[self.idx], next_obs)
        np.copyto(self.not_dones[self.idx], not done)
        np.copyto(self.not_dones_no_max[self.idx], not done_no_max)
        np.copyto(self.env_infos[self.idx], np.array(env_infos))
        np.copyto(self.agent_infos[self.idx], np.array(agent_infos))

        self.idx = (self.idx + 1) % self.capacity
        self.full = self.full or self.idx == 0

        if env_infos is not None and self.subtasks != 1:
            self.subtasks[env_infos['subtask']].add(obs, action, reward, next_obs, done, done_no_max, env_infos,agent_infos)

    def sample(self, batch_size, return_env_info=False, return_agent_info=False):
        idxs = np.random.randint(0,
                                 self.capacity if self.full else self.idx,
                                 size=batch_size)

        obses = torch.as_tensor(self.obses[idxs], device=self.device).float()
        actions = torch.as_tensor(self.actions[idxs], device=self.device)
        rewards = torch.as_tensor(self.rewards[idxs], device=self.device)
        next_obses = torch.as_tensor(self.next_obses[idxs],
                                     device=self.device).float()
        not_dones = torch.as_tensor(self.not_dones[idxs], device=self.device)
        not_dones_no_max = torch.as_tensor(self.not_dones_no_max[idxs],
                                           device=self.device)

        
        if return_agent_info:
            agent_infos = self.agent_infos[idxs].squeeze(1)

        if not return_env_info:
            if return_agent_info:
                return obses, actions, rewards, next_obses, not_dones, not_dones_no_max, agent_infos
            else:
                return obses, actions, rewards, next_obses, not_dones, not_dones_no_max
        else:
            env_infos = self.env_infos[idxs].squeeze(1)
            if return_agent_info:
                return obses, actions, rewards, next_obses, not_dones, not_dones_no_max, env_infos, agent_infos
            else:
                return obses, actions, rewards, next_obses, not_dones, not_dones_no_max, env_infos
            

    
    def subtask_sample(self, batch_size, subtask_id, return_env_info=False, return_agent_info=False):
        if self.subtasks != 1:
            return self.subtasks[subtask_id].sample(batch_size, return_env_info, return_agent_info)
        else:
            return self.sample(batch_size, return_env_info)


    def check_subtask_data(self, subtask_id):
        return self.subtasks[subtask_id].idx if not self.subtasks[subtask_id].full else self.capacity
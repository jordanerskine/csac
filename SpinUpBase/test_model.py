#!/usr/bin/env python3
from cgitb import small
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import copy
import math
import os
import sys
import time
import pickle as pkl
import yaml
import json
import cv2
from omegaconf.dictconfig import DictConfig
from omegaconf.listconfig import ListConfig



from video import VideoRecorder
from logger import Logger
from replay_buffer import ReplayBuffer
import utils

import dmc2gym
import hydra

import matplotlib.pyplot as plt

def plotResults(results):
    fig = plt.figure()
    x = np.linspace(0, len(results))
    plt.plot(x,results)
    plt.show()

def getFillBar(value, minimum = 0, maximum = 1, length = 20):
    filled = int(max(0,min(1,(value-minimum)/(maximum-minimum)))*length)
    st = "[" + filled*"=" + (length-filled)*" " + "]"
    return st


def make_custom_env(cfg):
    env = hydra.utils.instantiate(cfg.env)
    env._max_episode_steps = cfg.env.params._max_episode_steps
    return env


def make_env(cfg):
    """Helper function to create dm_control environment"""
    if cfg.env == 'ball_in_cup_catch':
        domain_name = 'ball_in_cup'
        task_name = 'catch'
    else:
        domain_name = cfg.env["class"].split('_')[0]
        task_name = '_'.join(cfg.env["class"].split('_')[1:])

    env = dmc2gym.make(domain_name=domain_name,
                       task_name=task_name,
                       seed=cfg.seed,
                       visualize_reward=True)
    # env.seed(cfg.seed)
    assert env.action_space.low.min() >= -1
    assert env.action_space.high.max() <= 1

    return env

def updateCfg(cfg,updates):
    for key in updates:
        value = updates[key]
        smallCFG = copy.deepcopy(cfg)
        for k in key.split("."):
            smallCFG = smallCFG[k]
        if smallCFG is None:
            try:
                value = float(value)
            except:
                value = str(value)
        else:
            value = type(smallCFG)(value)
        cfg.update(key,value)

class Workspace(object):
    def __init__(self, path, load='latest', progression = False, render = False,kwargUpdates = None):

        with open(f'{path}/config.yaml') as cfg_file:
            cfg = DictConfig(yaml.load(cfg_file,yaml.FullLoader))
        self.device = torch.device(cfg.device)
        if kwargUpdates is not None:
            updateCfg(cfg,kwargUpdates)

        self.progression = progression
        self.path = path

        cfg.env.params.headless= (not render)
        self.render = render

        
        if 'source' in cfg and cfg['source'] == "mujoco":
            self.env = make_env(cfg)
        else:
            self.env = make_custom_env(cfg)

        self.agent = hydra.utils.instantiate(cfg.agent)

        if load == 'latest':
            load = max([int(x) for x in os.listdir(f'{path}/model')])

        self.load_model(load)

    def load_model(self, id):
        print(f"----\nLoading model from iteration: {id}")
        if not os.path.exists(f'{self.path}/model/{id}'):
            print("Load step does not exist. Available steps are:")
            for x in os.listdir(f'{self.path}/model'):
                print(x)
            print("Will now run a vanilla experiment")
        else:
            self.agent.load_model(f'{self.path}/model/{id}')

    def evaluate_critic(self, obs, act, critic = None):
        obs = torch.Tensor(obs).unsqueeze(0).cuda()
        act = torch.Tensor(act).unsqueeze(0).cuda()
        try:
            out = self.agent.critic(obs, act)
        except:
            if critic is not None:
                out = self.agent.agents[critic].critic(obs, act)
            else:
                out = self.agent.agents[0].critic(obs, act)
        # print(out)


    def run(self,sample = False, episodes=5, print_progress = False, trajectoryRender = False):
        if self.progression:
            saves = [int(x) for x in os.listdir(f'{self.path}/model')]
            last_id = -1
            for progress in np.linspace(0,100,11):
                id = np.percentile(saves,progress,interpolation = "higher")
                if last_id != id:
                    self.load_model(id)
                    print(f"{int(id/max(saves)*100)}% through training")
                    self.run_eps(sample,episodes,print_progress,trajectoryRender)
                last_id = copy.deepcopy(id)
        else:
            
            self.run_eps(sample,episodes,print_progress,trajectoryRender)

    def run_eps(self, sample, episodes, print_progress, trajectoryRender):

        for ep in range(int(episodes)):
            obs = self.env.reset()
            self.agent.reset(eval = True)
            done = False
            episode_reward = 0
            step = 0
            env_info = None
            success = []
            discrim = []
            while not done:
                with utils.eval_mode(self.agent):
                    action = self.agent.act(obs, sample=sample, env_info=env_info)
                try:
                    discrim.append(self.agent.discrim(torch.Tensor(np.concatenate([obs,action])).cuda()).cpu().detach().numpy())
                except:
                    pass
                obs, reward, done, env_info = self.env.step(action)
                # if self.render:
                #     time.sleep(0.02)
                if 'overallReward' in env_info:
                    episode_reward += env_info['overallReward']
                    success.append(env_info['success'])
                    subtask = env_info['subtask']
                else:
                    episode_reward += reward
                    success.append(0)
                    subtask = 0
                if not trajectoryRender and self.render:
                    self.env.render()
                step += 1
                if print_progress:
                    print(f"\nStep: {step}")
                    print(f"Action: {action}")
                    out = self.evaluate_critic(obs,action)
                    # print(f"Critic: {out.cpu().detach().numpy()}")
                    print(f"Subtask: {subtask}")
                    d = "NA" if len(discrim) == 0 else discrim[-1]
                    print(f"Discrim: {getFillBar(d)} : {d}")
            if len(discrim) == 0:
                print(f'Episode {ep} \n\
                        Overall Reward: {episode_reward}\n\
                        Final subtask: {subtask}\n\
                        Success Rate: {np.mean(success)}')
            else:
                print(f'Episode {ep} \n\
                        Overall Reward: {episode_reward}\n\
                        Final subtask: {subtask}\n\
                        Success Rate: {np.mean(success)}\n\
                        Discrimination Rate: {np.mean(discrim)}')
        if trajectoryRender:
            img = self.env.trajectoryRender(False)
            cv2.namedWindow("Trajectory Render", cv2.WINDOW_NORMAL)
            cv2.imshow("Trajectory Render", img)
            cv2.waitKey(0)

        


def arg(tag):
    if tag in sys.argv:
        val = sys.argv[sys.argv.index(tag)+1]
    else:
        val = None
    return val

def getArgs(tags):
    arguments = {}
    for tag in tags:
        if arg(tag) is not None:
            arguments[tag[2:]] = arg(tag)
    return arguments

# @hydra.main(config_path=('{}/config.yaml'.format()), strict=True)
def main():
    workArgs = getArgs(['--path','--load', '--progression', '--render'])
    workArgs['kwargUpdates'] = {a.split("=")[0]:a.split("=")[1] for a in sys.argv if "=" in a}
    workspace = Workspace(**workArgs)
    runArgs = getArgs(['--sample','--episodes','--print_progress', '--trajectoryRender'])
    workspace.run(**runArgs)


if __name__ == '__main__':
    main()
import numpy as np
import torch


class PandaCanExpert:
    def __init__(self,noise = 0.05):
        self.noise = noise

    def get_actions(self,o):
        device = o.device
        actions = [self.get_action(obs) for obs in o.cpu()]
        actions = torch.tensor(actions,device = device,dtype = o.dtype)
        return actions

    def get_action(self,observation):

        ee_pos = observation[:3]
        ee_vel = observation[3:6]
        gripper = observation[6]
        object_pos = observation[7:10]
        object_rotation = observation[10:13]
        object_velocity = observation[13:16]
        object_angular_velocity = observation[16:]
        # if np.linalg.norm(ee_pos-object_pos) > 0.1:
        ee2objDist = np.linalg.norm(np.array(ee_pos)-np.array(object_pos))
        goalPos = np.array([0.0, 0.3, 0.1])
        obj2goalDist = np.linalg.norm(np.array(object_pos)-np.array(goalPos))
        if obj2goalDist > 0.05: # If object not at goal
            if ee2objDist > 0.02: # if end effector not at object
                eeAct = np.array(object_pos)-np.array(ee_pos)
                eeAct[2] /= 2
                gripperAct = np.array([1.0])
            else:
                gripperAct = np.array([-1.0])
                eeAct = np.array(goalPos)-np.array(ee_pos)
        else:
            eeAct = (np.array(object_pos)+np.array([0,0,0.2]))-np.array(ee_pos)
            gripperAct = np.array([1.0])
        action = np.concatenate([eeAct,gripperAct])
        action += np.random.normal(0,self.noise,size=len(action))
        return np.clip(action,-1,1)
        

        


if __name__ == "__main__":

    from gailSandbox.envs.pandaCan.pandaCan import PandaCan
    env = PandaCan(controlType = "ee",render = 1)
    expert = PandaCanExpert()
    for _ in range(20):
        o = env.reset()
        for _ in range(300):
            a = expert.get_action(o)
            o,_,_,_ = env.step(a)

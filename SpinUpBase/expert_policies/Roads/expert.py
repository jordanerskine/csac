
import numpy as np
import torch
import math

def angleDiff(a1,a2):
    a = a1-a2
    a = (a + math.pi) % (math.pi*2) - math.pi
    return a

class RoadExpert:
    def __init__(self,noise = 0.05):
        self.noise = noise
        self.reversing = 0
        self.turning = False
        self.lastPose = None

    def add_noise(self,value):
        return np.clip(np.random.normal(value,self.noise),-1,1)
  
        
    def get_actions(self,o):
        device = o.device
        actions = [self.get_action(obs) for obs in o.cpu()]
        actions = torch.tensor(actions,device = device,dtype = o.dtype)
        return actions

    def get_action(self,observation):
        robotAngle = observation[-1]
        robotPose = observation[-3:-1]
        if self.lastPose is None or np.linalg.norm(robotPose[:2]-self.lastPose) > 0.05:
            self.turning = False
            self.reversing = 0
        self.lastPose = robotPose[:2]
        laser = observation[:-3]
        speed = 0.5
        if laser[int(len(laser)/2)] < 0.1:
            self.reversing = 25
        if robotPose[1] > 0:
            angle = 3*angleDiff(0,robotAngle)
            self.turning = False
        else:
            if (laser[int(len(laser)/2)] < 0.4 and abs(angleDiff(robotAngle,math.pi)) < 0.03) or self.turning:
                self.turning = True
                angle = 3*angleDiff(math.pi/2,robotAngle)
                speed = 0.3
            else:
                angle = 3*angleDiff(math.pi,robotAngle)

        angle = np.clip(angle,-1,1)

        if self.reversing > 0:
            self.reversing -= 1
            speed = -1
            angle = 0
        
        speed = self.add_noise(speed)
        angle = self.add_noise(angle)

        return np.array([speed,angle])
        
        

        


if __name__ == "__main__":

    from ParallelRoads import Roads
    env = Roads(roadLength = 2)
    expert = RoadExpert()
    for _ in range(20):
        o = env.reset()
        for _ in range(200):
            a = expert.get_action(o)
            try:
                env.render()
            except:
                pass
            o,_,_,_ = env.step(a)

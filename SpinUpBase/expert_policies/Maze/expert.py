from tabnanny import check
import numpy as np
import torch
import math

def angleDiff(a1,a2):
    a = a1-a2
    a = (a + math.pi) % (math.pi*2) - math.pi
    return a

class MazeExpert:
    def __init__(self,noise = 0.05):
        self.noise = noise
        self.reversing = 0
        self.nChecks = None

    def nextCheckpoint(self,pose):
        if self.nChecks is None:
            self.nChecks = math.ceil(pose[0]*-2)
        checkpoints = []
        n = self.nChecks
        for i in range(n):
            checkpoints.append([i-n/2+0.5,0.85*(-1+(i%2)*2)])
        for ch in checkpoints:
            if pose[0] < ch[0]:
                return ch
        return [n-1,0]
        

    def get_actions(self,o):
        device = o.device
        actions = [self.get_action(obs) for obs in o.cpu()]
        actions = torch.tensor(actions,device = device,dtype = o.dtype)
        return actions

    def get_action(self,observation):
        robotAngle = observation[-1]
        robotPose = observation[-3:-1]
        check = self.nextCheckpoint(robotPose)
        checkAngle = math.atan2(check[1]-robotPose[1],check[0]-robotPose[0])
        ad= angleDiff(checkAngle,robotAngle)
        speed = 0.2 if abs(ad) > math.pi/6 else 0.6
        forwardVect = observation[int((len(observation)-3)/2)]
        if forwardVect < 0.12:
            self.reversing = 15
        angle = np.clip(ad/(math.pi/6),-1,1)

        if self.reversing > 0:
            self.reversing -= 1
            speed = -1
            angle = 0
        return np.array([speed,angle])
        
        

        


if __name__ == "__main__":

    from CheckpointMazeEnv import CheckpointMaze
    env = CheckpointMaze(checkpoints = 2)
    expert = MazeExpert()
    for _ in range(20):
        o = env.reset()
        for _ in range(500):
            a = expert.get_action(o)
            try:
                env.render()
            except:
                pass
            o,_,_,_ = env.step(a)

from torch.utils.tensorboard import SummaryWriter
from collections import defaultdict
import json
import os
import csv
import shutil
import torch
import numpy as np
from termcolor import colored
import re
from copy import deepcopy as copy

import wandb

COMMON_TRAIN_FORMAT = [
    ('full_time', 'T', 'full_duration'),
    ('episode', 'E', 'int'),
    ('step', 'S', 'int'),
    ('length','L','int'),
    ('episode_reward', 'R', 'float'),
    ('duration', 'D', 'time') 
]

COMMON_EVAL_FORMAT = [
    ('episode', 'E', 'int'),
    ('step', 'S', 'int'),
    ('episode_reward', 'R', 'float'),
    ('duration', 'D', 'time')
]


AGENT_TRAIN_FORMAT = {
    'sac': [
        ('batch_reward', 'BR', 'float'),
        ('actor_loss', 'ALOSS', 'float'),
        ('critic_loss', 'CLOSS', 'float'),
        ('alpha_loss', 'TLOSS', 'float'),
        ('alpha_value', 'TVAL', 'float'),
        ('actor_entropy', 'AENT', 'float')
    ]

}

TIMING_FORMAT = [
    ('episode', 'EP', 'time'),
    ('action', 'A', 'time'),
    ('env_step', 'ES', 'time'),
    ('train','T', 'time')
]


class AverageMeter(object):
    def __init__(self):
        self._sum = 0
        self._count = 0

    def update(self, value, n=1):
        self._sum += value
        self._count += n

    def value(self):
        return self._sum / max(1, self._count)


class MetersGroup(object):
    def __init__(self, file_name, formating, suppress_console=False,resume = False, max_duration = None):
        if not resume:
            self._csv_file_name = self._prepare_file(file_name, 'csv')
        else:
            self._csv_file_name = f'{file_name}.csv'
        self._formating = formating
        self._meters = defaultdict(AverageMeter)
        self._csv_file = open(self._csv_file_name, 'a')
        self._csv_writer = None
        self.suppress_console = suppress_console
        self.resume = resume
        self.max_duration = max_duration
        if max_duration is not None:
            value = max_duration
            t_type = 's'
            if value > (60*60*24):
                value /= (60*60*24)
                t_type = 'd'
            elif value > (60*60):
                value /= (60*60)
                t_type = 'h'
            elif value >60:
                value /= 60
                t_type = 'm'
            self.max_duration = f'{value}{t_type}'


    def _prepare_file(self, prefix, suffix):
        file_name = f'{prefix}.{suffix}'
        if os.path.exists(file_name):
            os.remove(file_name)
        return file_name

    def log(self, key, value, n=1):
        self._meters[key].update(value, n)

    def _prime_meters(self):
        data = dict()
        for key, meter in self._meters.items():
            if key.startswith('train'):
                key = key[len('train') + 1:]
            elif key.startswith('env_info'):
                key = key[len('env_info') + 1:]
            elif key.startswith('timing'):
                key = key[len('timing') + 1:]
            else:
                key = key[len('eval') + 1:]
            key = key.replace('/', '_')
            data[key] = meter.value()
        return data

    def _dump_to_csv(self, data):
        if self._csv_writer is None:
            self._csv_writer = csv.DictWriter(self._csv_file,
                                              fieldnames=sorted(data.keys()),
                                              restval=0.0)
            if not self.resume:
                self._csv_writer.writeheader()
        self._csv_writer.writerow(data)
        self._csv_file.flush()

    def _format(self, key, value, ty):
        if ty == 'int':
            value = int(value)
            return f'{key}: {value}'
        elif ty == 'float':
            return f'{key}: {value:.04f}'
        elif ty == 'time':
            return f'{key}: {value:04.1f} s'
        elif ty == 'full_duration':
            t_type = 's'
            if value > (60*60*24):
                value /= (60*60*24)
                t_type = 'd'
            elif value > (60*60):
                value /= (60*60)
                t_type = 'h'
            elif value >60:
                value /= 60
                t_type = 'm'
            if self.max_duration is None:
                return f'{key}: {value:04.2f}{t_type}'
            else:
                return f'{key}: {value:04.2f}{t_type}/{self.max_duration}'
        else:
            raise f'invalid format type: {ty}'

    def _dump_to_console(self, data, prefix):
        if not self.suppress_console:
            prefix = colored(prefix, 'yellow' if prefix == 'train' else 'green')
            pieces = [f'| {prefix: <14}']
            for key, disp_key, ty in self._formating:
                value = data.get(key, 0)
                pieces.append(self._format(disp_key, value, ty))
            print(' | '.join(pieces))

    def dump(self, step, prefix, save=True):
        if len(self._meters) == 0:
            return
        if save:
            data = self._prime_meters()
            data['step'] = step
            self._dump_to_csv(data)
            self._dump_to_console(data, prefix)
        self._meters.clear()


class Logger(object):
    def __init__(self,
                 log_dir,
                 save_tb=False,
                 log_frequency=10000,
                 agent='sac',
                 resume = False,
                 max_duration = None):

        self.wandbInit = False
        self._log_dir = log_dir
        self._log_frequency = log_frequency
        self.agent = agent
        if save_tb:
            tb_dir = os.path.join(log_dir, 'tb')
            if os.path.exists(tb_dir):
                try:
                    shutil.rmtree(tb_dir)
                except:
                    print("logger.py warning: Unable to remove tb directory")
                    pass
            self._sw = SummaryWriter(tb_dir)
        else:
            self._sw = None
        # each agent has specific output format for training
        if agent in AGENT_TRAIN_FORMAT:
            train_format = COMMON_TRAIN_FORMAT + AGENT_TRAIN_FORMAT[agent]
        else:
            train_format = COMMON_TRAIN_FORMAT
        self.log_dir = log_dir
        self._train_mg = MetersGroup(os.path.join(log_dir, 'train'),
                                     formating=train_format, resume=resume, max_duration=max_duration)
        self._eval_mg = MetersGroup(os.path.join(log_dir, 'eval'),
                                    formating=COMMON_EVAL_FORMAT, resume=resume)
        self._time_mg = MetersGroup(os.path.join(log_dir, 'timing'),
                                    formating=TIMING_FORMAT, suppress_console=True, resume=resume)
        self._env_mg = None
        self.dumped = False
        self.resume = resume

    def init_wandb(self,cfg, project):
        wandb.init(config = cfg,project = project, tags = [self.agent])
        self.wandbInit = True

    def _get_env_info_mg(self, env_info):
        if self._env_mg is None:
            form = []
            for tag in env_info:
                form.append((f'{tag}_mean', 'env', 'float'))
                form.append((f'{tag}_max', 'env', 'float'))
                form.append((f'{tag}_min', 'env', 'float'))
            self._env_mg = MetersGroup(os.path.join(self.log_dir, 'env_info'),
                                        formating=form, suppress_console=True,resume = self.resume)
            


    def _should_log(self, step, log_frequency):
        log_frequency = log_frequency or self._log_frequency
        return step % log_frequency == 0

    def _try_sw_log(self, key, value, step):
        if self._sw is not None:
            self._sw.add_scalar(key, value, step)

    def _try_sw_log_video(self, key, frames, step):
        if self._sw is not None:
            frames = torch.from_numpy(np.array(frames))
            frames = frames.unsqueeze(0)
            self._sw.add_video(key, frames, step, fps=30)

    def _try_sw_log_histogram(self, key, histogram, step):
        if self._sw is not None:
            self._sw.add_histogram(key, histogram, step)

    def log(self, key, value, step, n=1, log_frequency=1):
        if self.wandbInit:
            wandb.log({key:value},step=step)
        if not self._should_log(step, log_frequency):
            return
        assert key.startswith('train') or key.startswith('eval') or key.startswith('env_info') or key.startswith('timing')
        if type(value) == torch.Tensor:
            value = value.item()
        self._try_sw_log(key, value / n, step)
        if key.startswith('train'):
            mg = self._train_mg
        elif key.startswith('env_info'):
            mg = self._env_mg
        elif key.startswith('timing'):
            mg = self._time_mg
        else:
            mg = self._eval_mg
        mg.log(key, value, n)

    def log_param(self, key, param, step, log_frequency=None):
        if not self._should_log(step, log_frequency):
            return
        self.log_histogram(key + '_w', param.weight.data, step)
        if hasattr(param.weight, 'grad') and param.weight.grad is not None:
            self.log_histogram(key + '_w_g', param.weight.grad.data, step)
        if hasattr(param, 'bias') and hasattr(param.bias, 'data'):
            self.log_histogram(key + '_b', param.bias.data, step)
            if hasattr(param.bias, 'grad') and param.bias.grad is not None:
                self.log_histogram(key + '_b_g', param.bias.grad.data, step)

    def log_video(self, key, frames, step, log_frequency=None):
        if not self._should_log(step, log_frequency):
            return
        assert key.startswith('train') or key.startswith('eval')
        self._try_sw_log_video(key, frames, step)

    def log_histogram(self, key, histogram, step, log_frequency=None):
        if not self._should_log(step, log_frequency):
            return
        assert key.startswith('train') or key.startswith('eval')
        self._try_sw_log_histogram(key, histogram, step)

    def dump(self, step, save=True, ty=None):
        if save == True:
            self.dumped = True
        if ty is None:
            self._train_mg.dump(step, 'train', save)
            self._eval_mg.dump(step, 'eval', save)
            self._time_mg.dump(step, 'timing', save)
            if self._env_mg is not None:
                self._env_mg.dump(step, 'env_info', save)
        elif ty == 'eval':
            self._eval_mg.dump(step, 'eval', save)
        elif ty == 'train':
            self._train_mg.dump(step, 'train', save)
        elif ty == 'env_info' and self._env_mg is not None:
            self._env_mg.dump(step, 'env_info', save)
        elif ty == 'timing':
            self._time_mg.dump(step, 'timing', save)
        else:
            raise f'invalid log type: {ty}'

    def populate_with_n_subtasks(self, nSubtasks):
        mgs = [self._train_mg, self._eval_mg]
        if self._env_mg is not None:
            mgs.append(self._env_mg)
        for mg in mgs:
            names = copy(mg._meters)
            for name in names:
                for subtask in range(nSubtasks):
                    newName = re.sub('\d',f'{subtask}',name)
                    if newName not in mg._meters:
                        mg.log(newName, 0)

    def save_model(self, name, state_dict, step):
        loc = f'{self.log_dir}/model/{step}/{name}.pt'
        dirs = loc[:loc.rfind('/')]
        os.makedirs(dirs, exist_ok=True)
        torch.save(state_dict, loc)

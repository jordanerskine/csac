from tracemalloc import start
import numpy as np
import torch
from torch import nn
from torch import distributions as pyd
import torch.nn.functional as F
import gym
import os
from collections import deque
import random
import math
import types
from importlib.metadata import version

# import dmc2gym


def make_env(cfg):
    """Helper function to create dm_control environment"""
    if cfg.env == 'ball_in_cup_catch':
        domain_name = 'ball_in_cup'
        task_name = 'catch'
    else:
        domain_name = cfg.env.split('_')[0]
        task_name = '_'.join(cfg.env.split('_')[1:])

    env = dmc2gym.make(domain_name=domain_name,
                       task_name=task_name,
                       seed=cfg.seed,
                       visualize_reward=True)
    env.seed(cfg.seed)
    assert env.action_space.low.min() >= -1
    assert env.action_space.high.max() <= 1

    return env


class eval_mode(object):
    def __init__(self, *models):
        self.models = models

    def __enter__(self):
        self.prev_states = []
        for model in self.models:
            self.prev_states.append(model.training)
            model.train(False)

    def __exit__(self, *args):
        for model, state in zip(self.models, self.prev_states):
            model.train(state)
        return False


class train_mode(object):
    def __init__(self, *models):
        self.models = models

    def __enter__(self):
        self.prev_states = []
        for model in self.models:
            self.prev_states.append(model.training)
            model.train(True)

    def __exit__(self, *args):
        for model, state in zip(self.models, self.prev_states):
            model.train(state)
        return False


def soft_update_params(net, target_net, tau):
    for param, target_param in zip(net.parameters(), target_net.parameters()):
        target_param.data.copy_(tau * param.data +
                                (1 - tau) * target_param.data)

def set_seed_everywhere(seed):
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)


def make_dir(*path_parts):
    dir_path = os.path.join(*path_parts)
    try:
        os.mkdir(dir_path)
    except OSError:
        pass
    return dir_path

def weight_init(m):
    """Custom weight init for Conv2D and Linear layers."""
    if isinstance(m, nn.Linear):
        nn.init.orthogonal_(m.weight.data)
        if hasattr(m.bias, 'data'):
            m.bias.data.fill_(0.0)


class MLP(nn.Module):
    def __init__(self,
                 input_dim,
                 hidden_dim,
                 output_dim,
                 hidden_depth,
                 output_mod=None):
        super().__init__()
        self.trunk = mlp(input_dim, hidden_dim, output_dim, hidden_depth,
                         output_mod)
        self.apply(weight_init)

    def forward(self, x):
        return self.trunk(x)


def mlp(input_dim, hidden_dim, output_dim, hidden_depth, output_mod=None):
    if hidden_depth == 0:
        mods = [nn.Linear(input_dim, output_dim)]
    else:
        mods = [nn.Linear(input_dim, hidden_dim), nn.ReLU(inplace=True)]
        for i in range(hidden_depth - 1):
            mods += [nn.Linear(hidden_dim, hidden_dim), nn.ReLU(inplace=True)]
        mods.append(nn.Linear(hidden_dim, output_dim))
    if output_mod is not None:
        mods.append(output_mod)
    trunk = nn.Sequential(*mods)
    return trunk

def to_np(t):
    if t is None:
        return None
    elif t.nelement() == 0:
        return np.array([])
    else:
        return t.cpu().detach().numpy()

def evaluate(value):
    if isinstance(value,str):
        return eval(value)
    else:
        return value



class Variable:
    def __init__(self, **kwargs):
        self.value = None

    def update(self, condition = None):
        pass

    def get(self):
        return self.value

class Static(Variable):
    def __init__(self, value,**kwargs):
        self.value = value
    
class Sigmoid(Variable):
    def __init__(self, center, spread, max = 1, min = 0, startHigh = True,**kwargs):
        self.center = evaluate(center)
        self.spread = evaluate(spread)
        self.max = max
        self.min = min
        self.startHigh = 1 if startHigh else -1
        self.update()
    
    def update(self, episode = 0):
        x = (self.startHigh*episode - self.center)/self.spread
        value = 1 / (1 + np.exp(x))
        self.value = value * (self.max-self.min) + self.min

class Linear(Variable):
    def __init__(self,lastEpisode,max = 1, min = 0, startHigh = True, **kwargs):
        self.lastEpisode = evaluate(lastEpisode)
        self.max = max
        self.min = min
        self.startHigh = startHigh
        self.update()

    def update(self, episode=0):
        if self.startHigh:
            self.value = ((self.lastEpisode-episode)/self.lastEpisode * (self.max-self.min)) + self.min
        else:
            self.value = (episode/self.lastEpisode * (self.max-self.min)) + self.min

class Geometric(Variable):
    def __init__(self,startValue = 1, factor = 0.9,**kwargs):
        self.value = startValue
        self.factor = factor
    
    def update(self,episode = 0):
        self.value *= self.factor


def get_imported_modules():
    modules = []
    for name, val in globals().items():
        if isinstance(val, types.ModuleType):
            try:
                modules.append(f"{val.__name__}=={version(val.__name__)}")
            except:
                modules.append(f"{val.__name__}==???")
    return modules

def rotateAroundPoint(point,centre,angle):
    xd = point[0]-centre[0]
    yd = point[1]-centre[1]
    newPoint = [centre[0]+math.cos(angle)*xd+math.sin(angle)*yd,
                centre[1]-math.sin(angle)*xd+math.cos(angle)*yd]
    return newPoint

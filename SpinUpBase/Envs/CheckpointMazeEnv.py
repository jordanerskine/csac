#! /usr/bin/env python3
# Laser Based PointGoalNavigation Gym Environment
# Author: Jake Bruce, Krishan Rana

import numpy as np, cv2, sys, os, math, random, time, torch
from gym import spaces
from Box2D import *
import os
from copy import deepcopy as copy
from gym.envs.mujoco import mujoco_env


PATH = os.path.dirname(os.path.realpath(__file__))

#========================================================
# HELPERS

def rnd(mn,mx): return np.random.random()*(mx-mn)+mn

#--------------------------------------------------------

class RaycastCallback(b2RayCastCallback):
    def __init__(self, **kwargs): 
        super(RaycastCallback, self).__init__(**kwargs)
        self.hit = False
        self.fixture = None
        self.points  = []
        self.normals = []
    def ReportFixture(self, fixture, point, normal, fraction): 
        self.hit = True
        self.fixture = fixture
        self.points.append(point)
        self.normals.append(normal)
        return 1.0

#========================================================
# ENV CLASS

class CheckpointMaze(mujoco_env.MujocoEnv):
    def __init__(self, **kwargs):
        self.__dict__.update(dict(
            wait = 1, _max_episode_steps = 1000, k = 0.1, t = 0.0005, eps = 0.025,
            angle_min = -0.75*np.pi, angle_max=0.75*np.pi,
            laser_range = 0.5, num_beams = 16, laser_noise = 0.01, 
            velocity_max=5, omega_max=10, env_type=1, checkpoints=2, includePosition = True, singleAgent = True, reward_type="dense",
            randomStartsAcrossMap = 1, useLaserArray = True, envTimeStep = 1/60, wallSize = 0.5, roomSize = 1))
        self.__dict__.update(kwargs)
        checks = self.checkpoints
        self.__dict__.update(dict(w = int(1000*(checks+1)*self.roomSize), h = int(1000*self.roomSize), xmn = -(checks+1)/2*self.roomSize, xmx = (checks+1)/2*self.roomSize, 
                                  ymn = -self.roomSize, ymx = self.roomSize, nSubtasks = checks))


        self.bg_img = cv2.resize(cv2.cvtColor(np.random.randint(225,256,(self.h//8,self.w//8)).astype(np.uint8), cv2.COLOR_GRAY2BGR), dsize=(self.w,self.h), interpolation=cv2.INTER_NEAREST)       
        self.action_space = spaces.Box(low=np.array([-1, -1]), high=np.array([1, 1]), dtype=np.float64)
        self.num_laser_samples = self.num_beams
        self.num_bins = 15
        self.checkpointWalls = np.array([(-checks/2+n)*self.roomSize for n in range(checks)])

        # ---- To enable mujoco experiments, specifically to work with transition policies -------

        self.ob_shape = {"laser":[self.num_bins], "position":[3]}
        self.ob_type = self.ob_shape.keys()
        self._config = {} # This variable is designed to include all the environment parameters. It is a dummy for now
        self.viewer = None # This variable is menat to be a render viewer. Still a dummy at the moment
        # ----------------------------------------------------------------------------------------
        self.observation_space = spaces.Box(low=-1, high=1, shape=[(self.num_bins if self.useLaserArray else 0)+(3 if self.includePosition else 0)])           # ---- CHANGED ----
        angle_span  = self.angle_max - self.angle_min
        angle_span += angle_span / self.num_laser_samples
        self.laser_angles = [self.angle_min+i/self.num_laser_samples*angle_span for i in range(self.num_laser_samples)]
        # self._max_episode_steps = self.timeout
        self.collided = False
        self.pixels_per_meter = 500
        self.laser_obs = np.zeros(self.num_laser_samples)
        self.obs_loc = [-5, -5]
        self.actions_prev = [0, 0]
        self.done = False
        self.furthestDist = (-self.checkpoints/2-0.1)*self.roomSize
        self.initialDist = copy(self.furthestDist)
        self.trajectories = []
        self.reset()

    
#-------------- Added for Transition policies compatibility ------------------
    def get_ob_dict(self, ob):
        d = {"laser":[],"position":[]}
        if self.useLaserArray:
            d["laser"] = ob[:,:self.num_bins]
        if self.includePosition:
            d["position"] = ob[:,-3:]
        return d
    

    def get_next_primitive(self,obs,prev_primitive):
        outStr = f"ModularMaze_{self.checkpoints}_{self._getCheckpoint(obs)}"
        return outStr
#-----------------------------------------------------------------------------------------------------------------------------------------------#

    def w2p(self,x,y): return (int((x-self.xmn)/(self.xmx-self.xmn)*self.w), int(self.h-(y-self.ymn)/(self.ymx-self.ymn)*self.h))
    def p2w(self,x,y): return (float(x)/self.w*(self.xmx-self.xmn)+self.xmn), (float(y)-self.h)/-self.h*(self.ymx-self.ymn)+self.ymn
    def w2r(self,r)  : return  int(r/(self.xmx-self.xmn)*self.w)

#-----------------------------------------------------------------------------------------------------------------------------------------------#

    def reset(self, robotPos = None, eval = False):
        self.world = b2World(gravity=(0,0))
        self.epStep = 0
        self.trajectories.append([])

        # outer walls
        wall_top      = self.world.CreateStaticBody(position=( 0, self.ymx), shapes=b2PolygonShape(box=(self.xmx-self.xmn,0.025)))
        wall_bottom   = self.world.CreateStaticBody(position=( 0,self.ymn), shapes=b2PolygonShape(box=(self.xmx-self.xmn,0.025)))
        wall_right    = self.world.CreateStaticBody(position=( self.xmx, 0), shapes=b2PolygonShape(box=(0.025,self.ymx-self.ymn)))
        wall_left     = self.world.CreateStaticBody(position=(self.xmn, 0), shapes=b2PolygonShape(box=(0.025,self.ymx-self.ymn)))
        self.outer_walls = [wall_top, wall_bottom, wall_right, wall_left]

#-----------------------------------------------------------------------------------------------------------------------------------------------#
        # BASIC

        if self.env_type == 0:
            # Two corridors into alternating dead ends
            rooms = [0]
            for n in range(1,self.checkpoints):
                rooms.append((n%2)+1)
            
        elif self.env_type == 1:
            # alternating dead ends
            rooms = []
            for n in range(self.checkpoints):
                rooms.append((n%2)+1)

        self.barrier_walls = []
        xMid = (-self.checkpoints/2+0.5)*self.roomSize
        for roomId in rooms:
            room = self._createRoom(xMid,roomId)
            xMid +=self.roomSize
            for wall in room:
                self.barrier_walls.append(wall)
            

#-----------------------------------------------------------------------------------------------------------------------------------------------#
        self._getObstacleMask()

        # Search for obstacle free robot location
        end = self.furthestDist if not eval else self.initialDist
        if robotPos is None:
            while(1):
                robotx = rnd((-self.checkpoints/2-0.4)*self.roomSize,end)
                roboty = rnd(0.85, -0.85)*self.roomSize
                robotpt = self.w2p(*np.array([robotx, roboty]))
                if self.obstacle_mask[robotpt[1], robotpt[0]] == 1:
                    continue
                else:
                    break
            self.robot_loc = np.array([robotx, roboty])
            angle=rnd(-np.pi,np.pi)
        else:
            self.robot_loc = np.array(robotPos[:2])
            angle = robotPos[2]

        # Initialise Agent
        self.agent_body  = self.world.CreateDynamicBody(position=(self.robot_loc[0], self.robot_loc[1]), angle=angle, angularVelocity=0, linearDamping=20.0, angularDamping=30.0)
        self.agent_shape = self.agent_body.CreateFixture(shape=b2CircleShape(pos=(0,0), radius=0.05), density=0.1, friction=0.3)
        self.agent_body.mass = 1

        self.timestep = 0
        return self._obs()

        
    def _createRoom(self,xMid,roomId):
        room = []
        f = self.roomSize
        if roomId == 0:
            room.append(self.world.CreateStaticBody(position=( xMid-0.5*f, 0), shapes=b2PolygonShape(box=(0.05*f,0.5*f))))
            room.append(self.world.CreateStaticBody(position=( xMid+0.5*f, 0), shapes=b2PolygonShape(box=(0.05*f,0.5*f))))
            room.append(self.world.CreateStaticBody(position=( xMid, 0), shapes=b2PolygonShape(box=(0.5*f,0.05*f))))
            room.append(self.world.CreateStaticBody(position=( xMid, 0.7*f), shapes=b2PolygonShape(box=(0.05*f,0.3*f))))
            room.append(self.world.CreateStaticBody(position=( xMid, -0.7*f), shapes=b2PolygonShape(box=(0.05*f,0.3*f))))
        elif roomId == 1:
            room.append(self.world.CreateStaticBody(position=( xMid-0.5*f, 0), shapes=b2PolygonShape(box=(0.05*f,self.wallSize*f))))
            room.append(self.world.CreateStaticBody(position=( xMid, 0.5*f), shapes=b2PolygonShape(box=(0.05*f,0.5*f))))
            room.append(self.world.CreateStaticBody(position=( xMid-0.25*f, 0), shapes=b2PolygonShape(box=(0.25*f,0.05*f))))
            room.append(self.world.CreateStaticBody(position=( xMid+0.5*f, 0), shapes=b2PolygonShape(box=(0.05*f,self.wallSize*f))))
        elif roomId == 2:
            room.append(self.world.CreateStaticBody(position=( xMid-0.5*f, 0), shapes=b2PolygonShape(box=(0.05*f,self.wallSize*f))))
            room.append(self.world.CreateStaticBody(position=( xMid, -0.5*f), shapes=b2PolygonShape(box=(0.05*f,0.5*f))))
            room.append(self.world.CreateStaticBody(position=( xMid-0.25*f, 0), shapes=b2PolygonShape(box=(0.25*f,0.05*f))))
            room.append(self.world.CreateStaticBody(position=( xMid+0.5*f, 0), shapes=b2PolygonShape(box=(0.05*f,self.wallSize*f))))
        return room

#-----------------------------------------------------------------------------------------------------------------------------------------------#
    def clearTrajecories(self):
        self.trajectories = []

    def _getObstacleMask(self):
        self.obstacle_mask = np.zeros([self.h, self.w])
        
        for wall in self.barrier_walls:
            points = []
            for local_point in wall.fixtures[0].shape.vertices:
                world_point = wall.GetWorldPoint(local_point)
                pix_point = self.w2p(*world_point)
                points.append(pix_point)
            pt1 = np.array(points[3])
            pt2 = np.array(points[1])
            # Dilate with radius of robot
            self.obstacle_mask[max(pt1[1]-25,0):pt2[1]+25, max(pt1[0]-25,0):pt2[0]+25] = 1

    def trajectoryRender(self, show = True):

        img = self.bg_img.copy()

        # draw walls
        img = self._drawWalls(img)
        
        # draw rooms
        checkTotal = self.checkpoints
        f = self.roomSize
        for check in range(self.checkpoints):
            mid = ((check-checkTotal/2)+0.5)*f
            pnts = [self.w2p(mid-0.5*f,f),
                    self.w2p(mid-0.5*f,-f),
                    self.w2p(mid+0.5*f,-f),
                    self.w2p(mid+0.5*f,f)]
            for i in range(len(pnts)):
                cv2.line(img,pnts[i],pnts[(i+1)%len(pnts)],color=(0,0,0),thickness=8)

        # Agent position and shape data
        agent_loc = self.agent_body.GetWorldPoint(self.agent_shape.shape.pos)
        pix_point = self.w2p(*agent_loc)
        # radius    = self.w2r(self.agent_shape.shape.radius)
        radius = 10

        # Draw agent
        for traj in self.trajectories:
            # print(len(traj))
            for i,point in enumerate(traj):
                # print(int(255.0*(i/len(traj))))
                colour = (int(0),int(255.0*(i/len(traj))),int(255.0*(1.0-i/len(traj))))
                # colour = (0,100,250)
                # print(colour)
                pix_point = self.w2p(*point)
                img = cv2.circle(img, pix_point, radius, colour, -1)
        self.clearTrajecories()
        # show image
        if show:
            cv2.namedWindow("PointGoalNavigation", cv2.WINDOW_NORMAL)
            cv2.imshow("PointGoalNavigation", img)
            cv2.waitKey(1)
        else:
            return img


    def render(self,show = True,showFOV = True):

        agent_color = (0,0,0)
        img = self.bg_img.copy()


        # draw laser rays
        laser_img = img.copy()
        agent_loc = self.agent_body.GetWorldPoint(self.agent_shape.shape.pos)
        if showFOV:
            points = [self.w2p(*agent_loc)]
            for angle, dist in zip(self.laser_angles, self.laser_obs):
                end = agent_loc + [dist*np.cos(self.agent_body.angle + angle), dist*np.sin(self.agent_body.angle + angle)]
                p2 = self.w2p(*end)
                points.append(p2)
            cv2.fillPoly(laser_img, pts=[np.array(points)], color=(128,0,128))
            cv2.polylines(laser_img, pts=[np.array(points)], isClosed=True, color=(255,0,255), thickness=3)
            for angle, dist in zip(self.laser_angles, self.laser_obs):
                end = agent_loc + [dist*np.cos(self.agent_body.angle + angle), dist*np.sin(self.agent_body.angle + angle)]
                p2 = self.w2p(*end)
                cv2.line(laser_img, points[0], p2, (255,0,255), 3)
            img = laser_img//4 + img//4*3 # blend laser image at a low alpha

        # draw walls
        img = self._drawWalls(img)
        
        # draw rooms
        checkTotal = self.checkpoints
        f = self.roomSize
        for check in range(self.checkpoints):
            mid = ((check-checkTotal/2)+0.5)*f
            pnts = [self.w2p(mid-0.5*f,f),
                    self.w2p(mid-0.5*f,-f),
                    self.w2p(mid+0.5*f,-f),
                    self.w2p(mid+0.5*f,f)]
            for i in range(len(pnts)):
                cv2.line(img,pnts[i],pnts[(i+1)%len(pnts)],color=(200,200,200),thickness=8)

        # Agent position and shape data
        agent_loc = self.agent_body.GetWorldPoint(self.agent_shape.shape.pos)
        pix_point = self.w2p(*agent_loc)
        radius    = self.w2r(self.agent_shape.shape.radius)

        # Draw agent
        cv2.circle(img, pix_point, radius, agent_color, -1)
       
        # draw orientation vector
        cv2.circle(img, self.w2p(*(np.array(agent_loc) + [0.060*np.cos(self.agent_body.angle), 0.060*np.sin(self.agent_body.angle)])), self.w2r(0.02), (50,50,50), -1)

        # show image
        if show:
            cv2.namedWindow("PointGoalNavigation", cv2.WINDOW_NORMAL)
            cv2.imshow("PointGoalNavigation", img)
            cv2.waitKey(1)
        else:
            return img


    def _drawWalls(self,img):
        for wall in self.outer_walls + self.barrier_walls:
            points = []
            for local_point in wall.fixtures[0].shape.vertices:
                world_point = wall.GetWorldPoint(local_point)
                pix_point = self.w2p(*world_point)
                points.append(pix_point)
            cv2.fillConvexPoly(img, points=np.array(points), color=(64,64,64))
            cv2.polylines(img, pts=[np.array(points)], isClosed=True, color=(0,0,0), thickness=8)
        return img

    def _generateDomainDistribution(self):
        obsArray = []
        for x in np.arange(0,self.w,50):
            for y in np.arange(0,self.h,50):
                for angle in np.linspace(-np.pi,np.pi,50):
                    if self.obstacle_mask[y,x] == 0:
                        obsArray.append(self.reset(robotPos=np.array([x,y,angle])))
        return obsArray

    def paintQFunction(self,QF, policy):
        array = {}
        obsActArray = []
        obsArray = self._generateDomainDistribution()
        # for obs in obsArray:
        #     for lin in np.linspace(-1,1,10):
        #         for ang in np.linspace(-1,1,10):
        #             obsActArray.append(np.concatenate([obs,np.array([lin,ang])]))
        # values = QF(torch.Tensor(obsActArray))
        acts = policy(torch.Tensor(obsArray))[1]
        for obs, act in zip(obsArray, acts):
            obsActArray.append(torch.cat([torch.Tensor(obs),act],-1))
        values = QF(torch.stack(obsActArray))
        for obs, val in zip(obsActArray, values):
            if self._getCheckpoint(np.array(obs[:-2].detach())) == 0:
                x = str(float(obs[-5]))
                y = str(float(obs[-4]))
                a = str(float(obs[-3]))
                if x not in array:
                    array[x] = {}
                if y not in array[x]:
                    array[x][y] = {}
                if a not in array[x][y]:
                    array[x][y][a] = []
                array[x][y][a].append(val)
        valArray = []
        angles = []

        for x in np.unique(np.array(obsArray)[:,-3]):
            x = str(float(x))
            if x in array:
                valArray.append([])
                for y in np.unique(np.array(obsArray)[:,-2]):
                    y = str(float(y))
                    if y in array[x]:
                        valArray[-1].append(float(array[x][y][max(array[x][y], key=array[x][y].get)][0]))
                        angles.append([float(x),float(y),float(max(array[x][y], key=lambda key: array[x][y][key]))])
                    else:
                        valArray[-1].append(0)
        valArray = np.array(valArray)
        valArray = (valArray-np.min(valArray))/(np.max(valArray)-np.min(valArray))
        img = np.array([valArray for _ in range(3)])
        img = np.transpose(img,(2,1,0))
        img[:,:,0] = 0
        img = cv2.resize(img,(int(self.w/2),self.h), interpolation=cv2.INTER_AREA)
        for angle in angles:
            x,y,a = angle
            x = x/750*self.w/2
            y = y/450*self.h
            start = (int(x),int(y))
            r = 20
            end = (int(x-r*np.cos(a)),int(y-r*np.sin(a)))
            img = cv2.arrowedLine(img,start,end,color = (0,0,0),thickness=3,tipLength=0.3)
        cv2.imshow("image",img)
        cv2.waitKey(1000)
        bgImg = self.bg_img.copy()
        


#-----------------------------------------------------------------------------------------------------------------------------------------------#


    def step(self, action):

        # Scale the actions by their maximums
        lin = float(action[0] * self.velocity_max)
        lin = lin if lin >= 0 else lin * 0.2
        omega = float(action[1] * self.omega_max)

        velocity = (lin*np.cos(self.agent_body.angle), lin*np.sin(self.agent_body.angle))
        
        self.agent_body.linearVelocity = (velocity)
        self.agent_body.angularVelocity = (omega)

        self.trajectories[-1].append(self.agent_body.GetWorldPoint(self.agent_shape.shape.pos))

        # Previous range to goal
        self.actions_prev = action

        p_obs = self._obs()

        # Simulate
        self.world.Step(self.envTimeStep, 10, 10)
        self.world.ClearForces()


        self.timestep += 1
        self.epStep +=1

        obs = self._obs()
        if self.randomStartsAcrossMap:
            self.furthestDist = max(self.furthestDist,self.robotPos[0])
        check = self._getCheckpoint(p_obs)
        rews = self._genReward(obs)
        info = {"reward{}".format(n):rews[n] for n in range(len(rews))}
        info["overallReward"] = np.sum(rews)
        info["subtask"] = check
        info["success"] = self._getSuccess(rews)
        rew = rews[check] if not self.singleAgent else np.sum(rews)
        if not self.useLaserArray:
            obs = obs[-3:]
        if not self.includePosition:
            obs = obs[:-3]
        done = True if self.epStep >= self._max_episode_steps else False
        return obs, rew, done, info

    def _getSuccess(self,rews):
        if self.reward_type == "dense":
            return 1 if np.sum(rews) > self.checkpoints else 0
        else:
            return 1 if rews[-1] == 1 else 0

    def _getCheckpoint(self,obs):
        robotX = obs[-3]
        checkpoint = int(np.sum(robotX>self.checkpointWalls[1:]))
        return checkpoint
        

    def _genReward(self,obs):
        if self.reward_type == "dense":
            check = self._getCheckpoint(obs)
            rewards = [1 if n < check else 0 for n in range(self.checkpoints)]
            robotX = obs[-3]
            rewards[check] = (robotX-self.checkpointWalls[check])/self.roomSize
        else:
            robotX = obs[-3]
            check = self._getCheckpoint(obs)
            rewards = [1 if n < check else 0 for n in range(self.checkpoints)]
            rewards[-1] = 1 if robotX > self.checkpointWalls[-1] else 0
        return rewards
        


#-----------------------------------------------------------------------------------------------------------------------------------------------#


    def _laser_rays(self):
        agent_loc = np.array(self.agent_body.GetWorldPoint(self.agent_shape.shape.pos))
        return [(agent_loc, agent_loc + [self.laser_range*np.cos(self.agent_body.angle + angle), self.laser_range*np.sin(self.agent_body.angle + angle)]) for angle in self.laser_angles]

#-----------------------------------------------------------------------------------------------------------------------------------------------#


    def _get_position_data(self):
        robot_angle = np.arctan2(np.sin(self.agent_body.angle), np.cos(self.agent_body.angle))
        robot_loc = np.array(self.agent_body.GetWorldPoint(self.agent_shape.shape.pos))
        laser_scan = self.laser_obs

        return  robot_loc, robot_angle, laser_scan, self.obs_loc


    def _obs(self):
        # Generate laser scan
        laser_samples = np.zeros(self.num_laser_samples)
        for i,(start, end) in enumerate(self._laser_rays()):
            callback = RaycastCallback()
            self.world.RayCast(callback, start, end)
            laser_samples[i] = min([np.linalg.norm(start - point) for point in callback.points]) if len(callback.points) > 0 else self.laser_range
        num_hits = (laser_samples < self.laser_range).sum()
        self.laser_obs = laser_samples.copy()
        # Applying noise to laser scan
        self.laser_obs[laser_samples < self.laser_range] *= np.random.normal(1,self.laser_noise,num_hits)
        # Get global data
        self.robotPos, self.robotAngle, _,_ = self._get_position_data()
        hit = (laser_samples < self.laser_range).astype(np.float)
        self.collided = np.any((laser_samples < 0.1).astype(np.float) == 1.0)

        laser_scan = np.zeros(self.num_bins)
        laser_hit = np.zeros(self.num_bins)
        div_factor = int(self.num_laser_samples/self.num_bins)
        for i in range(self.num_bins):
            laser_scan[i] = np.mean(self.laser_obs[i*div_factor:(i*div_factor+div_factor)]) 
            laser_hit[i] = (laser_scan[i] < self.laser_range).astype(np.float)

        
        obs = [laser_scan]
        obs.append(self.robotPos)
        obs.append(np.array([self.robotAngle]))
            
        return  np.concatenate(obs)

    
    def seed(self, seed=0):
        random.seed(seed)
        np.random.seed(seed)
        self.action_space.np_random.seed(seed)
        return



class ModularCheckpointMaze(CheckpointMaze):
    def __init__(self, **kwargs):
        self.targetSubtask = kwargs["targetSubtask"]
        kwargs['singleAgent'] = False
        super().__init__(**kwargs)

    def is_terminate(self, ob, **kwargs):
        robotX = ob[-3] 
        if np.sum(robotX>self.checkpointWalls[1:]) > self.targetSubtask:
            return True
        elif self.targetSubtask == self.checkpoints-1 and np.sum(self._genReward(ob)) > self.checkpoints:
            return True
        return False

    def reset(self):
        if "obstacle_mask" not in self.__dict__:
            super().reset()

        while(1):
            robotx = rnd((-self.checkpoints/2-0.4)*self.roomSize,(self.checkpoints/2-0.4)*self.roomSize)
            roboty = rnd(0.85, -0.85)*self.roomSize
            robotpt = self.w2p(*np.array([robotx, roboty]))
            if self.obstacle_mask[robotpt[1], robotpt[0]] == 1 or np.sum(robotx>self.checkpointWalls[1:]) != self.targetSubtask:
                continue
            else:
                break
        angle=rnd(-np.pi,np.pi)
        obs = super().reset([robotx,roboty,angle])
        return obs

    def step(self, act):
        obs, rew, done, info = super().step(act)
        info['success'] = 1 if self.is_terminate(obs) else 0
        rew = info[f"reward{self.targetSubtask}"]
        return obs, rew, done, info
        


if __name__ == "__main__":
    env = ModularCheckpointMaze(targetSubtask = 1, checkpoints = 3)
    for _ in range(10):
        env.reset()
        st = time.time()
        delay = 5 if '--delay' not in sys.argv else int(sys.argv[sys.argv.index('--delay')+1])
        while (time.time()-st) < delay:
            env.render()

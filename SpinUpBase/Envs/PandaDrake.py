import pydrake
import meshcat
from meshcat.servers.zmqserver import start_zmq_server_as_subprocess
import matplotlib.pyplot as plt
import numpy as np, time, math, pydot
from pydrake.all import *
from pydrake.multibody.jupyter_widgets import MakeJointSlidersThatPublishOnCallback
from IPython.display import display, HTML, SVG

from gym import spaces


class PandaJointReacher:
    def __init__(self, **kwargs):
        self.actSize = 7
        self.obsSize = 10

        self.action_space = spaces.Box(low=-1, high=1, shape=[self.actSize])
        self.observation_space = spaces.Box(low=-1, high=1, shape=[self.obsSize])


        self.maxLen = 100 if "_max_episode_steps" not in kwargs else kwargs["_max_episode_steps"]
        headless = True if "headless" not in kwargs else kwargs["headless"]

        self.rate = 20 #hz
        self.dt = 1/self.rate


        self.initialState = np.array([-1.57, 0.1, 0, -1.2, 0, 1.6, 0, 0, 0])

        builder = DiagramBuilder()

        #Add scene grapher and multibody plant
        self.plant, scene_graph = AddMultibodyPlantSceneGraph(builder, time_step=1e-4)
        self.plant.set_name("Robot")
        scene_graph.set_name("Scene Graph")
        panda_model = Parser(self.plant, scene_graph).AddModelFromFile(FindResourceOrThrow("drake/manipulation/models/franka_description/urdf/panda_arm_hand.urdf"))
        # sphere_model = Parser(self.plant, scene_graph).AddModelFromFile(FindResourceOrThrow("drake/examples/manipulation_station/models/sphere.sdf"),"sphere")
        self.plant.WeldFrames(self.plant.world_frame(), self.plant.GetFrameByName("panda_link0"))
        self.plant.Finalize()


        if not headless:
            vis = DrakeVisualizer.AddToBuilder(builder, scene_graph)
            vis.set_name("Drake Visualizer")
        #meshcat = ConnectMeshcatVisualizer(builder, scene_graph, zmq_url=zmq_url)


        # ---- PID control ----
        Kp = np.full(9, 100)
        Ki = np.sqrt(Kp)*0#2
        Kd = np.full(9, 10)
        panda_controller = builder.AddSystem(InverseDynamicsController(self.plant, Kp, Ki, Kd, False))
        panda_controller.set_name("track_joint_config_controller")
        builder.Connect(self.plant.get_state_output_port(panda_model),
                        panda_controller.get_input_port_estimated_state())
        builder.Connect(panda_controller.get_output_port_control(),
                        self.plant.get_actuation_input_port())

        # Our Controlelr
        self.joint_controller = builder.AddSystem(JointController(self.initialState))
        self.joint_controller.set_name("Joint controller")
        builder.Connect(self.joint_controller.get_output_port(0), panda_controller.GetInputPort('desired_state'))
        builder.Connect(self.plant.get_state_output_port(panda_model),self.joint_controller.get_input_port())

        self.diagram = builder.Build()


        context = self.diagram.CreateDefaultContext()

        self.simulator = Simulator(self.diagram, context)
        # self.simulator.set_target_realtime_rate(1.0)



        self.reset()


    def reset(self):
        context = self.diagram.CreateDefaultContext()
        self.simulator.reset_context(context)
        self.plant_context = self.plant.GetMyMutableContextFromRoot(context)
        # x0 = np.hstack((q0, 0*q0))
        self.plant.SetPositions(self.plant_context, self.initialState)
        self.joint_controller.reset(self.initialState)
        self.epTime = 0

        self.target = 2*np.random.random(3)-1

        obs = np.hstack((self.initialState[:7],self.target))

        return obs

    def _getReward(self):
        eePos = self.plant.EvalBodyPoseInWorld(self.plant_context,self.plant.GetBodyByName("panda_hand")).translation()
        dist = np.linalg.norm(eePos-self.target)
        return math.exp(-dist)
    
    def step(self, act):
        act = np.hstack((act,np.array([0,0])))
        self.joint_controller.increment_target_position(act*0.1)
        self.epTime += self.dt
        self.simulator.AdvanceTo(self.epTime)

        obs = np.hstack((self.joint_controller.observed_state[:7],self.target))

        rew = self._getReward()
        done = self.epTime/self.dt >= self.maxLen
        info = {"Success": rew > 0.8,
                "overallReward":rew,
                "subtask": 0,
                "reward0":rew}

        return obs, rew, done, info

    

class PandaCartReacher:
    def __init__(self, **kwargs):
        self.actSize = 3
        self.obsSize = 6

        self.action_space = spaces.Box(low=-1, high=1, shape=[self.actSize ])
        self.observation_space = spaces.Box(low=-1, high=1, shape=[self.obsSize])


        self.maxLen = 100 if "_max_episode_steps" not in kwargs else kwargs["_max_episode_steps"]
        headless = True if "headless" not in kwargs else kwargs["headless"]

        self.rate = 20 #hz
        self.dt = 1/self.rate


        self.initialState = np.array([-1.57, 0.1, 0, -1.2, 0, 1.6, 0, 0, 0])

        builder = DiagramBuilder()

        #Add scene grapher and multibody plant
        self.plant, scene_graph = AddMultibodyPlantSceneGraph(builder, time_step=1e-4)
        self.plant.set_name("Robot")
        scene_graph.set_name("Scene Graph")
        panda_model = Parser(self.plant, scene_graph).AddModelFromFile(FindResourceOrThrow("drake/manipulation/models/franka_description/urdf/panda_arm_hand.urdf"))
        self.plant.WeldFrames(self.plant.world_frame(), self.plant.GetFrameByName("panda_link0"))
        self.plant.Finalize()


        if not headless:
            vis = DrakeVisualizer.AddToBuilder(builder, scene_graph)
            vis.set_name("Drake Visualizer")
        #meshcat = ConnectMeshcatVisualizer(builder, scene_graph, zmq_url=zmq_url)


        # ---- PID control ----
        Kp = np.full(9, 100)
        Ki = np.sqrt(Kp)*0#2
        Kd = np.full(9, 10)
        panda_controller = builder.AddSystem(InverseDynamicsController(self.plant, Kp, Ki, Kd, False))
        panda_controller.set_name("track_joint_config_controller")
        builder.Connect(self.plant.get_state_output_port(panda_model),
                        panda_controller.get_input_port_estimated_state())
        builder.Connect(panda_controller.get_output_port_control(),
                        self.plant.get_actuation_input_port())

        # Our Controlelr
        self.joint_controller = builder.AddSystem(JointController(self.initialState))
        self.joint_controller.set_name("Joint controller")
        builder.Connect(self.joint_controller.get_output_port(0), panda_controller.GetInputPort('desired_state'))
        builder.Connect(self.plant.get_state_output_port(panda_model),self.joint_controller.get_input_port())

        self.diagram = builder.Build()


        self.init_context = self.diagram.CreateDefaultContext()

        self.simulator = Simulator(self.diagram, self.init_context)
        # self.simulator.set_target_realtime_rate(1.0)


        self.reset()


    def reset(self):
        context = self.diagram.CreateDefaultContext()
        self.simulator.reset_context(context)
        self.plant_context = self.plant.GetMyMutableContextFromRoot(context)
        # x0 = np.hstack((q0, 0*q0))
        self.plant.SetPositions(self.plant_context, self.initialState)
        self.joint_controller.reset(self.initialState)
        self.epTime = 0

        self.target = 2*np.random.random(3)-1

        obs = np.hstack((self._getEEpos(),self.target))
        return obs

    def _getJacobian(self):
        
        G = self.plant.GetBodyByName("panda_hand").body_frame()
        W = self.plant.world_frame()
        J = self.plant.CalcJacobianSpatialVelocity(self.plant_context, JacobianWrtVariable.kQDot,G,[0,0,0],W,W)
        return J

    def _getJointVels(self,cartVels):
        V = np.hstack((np.zeros(3),cartVels))
        J = self._getJacobian()[:,0:7]
        jointVels = np.linalg.pinv(J).dot(V)
        return jointVels

    def _getReward(self):
        eePos = self._getEEpos()
        dist = np.linalg.norm(eePos-self.target)
        return math.exp(-dist)

    def _getEEpos(self):
        return self.plant.EvalBodyPoseInWorld(self.plant_context,self.plant.GetBodyByName("panda_hand")).translation()
    
    def step(self, act):
        act = self._getJointVels(act*0.01)
        act = np.hstack((act,np.array([0,0])))
        self.joint_controller.increment_target_position(act)
        self.epTime += self.dt
        self.simulator.AdvanceTo(self.epTime)

        obs = np.hstack((self._getEEpos(),self.target))

        rew = self._getReward()
        done = self.epTime/self.dt >= self.maxLen
        info = {"Success": rew > 0.8,
                "overallReward":rew,
                "subtask": 0,
                "reward0":rew}

        return obs, rew, done, info



class JointController(LeafSystem):
    def __init__(self, initialState):
        LeafSystem.__init__(self)
        self.reset(initialState)
        self.DeclareVectorInputPort("State",BasicVector(18))
        self.DeclareVectorOutputPort("DesiredPosition", BasicVector(18), self.CalcOutput)

    def reset(self, state):
        self.observed_state = np.hstack((state,np.zeros(9)))
        self.target_state = np.hstack((state,np.zeros(9)))

    def increment_target_position(self, dPos):
        self.target_state = self.observed_state + np.hstack((dPos,np.zeros(len(dPos))))

    
    def CalcOutput(self, context: Context, output: BasicVector):
        self.observed_state = self.get_input_port().Eval(context)
        
        output.SetFromVector(self.target_state)




if __name__ == "__main__":
    env = PandaCartReacher(headless=0)
    for ep in range(6):
        inc = 2*(ep%2-0.5)/10
        act = np.zeros(3)
        ind = int((ep-ep%2)/2)
        act[ind] = inc
        print(act)
        env.reset()
        for _ in range(30):
            env.step(act)
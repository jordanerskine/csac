from gym import spaces
import numpy as np


class Env:
    def __init__(self,**args):
        # This is how variables are passed into the environment through the Hydra formulation
        # Any additional variables can be passed in in a similar format
        self._max_episode_steps = 100 if '_max_episode_steps' not in args else args['_max_episode_steps']

        # The number of subtasks is defined
        self.nSubtasks = 2

        # The action and observations spaces are defined
        self.actSpace = 3
        self.obsSpace = 5

        self.action_space = spaces.Box(low=np.array([-1 for _ in range(self.actSpace)]),
                                    high=np.array([1 for _ in range(self.actSpace)]),dtype=np.float64)
        self.observation_space = spaces.Box(low=-1,high=1,shape=[self.obsSpace])

        self.reset()

    def reset(self):
        # Some reset is done on the environment, return the observation

        self.epStep = 0

        return self._obs()

    def step(self, action):
        # A step is taken based on an action, and then the reward, observation, done and info are produced
        # Info is important for the CSAC method, as the subtask that the environment is currently in, as well as the
        # reward for the different subtasks is stored there

        self.epStep += 1

        subtask = self._subtask()
        rews = self._reward()
        done = self._done()
        success = self._success()

        obs = self._obs()

        info = {f"reward{n}":rews[n] for n in range(self.nSubtasks)}
        info["overallReward"] = np.sum(rews)
        info["success"] = success
        info["subtask"] = subtask

        return obs, rews[subtask], done, info


    def _obs(self):
        # Return the state of the environment for observation
        return np.zeros(self.observation_space)

    def _subtask(self):
        # Return the current subtask, based on the state of the environment.
        # This value must be in [0,self.nSubtasks)
        return 0

    def _reward(self):
        # Return reward based on the state of the environment.
        # This should include the reward for each subtask

        rews = [0 for n in range(self.nSubtasks)]
        return rews


    def _success(self):
        # This is useful for estimating the completion of the overall task
        # This can be evaluated based on the state of the environment

        return 0

    
    def _done(self):
        # The done flag. This can be defined based on success, but I often have it automatically go to max length
        done = self.epStep > self._max_episode_steps or self._success()
        return done
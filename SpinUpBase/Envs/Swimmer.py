from dm_control.suite import swimmer as DM_swimmer
from dm_control.suite.swimmer import _make_swimmer
from dm_control.rl import control
from gym import spaces
import numpy as np


class Swimmer():
    def __init__(self,**kwargs):
        
        self.nSubtasks = 1 if "nSubtasks" not in kwargs else kwargs["nSubtasks"]
        nJoints = 3 if "nJoints" not in kwargs else kwargs["nJoints"]
        self._max_episode_steps = 1000 if '_max_episode_steps' not in kwargs else kwargs['_max_episode_steps']

        self.env = _make_swimmer(nJoints)
        obs = self.reset()
        self.action_space = spaces.Box(low=-1, high=1, shape=[nJoints-1])
        self.observation_space = spaces.Box(low=-1, high=1, shape=[len(obs)])

    def reset(self):
        obs = self.env.reset()
        obs = self._convertObs(obs.observation)
        self.ep_step = 0
        return obs
    
    def _convertObs(self,obs):
        return np.concatenate(list(obs.values()))
    
    def step(self,action):
        _, reward, done, obs = self.env.step(action)
        obs = self._convertObs(obs)
        self.ep_step += 1
        done = 0 if self.ep_step < self._max_episode_steps else 1
        info = {"success":reward >= 1,
                "reward0": reward,
                "overallReward":reward}
        return obs, reward, done, info

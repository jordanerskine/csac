from roboticstoolbox.backends.Swift import Swift
import roboticstoolbox as rtb
import spatialmath as sm
import numpy as np
import copy, os, time, random, math

from collections import deque

from gym import spaces


class MobileManipulator:
    def __init__(self, **kwargs):

        self._max_episode_steps = 200 if '_max_episode_steps' not in kwargs else kwargs['_max_episode_steps']
        self.headless = True if 'headless' not in kwargs else kwargs['headless']
        self.earlyStop = False if 'earlyStop' not in kwargs else kwargs['earlyStop']
        self.linger = 1 if 'linger' not in kwargs else kwargs['linger']


        self.lingerCheck = deque(maxlen=self.linger)
        

        self.env = Swift(display=not self.headless)
        self.env.launch()

        self.action_space = spaces.Box(low=np.array([-1 for _ in range(9)]),
                                         high=np.array([1 for _ in range(9)]), dtype=np.float64) # 3 action length
        self.observation_space = spaces.Box(low=-1, high=1, shape=[9+3])

        self.nSubtasks = 2

        # self.genCollisionObjects()
        self.genRobot()
        self.genTarget()

        self.reset()

    def genCollisionObjects(self):


        b1 = rtb.Box(
            scale=[0.60, 1.1, 0.02],
            base=sm.SE3(2.9, 0, 0.20))

        b2 = rtb.Box(
            scale=[0.60, 1.1, 0.02],
            base=sm.SE3(2.9, 0, 0.60))

        b3 = rtb.Box(
            scale=[0.60, 1.1, 0.02],
            base=sm.SE3(2.9, 0, 1.00))

        b4 = rtb.Box(
            scale=[0.60, 1.1, 0.02],
            base=sm.SE3(2.9, 0, 1.40))

        b5 = rtb.Box(
            scale=[0.60, 0.02, 1.40],
            base=sm.SE3(2.9, 0.55, 0.7))

        b6 = rtb.Box(
            scale=[0.60, 0.02, 1.40],
            base=sm.SE3(2.9, -0.55, 0.7))

        # Front panel
        t1 = rtb.Box(
            scale=[0.02, 1.6, 0.6],
            base=sm.SE3(2.6, 1.4, 0.3)
        )

        # Left side
        t2 = rtb.Box(
            scale=[0.6, 0.02, 0.6],
            base=sm.SE3(2.9, 2.2, 0.3)
        )

        # Right side
        t3 = rtb.Box(
            scale=[0.6, 0.02, 0.6],
            base=sm.SE3(2.9, 0.6, 0.3)
        )

        # Back panel
        t4 = rtb.Box(
            scale=[0.02, 1.6, 0.6],
            base=sm.SE3(3.2, 1.4, 0.3)
        )

        # Bench top
        t5 = rtb.Box(
            scale=[0.62, 1.12, 0.02],
            base=sm.SE3(2.9, 1.15, 0.6)
        )

        # Sink bottom
        t6 = rtb.Box(
            scale=[0.6, 0.5, 0.02],
            base=sm.SE3(2.9, 1.95, 0.3)
        )

        # Sink Right
        t7 = rtb.Box(
            scale=[0.6, 0.02, 0.6],
            base=sm.SE3(2.9, 1.7, 0.3)
        )

        # Bench Obstacle
        t8 = rtb.Box(
            scale=[0.6, 0.02, 0.3],
            base=sm.SE3(2.9, 1.1, 0.75)
        )

        self.collisions = [
            b1, b2, b3, b4, b5, b6,
            t1, t2, t3, t4, t5, t6,
            t7, t8
        ]

        for col in self.collisions:
            self.env.add(col)

    def genRobot(self):

        self.r = rtb.models.Frankie()
        self.r.q = self.r.qr
        self.env.add(self.r)

    def genTarget(self):
        self.target = rtb.Sphere(
            radius = 0.05
        )
        self.env.add(self.target)

        self.cylinder = rtb.Cylinder(radius = 1, length = 2)
        self.env.add(self.cylinder)

    def reset(self):
        self.epStep = 0
        self.r.q = self.r.qr

        self.targetPos = [random.random()*(max-min)+min for min, max in [[-3,3],[-3,3],[0,0.5]]]
        self.target.base=sm.SE3(*self.targetPos)
        targetPos = self.targetPos[:2] + [1]
        self.cylinder.base = sm.SE3(*targetPos)

        return self._obs()

    def step(self, act):
        self.r.qd = act
        self.env.step()

        obs = self._obs()
        # self.r.base = self.r.links[2]._fk * sm.SE3.Tz(-0.3)
        # self.r.q[:2] = 0

        
        collided = False
        # for obj in self.collisions:
        #     if self.r.collided(obj):
        #         collided = True

        done = self.epStep >= self._max_episode_steps or collided
        

        self.epStep += 1



        rews = self.getReward()
        info = {f'reward{n}':rews[n] for n in range(len(rews))}
        info['overallReward'] = np.sum(rews)
        subtask = self.getSubtask()
        self.lingerCheck.append(subtask)
        info['subtask'] = min(self.lingerCheck)
        self.lingerCheck.append(self.earlyStop and subtask == 1)
        # if np.all(self.lingerCheck):
        #     done = True

        return obs, rews[subtask], done, info

    def getReward(self):
        baseDist = np.linalg.norm(self.r.q[:2]-self.targetPos[:2])
        rew1 = math.exp(-3*(max(0,baseDist-0.5)))-1

        eePos = self.r.fkine(self.r.q)
        goalDist = np.linalg.norm(eePos.t-self.targetPos)
        rew2 = math.exp(-3*goalDist)-1
        rews = [rew1, rew2]
        return rews

    def getSubtask(self):
        subtask = np.linalg.norm(self.r.q[:2]-self.targetPos[:2]) < 1
        return subtask

    def _obs(self):
        obs = np.concatenate([self.r.q,self.targetPos])
        return obs




if __name__ == '__main__':
    env = MobileManipulator(**{'headless':0})
    env.env.step(render=False)
    for ep in range(10):
        env.reset()
        for i in range(100):
            
            env.step([0.1 for _ in range(9)])

            # if r.collided(b2):
            #     print("collided you muppet")

            # 

    env.env.hold()
import gym, numpy as np
import panda_gym
from gym import spaces

class Reacher():
    def __init__(self,**args):
        self.epLength = 100 if '_max_episode_steps' not in args else args['_max_episode_steps']
        self.render = 0 if 'render' not in args else args['render']
        self.rewardType = "" if 'rewardType' not in args or args['rewardType'] not in ['Dense'] else args['rewardType']
        self.controlType = "" if 'controlType' not in args or args['controlType'] not in ['Joints'] else args['controlType']
        self.env = gym.make(f'PandaReach{self.controlType}{self.rewardType}-v2', render = self.render)
        self.nSubtasks = 1

        actSize = len(self.env.action_space.sample())
        obsSize = len(self.reset())
        self.action_space = spaces.Box(low=-1, high=1, shape=[actSize])
        self.observation_space = spaces.Box(low=-1, high=1, shape=[obsSize])
            
    

    def step(self, act):
        envObs, rew, done, envInfo = self.env.step(act)
        obs = np.concatenate((envObs['observation'],envObs['desired_goal']))
        info = {}
        info['overallReward'] = rew
        info['reward0'] = rew
        info['subtask'] = 0
        info['success'] = envInfo['is_success']
        self.epStep += 1
        done = self.epStep > self.epLength
        return obs, rew, done, info

    def reset(self):
        self.epStep = 0
        envObs = self.env.reset()
        obs = np.concatenate((envObs['observation'],envObs['desired_goal']))
        return obs

class PickPlace():
    def __init__(self,**args):
        self.epLength = 100 if '_max_episode_steps' not in args else args['_max_episode_steps']
        self.render = 0 if 'render' not in args else args['render']
        self.rewardType = "" if 'rewardType' not in args or args['rewardType'] not in ['Dense'] else args['rewardType']
        self.controlType = "" if 'controlType' not in args or args['controlType'] not in ['Joints'] else args['controlType']
        self.env = gym.make(f'PandaPickAndPlace{self.controlType}{self.rewardType}-v2', render = self.render)
        self.nSubtasks = 1

        actSize = len(self.env.action_space.sample())
        obsSize = len(self.reset())
        self.action_space = spaces.Box(low=-1, high=1, shape=[actSize])
        self.observation_space = spaces.Box(low=-1, high=1, shape=[obsSize])
            
    

    def step(self, act):
        envObs, rew, done, envInfo = self.env.step(act)
        obs = np.concatenate((envObs['observation'],envObs['desired_goal']))
        info = {}
        info['overallReward'] = rew
        info['reward0'] = rew
        info['subtask'] = 0
        info['success'] = envInfo['is_success']
        self.epStep += 1
        done = self.epStep > self.epLength
        return obs, rew, done, info

    def reset(self):
        self.epStep = 0
        envObs = self.env.reset()
        obs = np.concatenate((envObs['observation'],envObs['desired_goal']))
        return obs

class Stack():
    def __init__(self,**args):
        self.epLength = 100 if '_max_episode_steps' not in args else args['_max_episode_steps']
        self.render = 0 if 'render' not in args else args['render']
        self.rewardType = "" if 'rewardType' not in args or args['rewardType'] not in ['Dense'] else args['rewardType']
        self.controlType = "" if 'controlType' not in args or args['controlType'] not in ['Joints'] else args['controlType']
        self.env = gym.make(f'PandaStack{self.controlType}{self.rewardType}-v2', render = self.render)
        self.nSubtasks = 1

        actSize = len(self.env.action_space.sample())
        obsSize = len(self.reset())
        self.action_space = spaces.Box(low=-1, high=1, shape=[actSize])
        self.observation_space = spaces.Box(low=-1, high=1, shape=[obsSize])
            
    

    def step(self, act):
        envObs, rew, done, envInfo = self.env.step(act)
        obs = np.concatenate((envObs['observation'],envObs['desired_goal']))
        info = {}
        info['overallReward'] = rew
        info['reward0'] = rew
        info['subtask'] = 0
        info['success'] = envInfo['is_success']
        self.epStep += 1
        done = self.epStep > self.epLength
        return obs, rew, done, info

    def reset(self):
        self.epStep = 0
        envObs = self.env.reset()
        obs = np.concatenate((envObs['observation'],envObs['desired_goal']))
        return obs
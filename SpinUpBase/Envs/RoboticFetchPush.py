import gym, numpy as np
from gym.envs.robotics.fetch.push import FetchPushEnv

class FetchPush(FetchPushEnv):
    def __init__(self,**args):
        self.epLength = 100 if '_max_episode_steps' not in args else args['_max_episode_steps']
        self.showRender = 0 if 'render' not in args else args['render']
            
        super().__init__(args)
        self.nSubtasks = 2
    

    def step(self, act):
        obs, reward, done, info = super().step(act)
        if self.showRender:
            super().render()
        grip_pos = self.sim.data.get_site_xpos('robot0:grip')
        object_pos = self.sim.data.get_site_xpos('object0')
        grip2Obj = -np.linalg.norm(grip_pos-object_pos)
        overallReward = grip2Obj + reward

        self.subtask = self.subtask if self.lastObjPos is None or abs(object_pos-self.lastObjPos) < 0.1 else 1
        self.lastObjPos = object_pos
        obs = obs['observation']
        info['overallReward'] = overallReward
        info['reward0'] = grip2Obj
        info['reward1'] = reward
        info['subtask'] = self.subtask
        self.epStep += 1
        done = self.epStep > self.epLength
        return obs, overallReward, done, info

    def reset(self):
        self.epStep = 0
        obs = super().reset()
        self.lastObjPos = None
        self.subtask = 0
        obs = obs['observation']
        return obs
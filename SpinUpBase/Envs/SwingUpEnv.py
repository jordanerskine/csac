import numpy as np, sys, time, gym


class SwingUpEnv:
    def __init__(self,**kwargs):
        self.kwargs = dict(angle_threshold = 0.8, reward_type = "dense", _max_episode_steps=None, headless=True)
        self.kwargs.update(kwargs)
        self.env = gym.make('Pendulum-v0')
        self.action_space = self.env.action_space
        self.observation_space = self.env.observation_space
        self.nSubtasks = 2
        self.env.reset()
        self.angle_threshold = self.kwargs['angle_threshold']
        self.reward_type = self.kwargs['reward_type']
        self._max_episode_steps = self.kwargs['_max_episode_steps']
        self.headless = self.kwargs['headless']

    def reset(self):
        self.ep_step = 0
        return self.env.reset()

    def step(self,action):
        obs, rew, done, _ = self.env.step(action)
        if not self.headless:
            self.render()
        st = self._subtask(obs)
        rews = self._genReward(obs)
        info = {}
        info['subtask'] = st
        info['reward0'] = rews[0]
        info['reward1'] = rews[1]
        info['overallReward'] = np.sum(rews)
        self.ep_step += 1
        done = done or self.ep_step >= self._max_episode_steps
        return obs, rews[st], done, info

    def render(self):
        self.env.render()

    def _genReward(self, obs):
        rewards = []
        rewards.append((obs[0]-1) if self.reward_type == "dense" else 0)
        rewards.append(1 if obs[0] > self.angle_threshold else 0)
        return rewards

    def _subtask(self, obs):
        if obs[0] > self.angle_threshold:
            return 1
        else:
            return 0


if __name__ == "__main__":
    env = SwingUpEnv()
    for _ in range(200):
        obs,_,_,_ = env.step(np.random.rand(1))
        env.render()

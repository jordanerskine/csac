import numpy as np, cv2, math
from gym import spaces
from copy import deepcopy as copy

def convex(a,b,ratio):
    return ratio*a+(1-ratio)*b

class UTurnEnv:
    def __init__(self, **kwargs):
        self.turningRate = 0.03
        self.speed = 1 if "speed" not in kwargs else kwargs["speed"]
        self.nSubtasks = 2 if "nSubtasks" not in kwargs else kwargs["nSubtasks"]
        self.includeSubtaskInObs = True if "includeSubtaskInObs" not in kwargs else kwargs["includeSubtaskInObs"]
        self.action_space = spaces.Box(low=-1, high=1, shape=[2])
        self.observation_space = spaces.Box(low=-1, high=1, shape=[3+2*self.nSubtasks+self.includeSubtaskInObs])

        self.targetWidth = 10

        self._max_episode_steps = 100 if '_max_episode_steps' not in kwargs else kwargs['_max_episode_steps']
        # print(self._max_episode_steps)
        self.headless = True if 'headless' not in kwargs else kwargs['headless']
        self.randomTargets = True if "randomTargets" not in kwargs or self.nSubtasks > 2 else kwargs["randomTargets"]
        self.randomInit = False if "randomInit" not in kwargs else kwargs["randomInit"]


        self.xBounds = (0,200)
        self.yBounds = (0,200)
        self.reset()

    def reset(self):
        if self.randomInit:
            self.position = self.getPosFromNorm(np.random.rand()*0.8+0.1,np.random.rand()*0.8+0.1)
            self.heading = np.random.rand()*math.pi*2
        else:
            self.position = self.getPosFromNorm(0.5,0.25)
            self.heading = math.pi/2
        if self.randomTargets:
            self.targets = []
            for i in range(self.nSubtasks):
                self.targets.append(self.getPosFromNorm(np.random.rand()*0.8+0.1,np.random.rand()*0.8+0.1))
        else:
            self.targets = [self.getPosFromNorm(0.5,0.75), self.getPosFromNorm(0.5,0.2)]
            self.targets = self.targets[:self.nSubtasks]
        self.subtask = 0
        self.success = False
        self.epStep = 0

        return self._obs()


    def getPosFromNorm(self, ratioX, ratioY):
        return (convex(self.xBounds[1],self.xBounds[0],ratioX),convex(self.yBounds[1],self.yBounds[0],ratioY))

    def checkOutOfBounds(self,point):
        out = False
        if point[0] < self.xBounds[0] or \
                point[0] > self.xBounds[1] or \
                point[1] < self.yBounds[0] or \
                point[1] > self.yBounds[1]:
            out = True
        return out

    def step(self, action):
        dX = action[0] * (1 if action[0] > 0 else 0.1)*self.speed
        dTheta = action[1]*dX*self.turningRate

        self.heading = self.heading + dTheta
        self.lastPosition = copy(self.position)
        self.position = (self.position[0]+dX*math.cos(self.heading), self.position[1]+dX*math.sin(self.heading))
        if self.checkOutOfBounds(self.position):
            self.position = self.lastPosition
        self.epStep += 1
        if not self.headless:
            self.render()

        rews = self._getRewards()
        obs = self._obs()
        subtask = self._getSubtask()
        success = self.success
        done = self.epStep >= self._max_episode_steps

        info = {"reward{}".format(n):rews[n] for n in range(len(rews))}
        info["overallReward"] = np.sum(rews)
        info["subtask"] = subtask
        info["success"] = success

        return obs,rews[subtask],done,info

    def render(self):
        img = np.zeros([self.xBounds[1],self.yBounds[1],3],dtype = np.uint8)
        iMax = len(self.targets)
        for i, target in enumerate(self.targets):
            point = tuple(int(tup) for tup in target)  
            img = cv2.circle(img,point,self.targetWidth, (int(255*i/iMax),0,int(255*(iMax-i)/iMax)),-1)
        point = tuple(int(tup) for tup in self.position)   
        img = cv2.circle(img, point, 5, (255,255,255),-1)
        dirPoint = (int(self.position[0]+10*math.cos(self.heading)),
                    int(self.position[1]+10*math.sin(self.heading)))
        img = cv2.arrowedLine(img,point, dirPoint,(255,255,255),1)
        # cv2.namedWindow("Driving", cv2.WINDOW_NORMAL)
        cv2.imshow("Driving", img)
        cv2.waitKey(1)



    def _getRewards(self):
        rews = []
        for i, target in enumerate(self.targets):
            if i < self.subtask:
                rew = 1
            elif i > self.subtask:
                rew = 0
            else:
                dist = np.linalg.norm(np.array(target)-np.array(self.position))
                rew = math.exp(-dist/20)
            rews.append(rew)
        return rews

    def _getSubtask(self):
        if np.linalg.norm(np.array(self.targets[self.subtask])-np.array(self.position)) < self.targetWidth:
            if self.subtask == self.nSubtasks -1:
                self.success = True
            else:
                self.subtask += 1
        return self.subtask

    def _obs(self):
        obs = [np.array(self.position),np.array([self.heading])]
        for target in self.targets:
            obs.append(np.array(target))
        if self.includeSubtaskInObs:
            obs.append(np.array([self.subtask]))
        obs = np.concatenate(obs)
        return obs



if  __name__ == '__main__':
    env = UTurnEnv(headless = False, _max_episode_step = 100)
    done = False
    while not done:
        obs,rew,done,_ = env.step([1,0])
        print(f'Rew: {rew}')
        print(f'Pos & headings: {obs[:3]}')

from os.path import dirname, join, abspath
from pyrep import PyRep
from pyrep.robots.arms.panda import Panda
from pyrep.robots.end_effectors.panda_gripper import PandaGripper
from pyrep.objects.shape import Shape
import numpy as np
from gym import spaces
import math

SCENE_FILE = join(dirname(abspath(__file__))[:-4],
                  'Models/scene_reinforcement_learning_env.ttt')
# POS_MIN, POS_MAX = [0.0, -0.8, 1.0], [1.0, 0.8, 1.8]
POS_MIN, POS_MAX = [0.8, -0.2, 0.8], [1.2, 0.2, 1.2]
EPISODES = 5
EPISODE_LENGTH = 200


class ReacherEnv(object):

    def __init__(self,**kwargs):
        self.headless = True if 'headless' not in kwargs or kwargs['headless'] == 1 else False
        self.sparse = False if 'sparse' not in kwargs or kwargs['sparse'] == 0 else True 
        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=self.headless)
        self.pr.start()
        self.agent = Panda()
        # self.gripper = PandaGripper()
        self.agent.set_control_loop_enabled(False)
        self.agent.set_motor_locked_at_zero_velocity(True)
        self.target = Shape('target')
        self.agent_ee_tip = self.agent.get_tip()
        self.initial_joint_positions = self.agent.get_joint_positions()
        self.action_space = spaces.Box(low=np.array([-1, -1, -1, -1, -1, -1, -1]), high=np.array([1, 1, 1, 1, 1, 1, 1]), dtype=np.float64)
        self.observation_space = spaces.Box(low=-1, high=1, shape=[20])
        self.nSubtasks = 1
        self.eps_since_reset = 0
        self._max_episode_steps = 1000

    def _reinit(self):
        self.pr.shutdown()
        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=self.headless)
        self.pr.start()
        self.agent = Panda()
        self.agent.set_control_loop_enabled(False)
        self.agent.set_motor_locked_at_zero_velocity(True)
        self.agent_ee_tip = self.agent.get_tip()
        self.initial_joint_positions = self.agent.get_joint_positions()
        self.eps_since_reset = 0

    def _get_state(self):
        # Return state containing arm joint angles/velocities & target position
        ePos = self.agent_ee_tip.get_position()
        tPos = self.target.get_position()
        return np.concatenate([self.agent.get_joint_positions(),
                               self.agent.get_joint_velocities(),
                               ePos,
                               tPos-ePos])

    def reset(self):
        # Get a random position within a cuboid and set the target position
        pos = list(np.random.uniform(POS_MIN, POS_MAX))
        self.target.set_position(pos)
        self.agent.set_joint_positions(self.initial_joint_positions)
        self.ep_step = 0
        self.eps_since_reset += 1
        if self.eps_since_reset >= 100:
            self._reinit()
        return self._get_state()

    def gen_reward(self):
        ePos = self.agent_ee_tip.get_position()
        tPos = self.target.get_position()
        # Reward is negative distance to target
        dist = np.linalg.norm(ePos-tPos)

        # if dist < 0.2:
        #     reward = 10
        # else:
        #     reward = math.exp(-3*dist)
        if self.sparse:
            reward = 1.0 if dist < 0.1 else 0.0
        else:
            reward = math.exp(-3*dist)

        return reward

    def step(self, action):
        # print(action)
        self.agent.set_joint_target_velocities(action)  # Execute action on arm
        # self.gripper.actuate(action[-1])
        self.pr.step()  # Step the physics simulation

        self.ep_step += 1
        
        reward = self.gen_reward()

        # print(reward)
        done = True if self.ep_step >= self._max_episode_steps else False

        info = {"reward0":reward,
                "overallReward":reward,
                "subtask":0,
                "success":1 if reward > 0.9 else 0}
        return self._get_state(), reward, done, info

    def shutdown(self):
        self.pr.stop()
        self.pr.shutdown()


class Agent(object):

    def act(self, state):
        del state
        return list(np.random.uniform(-1.0, 1.0, size=(7,)))

    def learn(self, replay_buffer):
        del replay_buffer
        pass

if __name__ == '__main__':
    env = ReacherEnv(headless=False)
    agent = Agent()
    replay_buffer = []

    for e in range(EPISODES):

        print('Starting episode %d' % e)
        state = env.reset()
        for i in range(EPISODE_LENGTH):
            action = agent.act(state)
            
            reward, next_state,_,_ = env.step(action)
            replay_buffer.append((state, action, reward, next_state))
            state = next_state
            agent.learn(replay_buffer)

    print('Done!')
    env.shutdown()
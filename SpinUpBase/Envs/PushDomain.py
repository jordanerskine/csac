from gym.envs import robotics
from gym.spaces import Box
import numpy as np, time, sys, torch, json
from CooperativeSACImplementation import rollout as coopRollout
from rlkit.samplers.rollout_functions import rollout as singleRollout

def arg(tag, default):
    if tag in sys.argv:
        val = type(default)((sys.argv[sys.argv.index(tag)+1]))
    else:
        val = default
    return val


class PushDomain():
    def __init__(self,reward_type = "distance",singleAgent = False, threshold = 0.15):
        self.env = robotics.FetchPushEnv(reward_type=reward_type)
        ob = self.reset()
        self.observation_space = Box(low = np.full(len(ob),-float('inf')),
                                     high = np.full(len(ob),float('inf')))
        self.action_space = Box(low = np.full(3,float(-1)),high = np.full(3,float(1)))
        self.nSubtasks = 2
        self.singleAgent = singleAgent
        self.reward_type = reward_type
        self.threshold = threshold

    def reset(self):
        ob = self.env.reset()
        self.lastGripPos = self.env.sim.data.get_site_xpos('robot0:grip').copy()
        self.lastObjPos = self.env.sim.data.get_site_xpos('object0').copy()
        return self._combineObs(ob)
    
    def step(self,action):
        obs, reward, done, info =  self.env.step(np.concatenate([action,[0]]))
        reward = self._genReward()
        # print("Reward:{}".format(reward))
        done = self.env._is_success(self.env.sim.data.get_site_xpos('object0'),self.env.goal) and self.reward_type == "delta"
        st = self._getSubtask()
        info['subtask'] = st
        info['reward0'] = reward[0]
        info['reward1'] = reward[1]
        info['overallReward'] = np.sum(reward)
        rew = reward[st] if not self.singleAgent else np.sum(reward)

        return self._combineObs(obs), rew, done, info

    def render(self):
        self.env.render()

    def _combineObs(self,obs):
        return np.concatenate([obs[key] for key in obs])

    def _getSubtask(self):
        return int(np.linalg.norm(self.lastObjPos-self.lastGripPos)<self.threshold)

    def _genReward(self):
        objPos = self.env.sim.data.get_site_xpos('object0')
        # print("ObjPos:{}".format(objPos))
        grip_pos = self.env.sim.data.get_site_xpos('robot0:grip')
        # print("GripPos:{}".format(grip_pos))
        goal = self.env.goal
        # print("Goal:{}".format(goal))
        if self.reward_type == "delta":
            deltaGrip = np.linalg.norm(self.lastGripPos-self.lastObjPos) - np.linalg.norm(grip_pos-objPos)
            deltaObj = np.linalg.norm(self.lastObjPos-goal) - np.linalg.norm(objPos-goal)
            reward = [deltaGrip,deltaObj]
        elif self.reward_type == "distance":
            distGrip = -np.linalg.norm(grip_pos-objPos)
            distObj = -np.linalg.norm(objPos-goal)
            reward = [distGrip,distObj]
        elif self.reward_type == "sparse":
            distGrip = 1 if np.linalg.norm(grip_pos-objPos) < self.threshold else 0
            distObj = 1 if np.linalg.norm(objPos-goal) < self.threshold else 0
            reward = [distGrip,distObj]
        self.lastGripPos = grip_pos.copy()
        self.lastObjPos = objPos.copy()
        return reward




if __name__ == "__main__":
    
    env = PushDomain()
    print(env.action_space)
    path = arg('--loadPath','None')
    if path == 'None':
        count = 0
        while count < 100:
            env.step(np.random.rand(4))
            env.render()
            count += 1
    else:
        data = torch.load("{}/params.pkl".format(path))

        policies = data['exploration/policy']
        with open("{}/variant.json".format(path),'r') as f:
            variant = json.load(f)
        for _ in range(5):
            if variant['mode'] == 'singleAgent':
                singleRollout(env,policies, max_path_length=200,render=True)
            else:
                coopRollout(env,policies, max_path_length=200,render=True)
    

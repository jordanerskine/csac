#!/usr/bin/env python3

from xmlrpc.client import Boolean
import rospy
import tf2_ros
import actionlib
import numpy as np, sys, math, torch
import torch.nn as nn
import cv2
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib
from utils import rotateAroundPoint as rap

from cv_bridge import CvBridge

from collections import deque

from copy import deepcopy as copy
from gym import spaces

from collections import deque

from subprocess import call
import time
from shapely.geometry import Point, Polygon

matplotlib.use('TKAgg')
# import bezier

# from trajectory_generator import TrajectoryGenerator

# from pyquaternion import Quaternion as Q



def showImage(image,wait = 0):
    cv2.imshow("image",image)
    cv2.waitKey(wait)


def smooth(val,lastVal,maxChange):
  return lastVal+np.clip(val-lastVal,-maxChange,maxChange)

def angle_diff(angle1,angle2):
  a = angle1 - angle2
  a -= math.pi*2 if a > math.pi else 0
  a += math.pi*2 if a < -math.pi else 0
  return a

class Expert:
  def __init__(self,noise=0.02, graspLoc = 0.03, wrenchLen = 0.03, target = [0.59,0.09], angleAction = False):
    self.target = target
    self.graspLoc = graspLoc
    self.noise = noise
    self.wrenchLen = wrenchLen
    self.angleAction = angleAction
    

  def get_actions(self,o):
    device = o.device
    actions = [self.get_action(obs) for obs in o.cpu()]
    actions = torch.tensor(actions,device = device,dtype = o.dtype)
    return actions

  def get_action(self,observation):
    obs = observation
    if not obs[-1] and not obs[-2]:
      targetPoint = [obs[-6]+0.9*self.wrenchLen*obs[-4],obs[-5]+self.wrenchLen*obs[-3]]
    elif not obs[-1]:
      targetPoint =[self.target[0],self.target[1]-1.8*self.wrenchLen]
    else:
      targetPoint =[self.target[0]-1.8*self.wrenchLen,self.target[1]]

    angleVel = 0.2*angle_diff(math.atan2(obs[-4],obs[-3]),math.atan2(self.target[0]-obs[-6],self.target[1]-obs[-5]))
    if self.angleAction:
      action = np.clip(5*np.array([targetPoint[0]-obs[0],targetPoint[1]-obs[1],angleVel]),-0.5,0.5)
    else:
      action = np.clip(5*np.array([targetPoint[0]-obs[0],targetPoint[1]-obs[1]]),-0.5,0.5)

    action = np.clip(np.random.normal(action,self.noise),-1,1)
    return action

class Wrench:
  def __init__(self, **args):

    self._max_episode_steps = 20 if '_max_episode_steps' not in args else args['_max_episode_steps']
    self.freeWrenchAngle = False if 'freeWrenchAngle' not in args else args['freeWrenchAngle']
    self.useVel = True if 'useVel' not in args else args['useVel']
    self.randomStart = True if 'randomStart' not in args else args['randomStart']
    self.endOnOutOfBounds = 0 if 'endOnOutOfBounds' not in args else args['endOnOutOfBounds']
    self.rewardType = 'dense' if 'rewardType' not in args else args['rewardType']
    self.headless = True if 'headless' not in args else args['headless']
    self.subtaskLinger = 0 if 'subtaskLinger' not in args else args['subtaskLinger']
    self.wrenchLength = 0.03 if 'wrenchLength' not in args else args['wrenchLength']
    self.wrenchWidth = 0.003 if 'wrenchWidth' not in args else args['wrenchWidth']
    self.turnedDistThresh = 1.3 if 'turnedDistThresh' not in args else args['turnedDistThresh']
    self.objectVis = 'Wrench' if 'objectVis' not in args else args['objectVis']

    self.cutOffCorner = 0.05 if 'cutOffCorner' not in args else args['cutOffCorner']

    corners = [[0.45,-0.1],[0.60,0.1]] if 'corners' not in args else args['corners']

    if isinstance(corners,str):
      corners = eval(corners)
    print(corners)

    max_speed = 0.1 if 'maxSpeed' not in args else args['maxSpeed']


    self.error = 0
    self.epStep = 0
    self.grasped = False

    self.graspedLocations = deque(maxlen = 1000)

    

    self.bounds = {'x':[corners[0][0],corners[1][0]],
                   'y':[corners[0][1],corners[1][1]]}
    self.homePose = [0.532,-0.037,0.279]

    self.target = {'x':0.59,
                   'y':0.09}


    # action parameters
    self.acceleration_constraint = 0.1 #m/s/s
    self.max_speed = max_speed #m/s
    self.max_angle_speed = 0.5 #rad/s
    self.hz = 100
    self.actHz = 5 #Hz
    
    self.stepScale = 1/self.hz


    self.nSubtasks = 3

    # self.bounds = [self.xLim, self.yLim, self.zLim]
    self.ee_pose = np.array([0,0,0])
    self.frame = None
    self.gripper_position = None
    self.moveStatus = None
    self.graspStatus = None
    self.failedToReset = False

    # render settings
    self.w = 600
    self.h = 600

    self.bg_image = cv2.resize(cv2.cvtColor(np.random.randint(225,256,(self.w//8,self.h//8)).astype(np.uint8), cv2.COLOR_GRAY2BGR), dsize=(self.h,self.w), interpolation=cv2.INTER_NEAREST)
    


    self.actSpace = 2
    self.actSpace += 1 if self.freeWrenchAngle else 0
    self.obsSpace = 8
    self.obsSpace += 2 if self.useVel else 0

    self.action_space = spaces.Box(low=np.array([-1 for _ in range(self.actSpace)]),
                                   high=np.array([1 for _ in range(self.actSpace)]),dtype=np.float64)
    
    self.observation_space = spaces.Box(low=-1,high=1,shape=[self.obsSpace])

    
    currentObs = copy(self.ee_pose)
    lastObs = currentObs+1
    self.episode = 1
    self.lastEval = False
    self.graspedThisEp = False
    self.reset()


  def reset(self,eval = False):
    self.epStep = 0
    self.lastAction = np.zeros(self.actSpace-1)
    self.missed_grasp = False
    self.initAttachAngle = None

    border = 0.02
    self.wrenchPos = {'x':np.random.uniform(self.bounds['x'][0]+border,self.bounds['x'][1]-border),
                   'y':np.random.uniform(self.bounds['y'][0]+border,self.bounds['y'][1]-border)}
    if self.freeWrenchAngle:
      self.wrenchAngle = np.random.uniform(0,2*math.pi)

    self.grasped = 0
    self.attached = 0
    self.graspedThisEp = 0
    
    self.pos = {'x':np.random.uniform(self.bounds['x'][0]+border,self.bounds['x'][1]-border),
                'y':np.random.uniform(self.bounds['y'][0]+border,self.bounds['y'][1]-border)}
    self.vel = {'x':0,'y':0}
    self.epStep = 0
    self.lastAction = np.zeros(self.actSpace)
    self.episode += 1

    if eval and not self.lastEval:
      self.showGraspDistribution()
    self.lastEval = eval
    self.lingerDuration = [0,0]


    self.angle2Target = math.atan2(self.wrenchPos['x']-self.target['x'],self.wrenchPos['y'] - self.target['y'])
    if self.freeWrenchAngle:
      self.wrenchAngle = np.random.uniform(0,2*math.pi)

    return self._obs()


  def _obs(self):
    obs = np.array([p for p in self.pos.values()])
    if self.useVel:
      obs = np.concatenate([obs,np.array([vel/self.stepScale for vel in self.vel.values()])])
    obs = np.concatenate([obs,np.array([pos for pos in self.wrenchPos.values()])])
    obs = np.concatenate([obs, np.array([math.sin(self.wrenchAngle),math.cos(self.wrenchAngle)])])
    obs = np.concatenate([obs,np.array([self.grasped, self.attached])])
    
    return obs

  def reward(self,type = "total"):
    reachDist = np.linalg.norm([pos-wrench for pos,wrench in zip(self.pos.values(),self.wrenchPos.values())])
    attachDist = np.linalg.norm([self.wrenchPos['x']-self.target['x'],self.wrenchPos['y']-np.mean(self.target['y'])])
    turnDist = 0 if self.initAttachAngle is None else abs(angle_diff(self.angle2Target,self.initAttachAngle))
    if self.rewardType == "dense":
      attachRew = math.exp(-5*attachDist)
      allRew = {"reach":math.exp(-5*reachDist),
                "attach":attachRew,
                "turn":0.5*turnDist+0.5*(turnDist > self.turnedDistThresh)}
      totalRew = np.sum([allRew[r] for r in allRew])
    elif self.rewardType == "fullSparse":
      allRew = {"reach":0,
                "attach":0,
                "turn":turnDist > self.turnedDistThresh}
      totalRew = np.sum([allRew[r] for r in allRew])
    else:
      allRew = {"reach":1*self.grasped,
                "attach":self.attached,
                "turn":turnDist > self.turnedDistThresh}
      totalRew = np.sum([allRew[r] for r in allRew])
      
    if type == "total":
      return totalRew
    elif type == "all":
      return allRew

  def _checkOutBounds(self):
    if np.any([pos < bound[0] or pos > bound[1] for pos,bound in zip(self.pos.values(),self.bounds.values())]):
        return True
    if self.cutOffCorner > 0:
          posVal = self.pos['x'] + self.pos['y']-self.cutOffCorner
          boundVal = self.bounds['x'][1]+self.bounds['y'][1]-self.cutOffCorner
          if posVal > boundVal:
              return True
    return False

  def smooth_action(self,act):
    accel_constraint = self.acceleration_constraint/self.hz
    action = smooth(act,self.lastAction,accel_constraint)
    self.lastAction = action
    return action

  def grasp(self, grasp = True):
    points = [[self.wrenchPos['x']+self.wrenchLength*math.sin(self.wrenchAngle)-self.wrenchWidth*math.cos(self.wrenchAngle),
                    self.wrenchPos['y']+self.wrenchLength*math.cos(self.wrenchAngle)+self.wrenchWidth*math.sin(self.wrenchAngle)],
                  [self.wrenchPos['x']+self.wrenchLength*math.sin(self.wrenchAngle)+self.wrenchWidth*math.cos(self.wrenchAngle),
                    self.wrenchPos['y']+self.wrenchLength*math.cos(self.wrenchAngle)-self.wrenchWidth*math.sin(self.wrenchAngle)],
                  [self.wrenchPos['x']-self.wrenchLength*math.sin(self.wrenchAngle)+self.wrenchWidth*math.cos(self.wrenchAngle),
                    self.wrenchPos['y']-self.wrenchLength*math.cos(self.wrenchAngle)-self.wrenchWidth*math.sin(self.wrenchAngle)],
                  [self.wrenchPos['x']-self.wrenchLength*math.sin(self.wrenchAngle)-self.wrenchWidth*math.cos(self.wrenchAngle),
                    self.wrenchPos['y']-self.wrenchLength*math.cos(self.wrenchAngle)+self.wrenchWidth*math.sin(self.wrenchAngle)]]
    wrench = Polygon(points)
    pos = Point(self.pos['x'], self.pos['y'])
    if pos.within(wrench):
      if not self.grasped and grasp:
        self.relativeOffset = {'radius':np.linalg.norm([self.wrenchPos['x']-self.pos['x'],self.wrenchPos['y']-self.pos['y']]),
                               'angle':angle_diff(self.wrenchAngle+math.pi,math.atan2(self.pos['x']-self.wrenchPos['x'],self.pos['y'] - self.wrenchPos['y']))}
      self.grasped = int(grasp)
      # if self.grasped and not self.graspedThisEp:
      #   self.graspedLocations.append([peg-pos for peg,pos in zip(self.wrenchPos.values(),self.pos.values())])

      self.graspedThisEp = int(self.graspedThisEp or self.grasped)



  def showGraspDistribution(self, getHist = False):
    increment = 0.005
    print("Grasp Distribution")
    successes = []
    failures = []
    for point in np.arange(-0.05,0.05,increment):
      success = np.sum([1 if loc[1] else 0 for loc in self.graspedLocations if np.linalg.norm(loc[0]-point) < increment/2])
      failure = np.sum([0 if loc[1] else 1 for loc in self.graspedLocations if np.linalg.norm(loc[0]-point) < increment/2])
      successes.append(success)
      failures.append(failure)
      print(f"   Point {point:.3f}: Successes - {success} | Failures - {failure} | Success Rate - {success/(success+failure)*100:.1f}%")
    if getHist:
      self.plotGraspHistogram(successes,failures)
    

  def step(self, act):

    if not self.graspedThisEp:
        self.grasp()
      
    for _ in range(int(self.hz/self.actHz)):
      sm_act = self.smooth_action(np.array([act*self.max_speed]))[0]
      self.vel['x'] = sm_act[0]*self.stepScale
      self.vel['y'] = sm_act[1]*self.stepScale
      if self.freeWrenchAngle:
        angleVel = sm_act[2]*self.stepScale*5

      if self.attached:
        grip2target = math.atan2(self.pos['x']-self.target['x'],self.pos['y'] - self.target['y'])
        velAngle = math.atan2(self.vel['x'],self.vel['y'])
        velMag = np.linalg.norm([self.vel['x'],self.vel['y']])
        velMag *= math.sin(angle_diff(velAngle,grip2target))
        self.vel['x'] = velMag * math.sin(grip2target+math.pi/2)
        self.vel['y'] = velMag * math.cos(grip2target+math.pi/2)
        self.wrenchAngle = self.angle2Target
      
      if not self.endOnOutOfBounds:
        axis = 'x'
        for pos,bound in zip(self.pos.values(),self.bounds.values()):
            if pos < bound[0]:
                self.vel[axis] = max(0,self.vel[axis])
            elif pos > bound[1]:
                self.vel[axis] = min(0,self.vel[axis])
            if self.attached and self.vel[axis] == 0:
              self.vel['y' if axis == 'x' else 'x'] = 0
              
            axis = 'y'
        if self.cutOffCorner > 0:
          posVal = self.pos['x'] + self.pos['y']
          boundVal = self.bounds['x'][1]+self.bounds['y'][1]-self.cutOffCorner
          if posVal > boundVal:
            if self.vel['x']+self.vel['y'] > 0:
              self.vel['x'] = 0
              self.vel['y'] = 0
      
      self.pos['x'] += self.vel['x']
      self.pos['y'] += self.vel['y']
      if self.grasped:
        self.wrenchPos['x'] = self.pos['x']+self.relativeOffset['radius']*math.sin(self.wrenchAngle+self.relativeOffset['angle'])
        self.wrenchPos['y'] = self.pos['y']+self.relativeOffset['radius']*math.cos(self.wrenchAngle+self.relativeOffset['angle'])
        if self.freeWrenchAngle and not self.attached:
          self.wrenchAngle += angleVel
          

      self.angle2Target = math.atan2(self.wrenchPos['x']-self.target['x'],self.wrenchPos['y'] - self.target['y'])
      wrenchDist = np.linalg.norm([self.wrenchPos['x']-self.target['x'],self.wrenchPos['y']-self.target['y']])
      gripDist = np.linalg.norm([self.pos['x']-self.target['x'],self.pos['y']-self.target['y']])
      if self.grasped and not self.attached and \
                abs(wrenchDist - self.wrenchLength) < 0.003 and \
                abs(angle_diff(self.wrenchAngle,self.angle2Target)) < 0.05: #gripDist > self.wrenchLength*0.3
        self.attached = 1
        self.initAttachAngle = self.angle2Target


    if not self.headless:
      self.render()
    
    # if self.graspAfter and not self.graspedThisEp:
    #   self.grasp()

    subtask = self._getSubtask()


    self.epStep += 1

    obs = self._obs()
    rews = list(self.reward("all").values())
    done = self._checkDone()
    info = {}
    info['overallReward'] = self.reward()
    info['reachReward'] = self.reward("all")["reach"]
    info['attachReward'] = self.reward("all")["attach"]
    info['turnReward'] = self.reward("all")["turn"]
    info['subtask'] = subtask
    info['success'] = self._success()

    # if self._success():
    #   self.graspedLocations[-1][1] = True

    return obs, rews[subtask], done, info


  def render(self):
    def w2p(x,y, tup = True):
      edge = 0.05
      xOut = int((x-self.bounds['x'][0]+edge)/(self.bounds['x'][1]-self.bounds['x'][0]+2*edge)*self.w)
      yOut = int((y-self.bounds['y'][0]+edge)/(self.bounds['y'][1]-self.bounds['y'][0]+2*edge)*self.h)
      if tup:
        return tuple(np.array([xOut,yOut]))
      return np.array([xOut,yOut])
    image = self.bg_image.copy()


    

    # insert bounds
    image = cv2.line(image,w2p(self.bounds['x'][0],self.bounds['y'][0]),w2p(self.bounds['x'][0],self.bounds['y'][1]),(100,100,250),2)
    image = cv2.line(image,w2p(self.bounds['x'][0],self.bounds['y'][1]),w2p(self.bounds['x'][1]-self.cutOffCorner,self.bounds['y'][1]),(100,100,250),2)

    image = cv2.line(image,w2p(self.bounds['x'][1]-self.cutOffCorner,self.bounds['y'][1]),w2p(self.bounds['x'][1],self.bounds['y'][1]-self.cutOffCorner),(100,100,250),2)

    image = cv2.line(image,w2p(self.bounds['x'][1],self.bounds['y'][1]-self.cutOffCorner),w2p(self.bounds['x'][1],self.bounds['y'][0]),(100,100,250),2)
    image = cv2.line(image,w2p(self.bounds['x'][1],self.bounds['y'][0]),w2p(self.bounds['x'][0],self.bounds['y'][0]),(100,100,250),2)


    # draw wrench
    if not self.freeWrenchAngle:
      self.wrenchAngle = self.angle2Target
    if self.objectVis == "Wrench":
      ps = [[1,1],[1,-1],[-0.7,-1],[-0.9,-2],[-1.1,-2],[-1.2,-0.8],[-0.9,-0.8],[-0.9,0.8],[-1.2,0.8],[-1.1,2],[-0.9,2],[-0.7,1]]
    else:
      ps = [[1,1],[1,-1],[-1,-1],[-1,1]]
    points = [w2p(self.wrenchPos['x']+p[0]*self.wrenchLength*math.sin(self.wrenchAngle)-p[1]*self.wrenchWidth*math.cos(self.wrenchAngle),
                  self.wrenchPos['y']+p[0]*self.wrenchLength*math.cos(self.wrenchAngle)+p[1]*self.wrenchWidth*math.sin(self.wrenchAngle))
                    for p in ps]
    image = cv2.fillPoly(image,pts = [np.array(points, dtype=int)],color = (150,150,150))
    image = cv2.polylines(image,pts = [np.array(points, dtype=int)],isClosed = 1,color = (0,0,0),thickness=2)

    # insert target
    # image = cv2.rectangle(image,tuple(w2p(self.target['x'][0],self.target['y'][0])),tuple(w2p(self.target['x'][1],self.target['y'][1])),(150,150,150),-1)
    image = cv2.circle(image,w2p(self.target['x'],self.target['y']),10,(100,100,100),-1)
    image = cv2.circle(image,w2p(self.target['x'],self.target['y']),10,(0,0,0),2)
    rotateAngle = 0 if self.initAttachAngle is None else angle_diff(self.angle2Target,self.initAttachAngle)
    screwLen = 0.003
    image = cv2.line(image,w2p(self.target['x']+screwLen*math.sin(rotateAngle),self.target['y']+screwLen*math.cos(rotateAngle)),
                          w2p(self.target['x']-screwLen*math.sin(rotateAngle),self.target['y']-screwLen*math.cos(rotateAngle)),(0,0,0),2)
    rotateAngle += math.pi/2
    image = cv2.line(image,w2p(self.target['x']+screwLen*math.sin(rotateAngle),self.target['y']+screwLen*math.cos(rotateAngle)),
                          w2p(self.target['x']-screwLen*math.sin(rotateAngle),self.target['y']-screwLen*math.cos(rotateAngle)),(0,0,0),2)
    

    # draw gripper
    colour = (200,250,200) if self.grasped else (200,200,250)
    radius = (15 if self.grasped else 20)
    image = cv2.circle(image,w2p(self.pos['x'],self.pos['y']),radius,colour,-1)
    image = cv2.circle(image,w2p(self.pos['x'],self.pos['y']),radius,[50,50,50],2)

    # image = cv2.rotate(image,cv2.ROTATE_90_COUNTERCLOCKWISE)

    image = cv2.putText(image,f"Timestep: {self.epStep}",(10,10),cv2.FONT_HERSHEY_PLAIN,1,(100,100,100),2)

    if self._success():
      image = cv2.putText(image,"Successful",(10,25),cv2.FONT_HERSHEY_PLAIN,1,(0,0,0),2)

    cv2.imshow("Wrench-Sim",image)
    cv2.waitKey(30)

  def _getSubtask(self):
    if self.attached:
      if self.subtaskLinger <= self.lingerDuration[1]:
        return 2
      else:
        self.lingerDuration[1] += 1
        return 1
    elif self.grasped:
      if self.subtaskLinger <= self.lingerDuration[0]:
        return 1
      else:
        self.lingerDuration[0] += 1
      
    return 0

  def _success(self):
    if self.initAttachAngle is not None:
      turnedDist = angle_diff(self.angle2Target,self.initAttachAngle)
      return turnedDist > abs(self.turnedDistThresh)
    return False

  def _checkDone(self):
    return (self.epStep >= self._max_episode_steps or self.error > 0 or (self._checkOutBounds() and self.endOnOutOfBounds))
    

if __name__ == "__main__":
  env = Wrench(_max_episode_steps = 100,endOnOutOfBounds = 1,objectVis= "Wrench",freeWrenchAngle=1, cutOffCorner=0.07)#corners=[[0.33,-0.22],[0.73,0.18]]

  exp = Expert(noise = 0,angleAction=1)#target=0.73
  nEpisodes = 10
  for n in range(nEpisodes):
    
    done = False
    obs = env.reset()
    length = 0
    totalRew = 0
    while not done:

      # action = actor(torch.Tensor(obs))
      # print(action)
      action = exp.get_action(obs)
      nobs, rew, done, info = env.step(action)
      env.render()
      totalRew += rew
      # print("action: ",action)
      # print(f"position: {nobs[0]}, peg position: {nobs[-2]}")
      print(f"Success: {info['success']}")

      length += 1
      if length >= 200:
        done = True

      obs = nobs
      # print(length)

    print("Episode {}: \nOut of Bounds at termination: {}\nLength: {} \nAverage Reward: {}\n------".format(n,env._checkOutBounds(),length,totalRew/length))

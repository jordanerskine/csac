import numpy as np

from panda_gym.envs.core import Task, RobotTaskEnv
from panda_gym.envs.robots.panda import Panda
from panda_gym.utils import distance
from panda_gym.pybullet import PyBullet
from gym import spaces

import gym
import math
from gym.envs.registration import register

for reward_type in ["sparse", "dense"]:
    for control_type in ["ee", "joints"]:
        kwargs = {"reward_type": reward_type, "control_type": control_type}

        register(
            id="PandaCanRamp{}{}-v1".format(control_type, reward_type),
            entry_point="jordan_custom_envs.Envs:CanRampEnv",
            kwargs=kwargs,
            max_episode_steps=50,
        )


class CanRampTask(Task):
    def __init__(self, sim, reward_type,random_can):
        super().__init__(sim)
        self.reward_type = reward_type
        self.random_can = random_can

        self.distance_threshold = 0.05

        self.create_scene()
        self.reset()

    def create_scene(self):

        self.sim.create_table(length = 1.1, width = 0.9, height = 0.4, x_offset=-0.3)
        # self.sim.create_box('rampBase', np.array([0.15, 0.04, 0.05]), 100, np.array([0.0, 0.4, 0.05]))

        self.sim.create_box('ramp', np.array([0.15, 0.3, 0.002]), 0, np.array([0.0,0.0,0.0]))
        self.sim.set_base_pose('ramp', np.array([0.0, 0.15, 0.06]), np.array([0.10, 0,0,(1-0.10**2)**0.5]))

        self.sim.create_box('wall1',np.array([0.002,0.45,0.15]),0,np.array([-0.15,0.0,0.05]))
        self.sim.create_box('wall2',np.array([0.002,0.45,0.15]),0,np.array([0.15,0.0,0.05]))
        self.sim.create_box('wall3',np.array([0.15,0.002,0.15]),0,np.array([0.0,0.45,0.05]))
        self.sim.create_box('wall4',np.array([0.15,0.002,0.15]),0,np.array([0.0,-0.45,0.05]))

        self.canPose = np.array([0.0, -0.15, 0.04])
        self.sim.create_cylinder("can", 0.025, 0.05, 0.1, self.canPose,np.array([0.1, 0.1, 0.9 ,1.0]))
        self.sim.create_box("target", np.array([0.15, 0.05, 0.05]), 0.0, np.array([0.0, 0.3, 0.1]), 
                                            np.array([0.1, 0.9, 0.1, 0.3]), ghost = True)

    def reset(self):
        self.goal = self.sim.get_base_position("target")
        if self.random_can:
            canPose = self.canPose
            canPose[0] += np.random.uniform(-0.1,0.1)
            canPose[1] += np.random.uniform(-0.2,0)
            self.sim.set_base_pose("can", canPose, np.array([0.0, 0.0, 0.0, 1.0]))
        else:
            self.sim.set_base_pose("can", self.canPose, np.array([0.0, 0.0, 0.0, 1.0]))

        self.sim.set_base_pose('ramp', np.array([0.0, 0.15, 0.065]), np.array([0.10, 0,0,(1-0.10**2)**0.5]))
        # self.sim.set_base_pose('rampBase', np.array([0.0, 0.4, 0.05]), np.array([0.0, 0.0, 0.0, 1.0]))

    def get_obs(self):
        object_position = self.sim.get_base_position("can")
        object_rotation = self.sim.get_base_rotation("can")
        object_velocity = self.sim.get_base_velocity("can")
        object_angular_velocity = self.sim.get_base_angular_velocity("can")
        observation = np.concatenate([object_position, object_rotation, object_velocity, object_angular_velocity])
        return observation

    def get_achieved_goal(self):
        return np.array(self.sim.get_base_position("can"))

    def is_success(self, achieved_goal, desired_goal, info={}):
        d = distance(achieved_goal,desired_goal)
        return np.array(d < self.distance_threshold, dtype=float)

    def get_goal(self):
        return np.array(self.sim.get_base_position("target"))
    
    def compute_reward(self, achieved_goal, desired_goal, info={}):
        d = distance(achieved_goal,desired_goal)
        if self.reward_type == "sparse":
            return float(d < self.distance_threshold)
        else:
            return math.exp(-d*3)

class CanRampEnv(RobotTaskEnv):
    def __init__(self, render=False, reward_type = "sparse", control_type = "ee", random_can = False):
        sim = PyBullet(render = render)
        robot = Panda(sim, False, np.array([-0.6,0.0,0.0]), control_type=control_type)
        task = CanRampTask(sim, reward_type = reward_type,random_can = random_can)
        super().__init__(robot, task)

class PandaCan():
    def __init__(self,**args):
        self.epLength = 100 if '_max_episode_steps' not in args else args['_max_episode_steps']
        self.render = 0 if 'render' not in args else args['render']
        self.rewardType = "sparse" if 'rewardType' not in args else args['rewardType']
        self.controlType = "ee" if 'controlType' not in args else args['controlType']
        self.singleAgent = False if 'singleAgent' not in args else args['singleAgent']
        self.random_can = False if 'random_can' not in args else args['random_can']
        # self.env = gym.make(f'PandaCanRamp{self.controlType}{self.rewardType}-v1', render = self.render)
        self.env = CanRampEnv(self.render, self.rewardType, self.controlType)
        self.nSubtasks = 2

        actSize = len(self.env.action_space.sample())
        obsSize = len(self.reset())
        self.action_space = spaces.Box(low=-1, high=1, shape=[actSize])
        self.observation_space = spaces.Box(low=-1, high=1, shape=[obsSize])
            
    

    def step(self, act):
        envObs, envRew, done, envInfo = self.env.step(act)
        obs = envObs['observation']
        objPos = obs[7:10]
        rew = []
        rew.append(self.env.compute_reward(self.env.robot.get_ee_position(),objPos))
        rew.append(envRew)
        info = {}
        info['overallReward'] = np.sum(rew)
        info['reward0'] = rew[0]
        info['reward1'] = rew[1]
        info['subtask'] = 1 if rew[0] > 0.95 else 0
        info['success'] = envInfo['is_success']
        self.epStep += 1
        done = envInfo['is_success'] or self.epStep > self.epLength
        returnRew = np.sum(rew) if self.singleAgent else rew[info['subtask']]
        return obs, returnRew, done, info

    def reset(self):
        self.epStep = 0
        envObs = self.env.reset()
        obs = envObs['observation']
        return obs

if __name__ == "__main__":
    env = CanRampEnv(True)
    # env = gym.make("PandaCanRamp-v1")
    # env = PandaCan()
    ep = 0
    while ep < 10:
        env.reset()
        step = 0
        while step < 1000:
            env.step(env.action_space.sample())
            step += 1

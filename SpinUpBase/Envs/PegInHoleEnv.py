"""
A Franka Panda moves using delta end effector pose control.
This script contains examples of:
    - IK calculations.
    - Joint movement by setting joint target positions.
"""
from os.path import dirname, join, abspath
from pyrep import PyRep
from pyrep.robots.arms.panda import Panda
from pyrep.robots.end_effectors.panda_gripper import PandaGripper
from pyrep.objects.shape import Shape
from pyrep.objects.joint import Joint
import numpy as np, math, time
from scipy.spatial.transform import Rotation as R
from gym import spaces
from copy import deepcopy as copy
from multiprocessing import Process
from gym.envs.mujoco import mujoco_env



class PegInHole(mujoco_env.MujocoEnv):
    def __init__(self,**kwargs):
        self.kwargs = kwargs
        SCENE_FILE = join(dirname(abspath(__file__)), f'../Models/PegInHoleV1.ttt')
        # print(kwargs)
        self.headless = True if 'headless' not in kwargs else kwargs['headless']
        self.randomPeg = True if 'randomPeg' not in kwargs else kwargs['randomPeg']
        self.randomHole = True if 'randomHole' not in kwargs else kwargs['randomHole']
        self.fixedAngle = True if 'fixedAngle' not in kwargs else kwargs['fixedAngle']
        self.dim = 2 if 'dim' not in kwargs else int(kwargs['dim'])
        self.fakeGrasping = True if 'fakeGrasp' not in kwargs else kwargs['fakeGrasp']
        self.fixedHole = True if 'fixedHole' not in kwargs else kwargs['fixedHole']
        self.normaliseState = True if 'normaliseState' not in kwargs else kwargs['normaliseState']

        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=self.headless)
        self.pr.start()
        self.agent = Panda()
        self.gripper = PandaGripper()
        self.peg = Shape('Peg')
        self.hole = Shape('Hole')
        if self.fixedHole:
            self.hole.set_parent(Shape('diningTable'))
        actionDim = 1 + self.dim + (not self.fixedAngle) 

        obsDim = len(self._get_state())
        self.action_space = spaces.Box(low=np.array([-1 for _ in range(actionDim)]),
                                         high=np.array([1 for _ in range(actionDim)]), dtype=np.float64)
        self.observation_space = spaces.Box(low=-1, high=1, shape=[obsDim])
        self.nSubtasks = 2
        self.initDuration = 0

        self.working_height = 0.77
        self.working_axis = 0


        self._max_episode_steps = 1000
        
        self.starting_joint_positions = copy(self.agent.get_joint_positions())
        self.reset()

    def reinitialise(self):
        self.pr.shutdown()
        SCENE_FILE = join(dirname(abspath(__file__)), f'../Models/PegInHoleV1.ttt')
        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=self.headless)
        self.pr.start()
        self.agent = Panda()
        self.gripper = PandaGripper()
        self.peg = Shape('Peg')
        self.hole = Shape('Hole')
        if self.fixedHole:
            self.hole.set_parent(Shape('diningTable'))
        self.starting_joint_positions = copy(self.agent.get_joint_positions())
        self.initDuration = 0
        self.reset()

    def get_quat(self,angle):
        return R.from_euler('xyz',[angle,-np.pi/2,0])._quat[0]
        

    def reset(self):
        self.agent.set_joint_positions(self.starting_joint_positions)
        self.pos, self.quat = self.agent.get_tip().get_position(), self.agent.get_tip().get_quaternion()

        #Peg randomisation
        if self.randomPeg:
            peg_min = [0.9, -0.2]
            peg_max = [1.2, 0.2]
            peg_pos = list(np.random.uniform(peg_min[:self.dim], peg_max[:self.dim]))
            if self.dim == 1:
                peg_pos.append(self.working_axis)
            peg_pos.append(self.working_height)
            self.peg.set_position(peg_pos)
            angle = np.random.uniform(-0.2,0.2) if not self.fixedAngle else 0
            self.peg.set_quaternion(self.get_quat(angle))
        else:
            peg_pos = [1.0, self.working_axis, self.working_height]
            self.peg.set_position(peg_pos)
            self.peg.set_quaternion(self.get_quat(0))


        #Hole randomisation

        if self.randomHole:
            hole_min = [1.2,-0.2]
            hole_max = [1.3,0.2]
            hole_pos = list(np.random.uniform(hole_min[:self.dim], hole_max[:self.dim]))
            if self.dim == 1:
                hole_pos.append(self.working_axis)
            hole_pos.append(self.working_height)
            self.hole.set_position(hole_pos)
            angle = np.random.uniform(-0.2,0.2) if not self.fixedAngle else 0
            self.hole.set_quaternion(self.get_quat(angle-np.pi/2))
        else:
            hole_pos = [1.25, self.working_axis, self.working_height]
            self.hole.set_position(hole_pos)
            self.hole.set_quaternion(self.get_quat(-np.pi/2))


        self.gripper.release()

        self.ep_step = 0
        self.initDuration += 1
        if self.initDuration > 100:
            self.reinitialise()

        self.lastJPos = np.array([0,0])
        self.lastJForces = np.array([0,0])

        state = self._get_state()
        # print("State")
        # for s in state:
        #     print(s)
        self.joint_velocities = []
        return state

    
    def _get_state(self):
        if not self.fixedAngle:
            state = np.concatenate([self.agent.get_tip().get_position()[:self.dim],
                                self.agent.get_tip().get_velocity()[0][:self.dim], 
                                self.agent.get_tip().get_orientation(),
                                self.agent.get_tip().get_velocity()[1],
                                self.peg.get_position()[:self.dim],
                                self.peg.get_orientation(),
                                self.hole.get_position()[:self.dim],
                                self.hole.get_orientation(),
                                self.gripper.get_joint_positions()])
        else:
            state = np.concatenate([self.agent.get_tip().get_position()[:self.dim],
                                self.agent.get_tip().get_velocity()[0][:self.dim],
                                self.agent.get_tip().get_velocity()[1],
                                self.peg.get_position()[:self.dim],
                                self.hole.get_position()[:self.dim],
                                self.gripper.get_joint_positions()])
        return state



    def _get_reward(self):
        agentPose = self.agent.get_tip().get_position()
        pegPose = self.peg.get_position()
        holePose = self.hole.get_position()

        end2pegDist = np.linalg.norm(agentPose-pegPose)
        peg2holeDist = np.linalg.norm(pegPose-holePose)

        rews = []
        rews.append(math.exp(-3*end2pegDist))

        rews.append(math.exp(-3*peg2holeDist))#+(1 if math.exp(-3*peg2holeDist)>=0.8 else 0))
        return rews

    def _get_subtask(self):
        if self.fakeGrasping:
            return self._checked_grasped()
        else:
            jPos = np.array(self.gripper.get_joint_positions())
            jForces = np.array(self.gripper.get_joint_forces())
            # print(f"Target Velocity: {tVel}")
            # print(f"Joint Velocity: {jVel}")
            if self.gripper._proximity_sensor.is_detected(self.peg) and \
                sum(jForces > 2) == 2 and \
                sum(self.lastJForces > 2) == 2 and \
                sum(np.abs(jPos-self.lastJPos)<0.005) == 2:
                state = 1
            else:
                state = 0
            self.lastJPos = copy(jPos)
            self.lastJForces = copy(jForces)
            return state

    def _checked_grasped(self):
        if self.peg in self.gripper._grasped_objects:
            return True
        return False

    def clip_target(self,current,target,clip = 0.1):
        return current + np.clip((target-current),-clip,clip)
        

    def step(self,action):
        move = np.zeros((3,))
        move[:self.dim] = 0.02*action[:self.dim]
        # move[0] = 0.02
        self.pos += move
        if self.dim == 1:
            self.pos[1] = self.clip_target(self.pos[1],self.working_axis)
        self.pos[2] = self.clip_target(self.pos[2],self.working_height)

        if not self.fixedAngle:
            r = R.from_quat(self.quat).as_euler('xyz')
            r[2] += 0.1*action[2]
            quat = R.from_euler('xyz',r)._quat[0]
        else:
            quat = self.quat
        # print(pos,quat)
        try:
            new_joint_angles = self.agent.solve_ik(self.pos, quaternion=quat)
        except:
            rews = self._get_reward()
            subtask = self._get_subtask()
            info = {"reward0":rews[0],
                    "reward1":rews[1],
                    "overallReward":np.sum(rews),
                    "subtask":subtask,
                    "success":1 if rews[1] > 0.8 else 0}
            for n, act in enumerate(action):
                info[f"action{n}"] = act
            print(f"Failed to move: {action}")
            return self._get_state(), rews[subtask], True, info

        self.agent.set_joint_target_positions(new_joint_angles)
        
        gripAct = 0.5*action[-1]+0.5
        self.gripper.actuate(gripAct,1)
        if self.fakeGrasping:
            if gripAct < 0.5:
                self.gripper.grasp(self.peg)
            else:
                self.gripper.release()
        self.pr.step()
        self.ep_step += 1
        self.pos, self.quat = self.agent.get_tip().get_position(), self.agent.get_tip().get_quaternion()

        done = True if self.ep_step >= self._max_episode_steps else False

        subtask = self._get_subtask()
        rews = self._get_reward()
        info = {"reward0":rews[0],
                "reward1":rews[1],
                "overallReward":np.sum(rews),
                "subtask":subtask,
                "success":1 if rews[1] > 0.8 else 0}
        for n, act in enumerate(action):
            info[f"action{n}"] = act
        return self._get_state(), rews[subtask], done, info

class PegInHoleV2:
    def __init__(self,**kwargs):
        self.kwargs = kwargs
        SCENE_FILE = join(dirname(abspath(__file__)), f'../Models/PegInHoleV2.ttt')
        # print(kwargs)
        self.headless = True if 'headless' not in kwargs else kwargs['headless']
        self.randomPeg = True if 'randomPeg' not in kwargs else kwargs['randomPeg']
        self.randomHole = True if 'randomHole' not in kwargs else kwargs['randomHole']
        self.dim = 1
        self.normaliseState = True if 'normaliseState' not in kwargs else kwargs['normaliseState']

        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=self.headless)
        self.pr.start()
        self.agent = Panda()
        self.gripper = PandaGripper()
        self.peg = Shape('Peg')
        self.hole = Shape('Hole')
        self.actuator = Joint('Prismatic_joint')

        actionDim = 1 + self.dim 

        obsDim = len(self._get_state())
        self.action_space = spaces.Box(low=np.array([-1 for _ in range(actionDim)]),
                                         high=np.array([1 for _ in range(actionDim)]), dtype=np.float64)
        self.observation_space = spaces.Box(low=-1, high=1, shape=[obsDim])
        self.nSubtasks = 2
        self.initDuration = 0

        self.working_height = 0.78
        self.working_axis = 0


        self._max_episode_steps = 1000
        
        self.starting_joint_positions = copy(self.agent.get_joint_positions())
        self.reset()

    def reinitialise(self):
        self.pr.shutdown()
        SCENE_FILE = join(dirname(abspath(__file__)), f'../Models/PegInHoleV2.ttt')
        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=self.headless)
        self.pr.start()
        self.agent = Panda()
        self.gripper = PandaGripper()
        self.peg = Shape('Peg')
        self.hole = Shape('Hole')
        self.actuator = Joint('Prismatic_joint')
        self.starting_joint_positions = copy(self.agent.get_joint_positions())
        self.initDuration = 0
        self.reset()

    def get_quat(self,angle):
        return R.from_euler('xyz',[angle,-np.pi/2,0])._quat[0]
        

    def reset(self):
        self.agent.set_joint_positions(self.starting_joint_positions)
        self.pos, self.quat = self.agent.get_tip().get_position(), self.agent.get_tip().get_quaternion()

        

        #Hole randomisation

        if self.randomHole:
            hole_min = [1.2]
            hole_max = [1.3]
            hole_pos = list(np.random.uniform(hole_min, hole_max))
            hole_pos.append(self.working_axis)
            hole_pos.append(self.working_height)
            self.hole.set_position(hole_pos)
            angle = 0
            self.hole.set_quaternion(self.get_quat(angle-np.pi/2))
        else:
            hole_pos = [1.25, self.working_axis, self.working_height]
            self.hole.set_position(hole_pos)
            self.hole.set_quaternion(self.get_quat(-np.pi/2))
        

        peg_min = [1.0]
        peg_max = [1.2]
        peg_pos = np.random.uniform(peg_min, peg_max)
        
        self.actuator.set_joint_position(peg_pos-hole_pos[0])

        angle = 0
        self.peg.set_quaternion(self.get_quat(angle))
        self.peg.set_position([peg_pos,self.working_axis,self.working_height])

        self.ep_step = 0
        self.initDuration += 1
        if self.initDuration > 100:
            self.reinitialise()

        self.lastJPos = np.array([0,0])
        self.lastJForces = np.array([0,0])

        state = self._get_state()
        # print("State")
        # for s in state:
        #     print(s)
        self.joint_velocities = []
        return state

    
    def _get_state(self):
        
        state = np.concatenate([self.agent.get_tip().get_position()[:self.dim],
                            self.agent.get_tip().get_velocity()[0][:self.dim],
                            self.agent.get_tip().get_velocity()[1],
                            self.peg.get_position()[:self.dim],
                            self.hole.get_position()[:self.dim],
                            self.gripper.get_joint_positions()])
        return state



    def _get_reward(self):
        agentPose = self.agent.get_tip().get_position()
        pegPose = self.peg.get_position()
        holePose = self.hole.get_position()

        end2pegDist = np.linalg.norm(agentPose-pegPose)
        peg2holeDist = np.linalg.norm(pegPose-holePose)

        rews = []
        rews.append(math.exp(-3*end2pegDist))

        rews.append(math.exp(-3*peg2holeDist))#+(1 if math.exp(-3*peg2holeDist)>=0.8 else 0))
        return rews

    def _get_subtask(self):
        jPos = np.array(self.gripper.get_joint_positions())
        jForces = np.array(self.gripper.get_joint_forces())
        # print(f"Target Velocity: {tVel}")
        # print(f"Joint Velocity: {jVel}")
        if self.gripper._proximity_sensor.is_detected(self.peg) and \
            sum(jForces > 2) == 2 and \
            sum(self.lastJForces > 2) == 2 and \
            sum(np.abs(jPos-self.lastJPos)<0.005) == 2:
            state = 1
        else:
            state = 0
        self.lastJPos = copy(jPos)
        self.lastJForces = copy(jForces)
        return state

    def _checked_grasped(self):
        if self.peg in self.gripper._grasped_objects:
            return True
        return False

    def clip_target(self,current,target,clip = 0.1):
        return current + np.clip((target-current),-clip,clip)
        

    def step(self,action):
        move = np.zeros((3,))
        move[:self.dim] = 0.02*action[:self.dim]
        # move[0] = 0.02
        self.pos += move
        if self.dim == 1:
            self.pos[1] = self.clip_target(self.pos[1],self.working_axis)
        self.pos[2] = self.clip_target(self.pos[2],self.working_height)

        # print(pos,quat)
        try:
            new_joint_angles = self.agent.solve_ik(self.pos, quaternion=self.quat)
        except:
            rews = self._get_reward()
            subtask = self._get_subtask()
            info = {"reward0":rews[0],
                    "reward1":rews[1],
                    "overallReward":np.sum(rews),
                    "subtask":subtask,
                    "success":1 if rews[1] > 0.8 else 0}
            for n, act in enumerate(action):
                info[f"action{n}"] = act
            print(f"Failed to move: {action}")
            return self._get_state(), rews[subtask], True, info

        self.agent.set_joint_target_positions(new_joint_angles)
        
        gripAct = 0.5*action[-1]+0.5
        self.gripper.actuate(gripAct,1)
        self.pr.step()
        self.ep_step += 1
        self.pos, self.quat = self.agent.get_tip().get_position(), self.agent.get_tip().get_quaternion()

        done = True if self.ep_step >= self._max_episode_steps else False

        subtask = self._get_subtask()
        rews = self._get_reward()
        info = {"reward0":rews[0],
                "reward1":rews[1],
                "overallReward":np.sum(rews),
                "subtask":subtask,
                "success":1 if rews[1] > 0.8 else 0}
        for n, act in enumerate(action):
            info[f"action{n}"] = act
        return self._get_state(), rews[subtask], done, info

class PegInHoleV3:
    def __init__(self,**kwargs):
        self.kwargs = kwargs
        SCENE_FILE = join(dirname(abspath(__file__)), f'../Models/PegInHoleV1.ttt')
        # print(kwargs)
        self.headless = True if 'headless' not in kwargs else kwargs['headless']
        self.randomPeg = True if 'randomPeg' not in kwargs else kwargs['randomPeg']
        self.randomHole = True if 'randomHole' not in kwargs else kwargs['randomHole']
        self.fixedAngle = True if 'fixedAngle' not in kwargs else kwargs['fixedAngle']
        self.dim = 2 if 'dim' not in kwargs else int(kwargs['dim'])
        self.fakeGrasping = True if 'fakeGrasp' not in kwargs else kwargs['fakeGrasp']
        self.fixedHole = True if 'fixedHole' not in kwargs else kwargs['fixedHole']
        self.normaliseState = True if 'normaliseState' not in kwargs else kwargs['normaliseState']

        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=self.headless)
        self.pr.start()
        self.agent = Panda()
        self.gripper = PandaGripper()
        self.peg = Shape('Peg')
        self.hole = Shape('Hole')
        if self.fixedHole:
            self.hole.set_parent(Shape('diningTable'))
        actionDim = 8

        obsDim = len(self._get_state())
        self.action_space = spaces.Box(low=np.array([-1 for _ in range(actionDim)]),
                                         high=np.array([1 for _ in range(actionDim)]), dtype=np.float64)
        self.observation_space = spaces.Box(low=-1, high=1, shape=[obsDim])
        self.nSubtasks = 2
        self.initDuration = 0

        self.working_height = 0.77
        self.working_axis = 0


        self._max_episode_steps = 1000
        
        self.starting_joint_positions = copy(self.agent.get_joint_positions())
        self.reset()

    def reinitialise(self):
        self.pr.shutdown()
        SCENE_FILE = join(dirname(abspath(__file__)), f'../Models/PegInHoleV1.ttt')
        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=self.headless)
        self.pr.start()
        self.agent = Panda()
        self.gripper = PandaGripper()
        self.peg = Shape('Peg')
        self.hole = Shape('Hole')
        if self.fixedHole:
            self.hole.set_parent(Shape('diningTable'))
        self.starting_joint_positions = copy(self.agent.get_joint_positions())
        self.initDuration = 0
        self.reset()

    def get_quat(self,angle):
        return R.from_euler('xyz',[angle,-np.pi/2,0])._quat[0]
        

    def reset(self):
        self.agent.set_joint_positions(self.starting_joint_positions)
        self.pos, self.quat = self.agent.get_tip().get_position(), self.agent.get_tip().get_quaternion()

        #Peg randomisation
        if self.randomPeg:
            peg_min = [0.9, -0.2]
            peg_max = [1.2, 0.2]
            peg_pos = list(np.random.uniform(peg_min[:self.dim], peg_max[:self.dim]))
            if self.dim == 1:
                peg_pos.append(self.working_axis)
            peg_pos.append(self.working_height)
            self.peg.set_position(peg_pos)
            angle = np.random.uniform(-0.2,0.2) if not self.fixedAngle else 0
            self.peg.set_quaternion(self.get_quat(angle))
        else:
            peg_pos = [1.0, self.working_axis, self.working_height]
            self.peg.set_position(peg_pos)
            self.peg.set_quaternion(self.get_quat(0))


        #Hole randomisation

        if self.randomHole:
            hole_min = [1.2,-0.2]
            hole_max = [1.3,0.2]
            hole_pos = list(np.random.uniform(hole_min[:self.dim], hole_max[:self.dim]))
            if self.dim == 1:
                hole_pos.append(self.working_axis)
            hole_pos.append(self.working_height)
            self.hole.set_position(hole_pos)
            angle = np.random.uniform(-0.2,0.2) if not self.fixedAngle else 0
            self.hole.set_quaternion(self.get_quat(angle-np.pi/2))
        else:
            hole_pos = [1.25, self.working_axis, self.working_height]
            self.hole.set_position(hole_pos)
            self.hole.set_quaternion(self.get_quat(-np.pi/2))


        self.gripper.release()

        self.ep_step = 0
        self.initDuration += 1
        if self.initDuration > 100:
            self.reinitialise()

        self.lastJPos = np.array([0,0])
        self.lastJForces = np.array([0,0])

        state = self._get_state()
        # print("State")
        # for s in state:
        #     print(s)
        self.joint_velocities = []
        return state

    
    def _get_state(self):
        if not self.fixedAngle:
            state = np.concatenate([self.agent.get_joint_positions(),
                                self.gripper.get_joint_positions(),
                                self.peg.get_position(),
                                self.peg.get_orientation(),
                                self.hole.get_position(),
                                self.hole.get_orientation()])
        else:
            state = np.concatenate([self.agent.get_joint_positions(),
                                    self.gripper.get_joint_positions(),
                                    self.peg.get_position(),
                                    self.hole.get_position()])
        return state



    def _get_reward(self):
        agentPose = self.agent.get_tip().get_position()
        pegPose = self.peg.get_position()
        holePose = self.hole.get_position()

        end2pegDist = np.linalg.norm(agentPose-pegPose)
        peg2holeDist = np.linalg.norm(pegPose-holePose)

        rews = []
        rews.append(math.exp(-3*end2pegDist))

        rews.append(math.exp(-3*peg2holeDist))#+(1 if math.exp(-3*peg2holeDist)>=0.8 else 0))
        return rews

    def _get_subtask(self):
        if self.fakeGrasping:
            return self._checked_grasped()
        else:
            jPos = np.array(self.gripper.get_joint_positions())
            jForces = np.array(self.gripper.get_joint_forces())
            # print(f"Target Velocity: {tVel}")
            # print(f"Joint Velocity: {jVel}")
            if self.gripper._proximity_sensor.is_detected(self.peg) and \
                sum(jForces > 2) == 2 and \
                sum(self.lastJForces > 2) == 2 and \
                sum(np.abs(jPos-self.lastJPos)<0.005) == 2:
                state = 1
            else:
                state = 0
            self.lastJPos = copy(jPos)
            self.lastJForces = copy(jForces)
            return state

    def _checked_grasped(self):
        if self.peg in self.gripper._grasped_objects:
            return True
        return False

    def clip_target(self,current,target,clip = 0.1):
        return current + np.clip((target-current),-clip,clip)
        

    def step(self,action):
        new_pos = self.agent.get_joint_positions() + action[:-1]

        # try:
        #     new_joint_angles = self.agent.set_joint_position(new_pos)
        # except:
        #     rews = self._get_reward()
        #     subtask = self._get_subtask()
        #     info = {"reward0":rews[0],
        #             "reward1":rews[1],
        #             "overallReward":np.sum(rews),
        #             "subtask":subtask,
        #             "success":1 if rews[1] > 0.8 else 0}
        #     for n, act in enumerate(action):
        #         info[f"action{n}"] = act
        #     print(f"Failed to move: {action}")
        #     return self._get_state(), rews[subtask], True, info

        self.agent.set_joint_target_positions(new_pos)
        
        gripAct = 0.5*action[-1]+0.5
        self.gripper.actuate(gripAct,1)
        if self.fakeGrasping:
            if gripAct < 0.5:
                self.gripper.grasp(self.peg)
            else:
                self.gripper.release()
        self.pr.step()
        self.ep_step += 1
        self.pos, self.quat = self.agent.get_tip().get_position(), self.agent.get_tip().get_quaternion()

        done = True if self.ep_step >= self._max_episode_steps else False

        subtask = self._get_subtask()
        rews = self._get_reward()
        info = {"reward0":rews[0],
                "reward1":rews[1],
                "overallReward":np.sum(rews),
                "subtask":subtask,
                "success":1 if rews[1] > 0.8 else 0}
        for n, act in enumerate(action):
            info[f"action{n}"] = act
        return self._get_state(), rews[subtask], done, info

if __name__ == "__main__":

    env = PegInHole(headless=0)
    nAttempts = 200
    step = 1

    for n in range(20):
        env.reset()
        time.sleep(1)
        print(f'\n{env._get_reward()}')

    env.pr.stop()
    env.pr.shutdown()

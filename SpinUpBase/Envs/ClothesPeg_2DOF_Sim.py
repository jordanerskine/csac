#!/usr/bin/env python3

from xmlrpc.client import Boolean
import rospy
import tf2_ros
import actionlib
import numpy as np, sys, math, torch
import torch.nn as nn
import cv2
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib
from utils import rotateAroundPoint as rap

from cv_bridge import CvBridge

from collections import deque

from copy import deepcopy as copy
from gym import spaces

from collections import deque

from subprocess import call
import time

matplotlib.use('TKAgg')
# import bezier

# from trajectory_generator import TrajectoryGenerator

# from pyquaternion import Quaternion as Q
def getGraspDistributionFromLog(logFile, showEpisode = False):
  fig, ax = plt.subplots()
  begun = 0
  episode = 0
  allData = []
  successes = []
  failures = []
  with open(logFile,"r") as f:
    for line in f.readlines():
      if "Point" in line:
        begun = 1
        relevant = line.split(":")[1]
        for seg in relevant.split(" | "):
          if "Successes" in seg:
            try:
              successes.append(int(seg.split("-")[1]))
            except:
              successes.append(0)
          if "Failures" in seg:
            try:
              failures.append(int(seg.split("-")[1]))
            except:
              failures.append(0)
      else:
        if begun ==1:
          allData.append({'Succ':successes,
                          'Fail':failures,
                          'Ep':episode})
          successes = []
          failures = []
          begun = 0
        segs = line.split(" | ")
        for seg in segs:
          if seg[0] == "E":
            episode = int(seg.split(": ")[1])
  x = 0
  global iter
  iter = 0
  global colorBarDone
  colorBarDone = False

  def plotGraspHistogram(i, Data:list,x:int):
    global iter
    global colorBarDone
    successes = Data[iter]['Succ']
    failures = Data[iter]['Fail']
    episode = Data[iter]['Ep']
    increment = 0.005
    my_cmap = plt.get_cmap("jet_r")
    pcm = ax.pcolormesh(np.random.random((20, 20)),cmap=my_cmap)
    ax.clear()
    bins = -np.arange(-0.05,0.05+increment,increment)
    totals = np.array(successes) + np.array(failures)
    success_rate = np.array(successes)/np.clip(totals,a_min=1,a_max=np.inf)
    try:
      plt.bar(bins[:-1],totals,color = my_cmap(success_rate),width = np.diff(bins),edgecolor="black")
    except:
      pass
    if showEpisode:
      plt.title(f"Grasp Distribution - Episode {episode}") #
    else:
      plt.title(f"Grasp Distribution") #- Episode {episode}
    plt.xlabel("Grasp Position relative to center of peg (m)")
    plt.ylabel("Grasp Count")
    plt.subplots_adjust(left=0.15,right=0.95,top=0.9,bottom=0.2)

    if not colorBarDone:
      colorBarDone = True
      fig.colorbar(pcm,ax=ax,label="Average Success Rate")

    # plt.draw()
    iter += 1
    if iter >= len(Data):
      iter = 0
    
  ani = animation.FuncAnimation(fig, plotGraspHistogram, fargs = (allData,x),interval=100)
  plt.show()



def showImage(image,wait = 0):
    cv2.imshow("image",image)
    cv2.waitKey(wait)


def smooth(val,lastVal,maxChange):
  return lastVal+np.clip(val-lastVal,-maxChange,maxChange)

def angle_diff(angle1,angle2):
  a = angle1 - angle2
  a -= math.pi*2 if a > math.pi else 0
  a += math.pi*2 if a < -math.pi else 0
  return a

class Expert:
  def __init__(self,noise=0.02, target = 0.6, alwaysGrasp = 0, graspLoc = -0.022):
    self.alwaysGrasp = alwaysGrasp
    self.graspLoc = graspLoc
    self.target = target
    self.noise = noise

  def get_actions(self,o):
    device = o.device
    actions = [self.get_action(obs) for obs in o.cpu()]
    actions = torch.tensor(actions,device = device,dtype = o.dtype)
    return actions

  def get_action(self,observation):
    obs = observation
    if obs[-1] ==0:
      if self.alwaysGrasp:
        grasp = 1
      else:
        grasp = 1*((abs(obs[-3]-obs[0]-0.022) < 0.005)and(abs(obs[-2]-obs[1]) < 0.005))-0.5
      xVel = np.clip(10*(obs[-3]-obs[0]+self.graspLoc),-0.5,0.5)
      action = np.array([xVel,
                        np.clip(10*(obs[-2]-obs[1]),-0.5,0.5),
                        grasp])#
    else:
      action = np.array([np.clip(10*(self.target-obs[-3]),-0.5,0.5),
                        np.clip(10*(0-obs[-2]),-0.5,0.5),
                        -1* ((abs(obs[-3] - self.target)<0.005)and(abs(obs[-2])<0.005))+0.5])

    action = np.clip(np.random.normal(action,self.noise),-1,1)
    return action

class Peg:
  def __init__(self, **args):

    self._max_episode_steps = 20 if '_max_episode_steps' not in args else args['_max_episode_steps']
    self.useVel = True if 'useVel' not in args else args['useVel']
    self.randomStart = True if 'randomStart' not in args else args['randomStart']
    self.endOnOutOfBounds = 0 if 'endOnOutOfBounds' not in args else args['endOnOutOfBounds']
    self.rewardType = 'dense' if 'rewardType' not in args else args['rewardType']
    self.headless = True if 'headless' not in args else args['headless']
    self.clipDist = 0.01 if 'clipDist' not in args else args['clipDist']
    self.graspAfter = True if 'graspAfter' not in args else args['graspAfter']
    self.subtaskLinger = 0 if 'subtaskLinger' not in args else args['subtaskLinger']
    self.objectVis = "Peg" if "objectVis" not in args else args["objectVis"]
    corners = [[0.45,-0.1],[0.60,0.1]] if 'corners' not in args else args['corners']
    if isinstance(corners,str):
      corners = eval(corners)
    print(corners)

    max_speed = 0.1 if 'maxSpeed' not in args else args['maxSpeed']


    self.error = 0
    self.epStep = 0
    self.grasped = False

    self.graspedLocations = deque(maxlen = 1000)

    self.pegSize = {'x':[0.026,0.035],
                    'y':[0.007,0.007]}


    self.bounds = {'x':[corners[0][0],corners[1][0]],
                   'y':[corners[0][1],corners[1][1]]}
    self.homePose = [0.532,-0.037,0.279]

    self.pickHeight = 0.169

    self.target = {'x':[corners[1][0]-0.06,corners[1][0]],
                   'y':[-0.003,0.003]}


    # action parameters
    self.acceleration_constraint = 0.1 #m/s/s
    self.max_speed = max_speed #m/s
    self.max_angle_speed = 0.5 #rad/s
    self.hz = 100
    self.actHz = 5 #Hz
    
    self.stepScale = 1/self.hz


    self.nSubtasks = 2

    # self.bounds = [self.xLim, self.yLim, self.zLim]
    self.ee_pose = np.array([0,0,0])
    self.frame = None
    self.gripper_position = None
    self.moveStatus = None
    self.graspStatus = None
    self.failedToReset = False

    # render settings
    self.w = 600
    self.h = 600

    self.bg_image = cv2.resize(cv2.cvtColor(np.random.randint(225,256,(self.w//8,self.h//8)).astype(np.uint8), cv2.COLOR_GRAY2BGR), dsize=(self.h,self.w), interpolation=cv2.INTER_NEAREST)
    


    self.actSpace = 3
    self.obsSpace = 5
    self.obsSpace += 2 if self.useVel else 0

    self.action_space = spaces.Box(low=np.array([-1 for _ in range(self.actSpace)]),
                                   high=np.array([1 for _ in range(self.actSpace)]),dtype=np.float64)
    
    self.observation_space = spaces.Box(low=-1,high=1,shape=[self.obsSpace])


    
    currentObs = copy(self.ee_pose)
    lastObs = currentObs+1
    self.episode = 1
    self.lastEval = False
    self.graspedThisEp = False
    self.reset()




  def reset(self,eval = False):
    self.epStep = 0
    self.lastAction = np.zeros(self.actSpace-1)
    self.missed_grasp = False

    border = 0.02
    self.pegPos = {'x':np.random.uniform(self.bounds['x'][0]+border,self.bounds['x'][1]-border),
                   'y':np.random.uniform(self.bounds['y'][0]+border,self.bounds['y'][1]-border)}

    self.grasped = 0
    self.graspedThisEp = 0
    
    self.pos = {'x':np.random.uniform(self.bounds['x'][0]+border,self.bounds['x'][1]-border),
                'y':np.random.uniform(self.bounds['y'][0]+border,self.bounds['y'][1]-border)}
    self.vel = {'x':0,'y':0}
    self.epStep = 0
    self.lastAction = np.zeros(self.actSpace-1)
    self.episode += 1

    if eval and not self.lastEval:
      self.showGraspDistribution()
    self.lastEval = eval
    self.lingerDuration = 0

    return self._obs()


  def _obs(self):
    obs = np.array([p for p in self.pos.values()])
    if self.useVel:
      obs = np.concatenate([obs,np.array([vel/self.stepScale for vel in self.vel.values()])])
    obs = np.concatenate([obs,np.array([pos for pos in self.pegPos.values()])])
    obs = np.concatenate([obs,np.array([self.grasped])])
    
    return obs

  def reward(self,type = "total"):
    reachDist = np.linalg.norm([pos-peg for pos,peg in zip(self.pos.values(),self.pegPos.values())])
    clipDist = np.linalg.norm([self.pegPos['x']+self.pegSize['x'][1]-np.mean(self.target['x']),self.pegPos['y']-np.mean(self.target['y'])])
    if self.rewardType == "dense":
      clipRew = 0.5 * math.exp(-5*clipDist) + 0.5 * (clipDist < self.clipDist) * (not self.grasped)
      allRew = {"reach":math.exp(-5*reachDist),
                "clip":clipRew}
      totalRew = np.sum([allRew[r] for r in allRew])
    elif self.rewardType == "fullSparse":
      clipRew = (clipDist < self.clipDist) * (not self.grasped)
      allRew = {"reach":0,
                "clip":clipRew}
      totalRew = np.sum([allRew[r] for r in allRew])
    else:
      clipRew = (clipDist < self.clipDist) * (not self.grasped)
      allRew = {"reach":1*self.graspedThisEp,
                "clip":clipRew}
      totalRew = np.sum([allRew[r] for r in allRew])
      
    if type == "total":
      return totalRew
    elif type == "all":
      return allRew

  def _checkOutBounds(self):
    if np.any([pos < bound[0] or pos > bound[1] for pos,bound in zip(self.pos.values(),self.bounds.values())]):
        return True
    return False

  def smooth_action(self,act):
    accel_constraint = self.acceleration_constraint/self.hz
    action = smooth(act,self.lastAction,accel_constraint)
    self.lastAction = action
    return action

  def grasp(self, grasp = True):
    if np.all([((pos > (peg - size[0])) and (pos < (peg + size[1]))) for pos,peg,size in zip(self.pos.values(),self.pegPos.values(),self.pegSize.values())]):
      if not self.grasped and grasp:
        self.pegPos['y'] = self.pos['y']
      self.grasped = int(grasp)
      if self.grasped and not self.graspedThisEp:
        self.graspedLocations.append([peg-pos for peg,pos in zip(self.pegPos.values(),self.pos.values())])

      self.graspedThisEp = int(self.graspedThisEp or self.grasped)



  def showGraspDistribution(self, getHist = False):
    increment = 0.005
    print("Grasp Distribution")
    successes = []
    failures = []
    for point in np.arange(-0.05,0.05,increment):
      success = np.sum([1 if loc[1] else 0 for loc in self.graspedLocations if np.linalg.norm(loc[0]-point) < increment/2])
      failure = np.sum([0 if loc[1] else 1 for loc in self.graspedLocations if np.linalg.norm(loc[0]-point) < increment/2])
      successes.append(success)
      failures.append(failure)
      print(f"   Point {point:.3f}: Successes - {success} | Failures - {failure} | Success Rate - {success/(success+failure)*100:.1f}%")
    if getHist:
      self.plotGraspHistogram(successes,failures)
    

  def step(self, act):

    if not self.graspAfter:
      if act[-1] > 0 and not self.graspedThisEp:
        self.grasp(True)
      elif act[-1] < 0 and self.graspedThisEp:
        self.grasp(False)
      
    for _ in range(int(self.hz/self.actHz)):
      subtask = 0 if not self.graspedThisEp else 1
      sm_act = self.smooth_action(np.array([act[:2]*self.max_speed]))[0]
      if not self.endOnOutOfBounds:
        i = 0
        for pos,bound in zip(self.pos.values(),self.bounds.values()):
            if pos < bound[0]:
                sm_act[i] = max(0,sm_act[i])
            elif pos > bound[1]:
                sm_act[i] = min(0,sm_act[i])
            i+=1
      self.vel['x'] = sm_act[0]*self.stepScale
      self.vel['y'] = sm_act[1]*self.stepScale
      
      self.pos['x'] += self.vel['x']
      self.pos['y'] += self.vel['y']
      if self.grasped:
        self.pegPos['x'] += self.vel['x']
        self.pegPos['y'] += self.vel['y']
    if not self.headless:
      self.render()
    
    if self.graspAfter:
      if act[-1] > 0 and not self.graspedThisEp:
        self.grasp(True)
      elif act[-1] < 0 and self.graspedThisEp:
        self.grasp(False)
    subtask = self._getSubtask()

    self.epStep += 1

    obs = self._obs()
    rews = list(self.reward("all").values())
    done = self._checkDone()
    info = {}
    info['overallReward'] = self.reward()
    info['reachReward'] = self.reward("all")["reach"]
    info['clipReward'] = self.reward("all")["clip"]
    info['subtask'] = subtask
    info['success'] = self._success()

    if self._success():
      self.graspedLocations[-1][1] = True

    return obs, rews[subtask], done, info


  def render(self):
    def w2p(x,y):
      edge = 0.05
      xOut = int((x-self.bounds['x'][0]+edge)/(self.bounds['x'][1]-self.bounds['x'][0]+2*edge)*self.w)
      yOut = int((y-self.bounds['y'][0]+edge)/(self.bounds['y'][1]-self.bounds['y'][0]+2*edge)*self.h)
      return np.array([xOut,yOut])
    image = self.bg_image.copy()

    clipDist = int(np.linalg.norm(w2p(0,0)-w2p(0,self.clipDist)))

    # insert target
    # image = cv2.rectangle(image,tuple(w2p(self.target['x'][0],self.target['y'][0])),tuple(w2p(self.target['x'][1],self.target['y'][1])),(150,150,150),-1)
    targetImage = cv2.circle(copy(image),tuple(w2p(np.mean(self.target['x']),np.mean(self.target['y']))),clipDist,(200,250,200),-1)
    image = np.array(image*0.2+targetImage*0.8,dtype=np.uint8)
    image = cv2.circle(image,tuple(w2p(np.mean(self.target['x']),np.mean(self.target['y']))),3,(100,150,100),-1)

    # insert bounds
    image = cv2.line(image,tuple(w2p(self.bounds['x'][0],self.bounds['y'][0])),tuple(w2p(self.bounds['x'][0],self.bounds['y'][1])),(100,100,250),2)
    image = cv2.line(image,tuple(w2p(self.bounds['x'][0],self.bounds['y'][1])),tuple(w2p(self.bounds['x'][1],self.bounds['y'][1])),(100,100,250),2)
    image = cv2.line(image,tuple(w2p(self.bounds['x'][1],self.bounds['y'][1])),tuple(w2p(self.bounds['x'][1],self.bounds['y'][0])),(100,100,250),2)
    image = cv2.line(image,tuple(w2p(self.bounds['x'][1],self.bounds['y'][0])),tuple(w2p(self.bounds['x'][0],self.bounds['y'][0])),(100,100,250),2)


    # draw peg
    if self.objectVis == "Peg":
      w = self.pegSize['x']
      h = self.pegSize['y']
      for sign in [1,-1]:
        # sign = 1
        points = [[w2p(self.pegPos['x']-w[0],self.pegPos['y']-sign*h[0])],
                  [w2p(self.pegPos['x']+w[1]*1.1,self.pegPos['y']-sign*h[0])],
                  [w2p(self.pegPos['x']+w[1]*1.05,self.pegPos['y'])],
                  [w2p(self.pegPos['x']+w[1]*0.95,self.pegPos['y'])],
                  [w2p(self.pegPos['x']+w[1]*0.65,self.pegPos['y']-sign*h[0]/2)],
                  [w2p(self.pegPos['x']+w[1]*0.6,self.pegPos['y']-sign*h[0]/2)],
                  [w2p(self.pegPos['x']+w[1]*0.2,self.pegPos['y'])],
                  [w2p(self.pegPos['x'],self.pegPos['y'])],
                  [w2p(self.pegPos['x']-w[0],self.pegPos['y']-sign*h[0]*0.8)]]
        if self.grasped:
          newPoints = []
          for point in points:
            newPoints.append([np.array(rap(point[0],w2p(self.pegPos['x'],self.pegPos['y']),sign*math.pi/16),int)])
          points = newPoints

        image = cv2.fillPoly(image,pts = [np.array(points)],color = (200,100,100))
    else:
      image = cv2.rectangle(image,tuple(w2p(self.pegPos['x']-self.pegSize['x'][0],self.pegPos['y']-self.pegSize['y'][0])),
                                      tuple(w2p(self.pegPos['x']+self.pegSize['x'][1],self.pegPos['y']+self.pegSize['y'][1])),(200,100,100),-1)
    image = cv2.circle(image,tuple(w2p(self.pegPos['x']+self.pegSize['x'][1],self.pegPos['y'])),5,(200,0,0),-1)

    # draw gripper
    colour = (200,250,200) if self.grasped else (200,200,250)
    height = (0.6 if self.grasped else 1.1) * (self.pegSize['y'][1]+self.pegSize['y'][0])
    width = 0.002
    image = cv2.rectangle(image,tuple(w2p(self.pos['x']+width,self.pos['y']+height)),tuple(w2p(self.pos['x']-width,self.pos['y']-height)),colour,-1)
    image = cv2.rectangle(image,tuple(w2p(self.pos['x']+width,self.pos['y']+height)),tuple(w2p(self.pos['x']-width,self.pos['y']-height)),(50,50,50),2)

    # image = cv2.rotate(image,cv2.ROTATE_90_COUNTERCLOCKWISE)

    image = cv2.putText(image,f"Timestep: {self.epStep}",(10,10),cv2.FONT_HERSHEY_PLAIN,1,(100,100,100),2)

    if self._success():
      image = cv2.putText(image,"Successful",(10,25),cv2.FONT_HERSHEY_PLAIN,1,(0,0,0),2)

    cv2.imshow("ClothesPeg-Sim",image)
    cv2.waitKey(30)

  def _getSubtask(self):
    if self.graspedThisEp:
      if self.subtaskLinger <= self.lingerDuration:
        return 1
      else:
        self.lingerDuration += 1
    return 0

  def _success(self):
    clipDist = np.linalg.norm([self.pegPos['x']+self.pegSize['x'][1]-np.mean(self.target['x']),self.pegPos['y']-np.mean(self.target['y'])])
    return clipDist < self.clipDist and not self.grasped

  def _checkDone(self):
    return (self.epStep >= self._max_episode_steps or self.error > 0 or (self._checkOutBounds() and self.endOnOutOfBounds))
    

if __name__ == "__main__":
  env = Peg(_max_episode_steps = 100,endOnOutOfBounds = 0,objectVis= "Block")#corners=[[0.33,-0.22],[0.73,0.18]]

  exp = Expert(target=env.bounds['x'][1]-0.07)#target=0.73
  nEpisodes = 10
  for n in range(nEpisodes):
    
    done = False
    obs = env.reset()
    length = 0
    totalRew = 0
    while not done:

      # action = actor(torch.Tensor(obs))
      # print(action)
      action = exp.get_action(obs)
      nobs, rew, done, info = env.step(action)
      env.render()
      totalRew += rew
      print("action: ",action)
      print(f"position: {nobs[0]}, peg position: {nobs[-2]}")
      print(f"Success: {info['success']}")

      length += 1
      if length >= 200:
        done = True

      obs = nobs
      # print(length)

    print("Episode {}: \nOut of Bounds at termination: {}\nLength: {} \nAverage Reward: {}\n------".format(n,env._checkOutBounds(),length,totalRew/length))

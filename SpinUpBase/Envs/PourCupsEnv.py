import numpy as np
from rlbench.environment import Environment
from rlbench.action_modes import ActionMode, ArmActionMode
from gym import spaces
import rlbench.tasks as Tasks

class PourCupsEnv():
    def __init__(self):

        actionMode = ActionMode(ArmActionMode.ABS_EE_VELOCITY)
        self.env = Environment(actionMode, headless = True)
        self.env.launch()
        taskName = 'PourFromCupToCup'
        task = getattr(Tasks,taskName)
        self.task = self.env.get_task(task)
        _, ob = self.task.reset()
        ob = self._getObs(ob)
        self.params = {'goalRange':1}
        self.lastObs = None
        self.lastSubtask = None
        self.timesteps = 0
        self.nSubtasks = 2

        # self.action_space = actionMode.action_size
        self.action_space = spaces.Box(low = np.full(actionMode.action_size,-1),
                                       high = np.full(actionMode.action_size,1))
        self.observation_space = spaces.Box(low = np.full(len(ob),-float('inf')),
                                            high = np.full(len(ob),float('inf')))

    def step(self,action):
        
        try:
            ob, trueRew, _ = self.task.step(action)
            rew = self._getRew(ob)
            st = self._getSubtask(ob)
            ob = self._getObs(ob)
            done = False
        except:
            ob = self.lastObs
            st = self.lastSubtask
            rew = [-1,-1]
            done = True
        
        self.timesteps += 1
        # print(self.timesteps)

        
        self.lastObs = ob
        self.lastSubtask = st

        return ob, rew[st], done, {"subtask":st,"reward0":rew[0],"reward1":rew[1]}

    def _getObs(self, obs):
        nobs = []
        nobs.append(obs.joint_positions)
        nobs.append(obs.joint_velocities)
        nobs.append(np.array(self.task._task.cup_source.get_position()))
        nobs = np.concatenate(nobs)
        return nobs
    
    def _getRew(self, obs):
        gripper = np.array(obs.gripper_pose[:3])
        goal = np.array(self.task._task.cup_source.get_position())
        dist = np.linalg.norm(gripper-goal)
        rew2 = 1 if goal[2] > 0.8 else 0
        rew1 = max((1-dist/self.params['goalRange']),0)
        return [rew1,rew2]

    def _getSubtask(self,obs):
        gripper = np.array(obs.gripper_pose[:3])
        goal = np.array(self.task._task.cup_source.get_position())
        dist = np.linalg.norm(gripper-goal)
        if dist > 0.3:
            return 0
        else:
            return 1

    def reset(self):
        reset = False
        while not reset:
            try:
                _, ob = self.task.reset()
                reset = True
            except:
                pass
        self.lastSubtask = self._getSubtask(ob)
        ob = self._getObs(ob)
        self.lastObs = ob

        return ob

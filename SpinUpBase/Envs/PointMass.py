import numpy as np
from gym import spaces



class PointMass():
    def __init__(self, **kwargs):
        self.scale = 20 if 'scale' not in kwargs else kwargs['scale']
        self.dim = 2 if 'dim' not in kwargs else kwargs['dim']
        self.randomReset = True if 'randomReset' not in kwargs else kwargs['randomReset']
        self._max_episode_step = 100 if '_max_episode_step' not in kwargs else kwargs['_max_episode_step']
        self.action_space = spaces.Box(-1,1,[self.dim])
        self.observation_space = spaces.Box(-1,1,[self.dim])
        self.reset()

    def reset(self):
        if self.randomReset:
            self.pos = np.clip(np.random.normal(self.scale/2, self.scale/4,[self.dim]),0,self.scale)
        else:
            self.pos = np.array([self.scale/2 for _ in range(self.dim)])
        self.ep_step = 0
        return self.pos

    def step(self, act):
        for n in range(len(act)):
            self.pos[n] += act[n]
            self.pos[n] = np.clip(self.pos[n],0,self.scale)
        self.ep_step += 1
        rew = -np.linalg.norm([np.array([self.scale/2 for _ in range(self.dim)])-self.pos])
        done = rew > -1 or self.ep_step >= self._max_episode_step
        return self.pos, rew, done, {'overallReward':rew}

import numpy as np, cv2, sys, os, math, random, time, torch
from gym import spaces
from Box2D import *
import os
from copy import deepcopy as copy

class GridReacher:
    def __init__(self,**kwargs):
        self.dim = 2 if 'dim' not in kwargs else kwargs['dim']
        self.action_space = spaces.Box(low=np.array([-1 for _ in range(self.dim)]), 
                                       high=np.array([1 for _ in range(self.dim)]), dtype=np.float64)
        self.observation_space = spaces.Box(low=-1, high=1, shape=[2*self.dim])
        self.nSubtasks = 1
        self.lowBound = 0
        self.highBound = 1
        self.stepSize = 0.02
        self._max_episode_steps = 100
        self.reset()

    def reset(self):
        self.target = np.random.uniform(self.lowBound,self.highBound,size = [self.dim])
        self.agent = np.random.uniform(self.lowBound,self.highBound,size = [self.dim])

        obs = np.concatenate((self.target,self.agent))
        self._ep_step = 0
        return obs

    def step(self,act):
        for n, a in enumerate(act):
            self.agent[n] += a.item() *self.stepSize
            self.agent[n] = max(self.lowBound,min(self.highBound,self.agent[n]))

        rew = self.getReward()
        info = {'success':1 if rew > -self.dim**-0.5*self.stepSize else 0,
                'subtask':0}
        obs = np.concatenate((self.target,self.agent))

        self._ep_step += 1
        done = False
        if self._ep_step >= self._max_episode_steps:
            done = True
        
        return obs, rew, done, info

    def getReward(self):
        dist = np.linalg.norm(self.target-self.agent)
        return -dist


if __name__ == "__main__":
    env = GridReacher()
    env.step(np.array([1,1]))
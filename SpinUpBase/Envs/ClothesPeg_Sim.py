#!/usr/bin/env python3

from xmlrpc.client import Boolean
import rospy
import tf2_ros
import actionlib
import numpy as np, sys, math, torch
import torch.nn as nn
import cv2

from cv_bridge import CvBridge

from collections import deque

from copy import deepcopy as copy
from gym import spaces

from collections import deque

from subprocess import call
import time
# import bezier

# from trajectory_generator import TrajectoryGenerator

# from pyquaternion import Quaternion as Q

def showImage(image,wait = 0):
    cv2.imshow("image",image)
    cv2.waitKey(wait)


def smooth(val,lastVal,maxChange):
  return lastVal+np.clip(val-lastVal,-maxChange,maxChange)

def angle_diff(angle1,angle2):
  a = angle1 - angle2
  a -= math.pi*2 if a > math.pi else 0
  a += math.pi*2 if a < -math.pi else 0
  return a

class Expert:
  def __init__(self,noise=0.02):
    self.noise = noise

  def get_actions(self,o):
    device = o.device
    actions = [self.get_action(obs) for obs in o.cpu()]
    actions = torch.tensor(actions,device = device,dtype = o.dtype)
    return actions

  def get_action(self,observation):
    obs = observation
    if obs[-1] ==0:
      action = np.array([np.clip(10*(obs[-2]-obs[0]-0.022),-0.5,0.5),1*(abs(obs[-2]-obs[0]-0.022) < 0.005)-0.5])#
    else:
      action = np.array([0.5,-1* (abs(obs[-2] - 0.73)<0.005)+0.5])

    action = np.clip(np.random.normal(action,self.noise),-1,1)
    return action

class Peg:
  def __init__(self, **args):

    self._max_episode_steps = 20 if '_max_episode_steps' not in args else args['_max_episode_steps']
    self.useVel = True if 'useVel' not in args else args['useVel']
    self.randomStart = True if 'randomStart' not in args else args['randomStart']
    self.endOnOutOfBounds = 0 if 'endOnOutOfBounds' not in args else args['endOnOutOfBounds']
    self.rewardType = 'dense' if 'rewardType' not in args else args['rewardType']
    self.headless = True if 'headless' not in args else args['headless']
    self.clipDist = 0.01 if 'clipDist' not in args else args['clipDist']
    self.graspAfter = True if 'graspAfter' not in args else args['graspAfter']

    max_speed = 0.1 if 'maxSpeed' not in args else args['maxSpeed']


    self.error = 0
    self.epStep = 0
    self.grasped = False

    self.graspedLocations = deque(maxlen = 1000)

    self.pegSize = [0.026,0.035]

    self.realSenseFOVAngles = {'horizontal':69.4,
                               'vertical':42.5}
    self.realSenseRes = [720,1280]


    self.bounds = [0.45,0.60]
    self.homePose = [0.532,-0.037,0.169]

    self.placeHeight = 0.279
    self.placeBounds = [0.45,0.71]

    self.clipPos = 0.73



    # action parameters
    self.acceleration_constraint = 0.1 #m/s/s
    self.max_speed = max_speed #m/s
    self.max_angle_speed = 0.5 #rad/s
    self.hz = 100
    self.actHz = 5 #Hz
    
    self.stepScale = 1/self.hz


    self.nSubtasks = 2

    # self.bounds = [self.xLim, self.yLim, self.zLim]
    self.ee_pose = np.array([0,0,0])
    self.frame = None
    self.gripper_position = None
    self.moveStatus = None
    self.graspStatus = None
    self.failedToReset = False

    # render settings
    self.w = 600
    self.h = 100

    self.bg_image = cv2.resize(cv2.cvtColor(np.random.randint(225,256,(self.w//8,self.h//8)).astype(np.uint8), cv2.COLOR_GRAY2BGR), dsize=(self.h,self.w), interpolation=cv2.INTER_NEAREST)
    


    self.actSpace = 2
    self.obsSpace = 3
    self.obsSpace += 1 if self.useVel else 0

    self.action_space = spaces.Box(low=np.array([-1 for _ in range(self.actSpace)]),
                                   high=np.array([1 for _ in range(self.actSpace)]),dtype=np.float64)
    
    self.observation_space = spaces.Box(low=-1,high=1,shape=[self.obsSpace])


    
    currentObs = copy(self.ee_pose)
    lastObs = currentObs+1
    self.episode = 1
    self.lastEval = False
    self.graspedThisEp = False
    self.reset()


    


  def reset(self,eval = False):
    self.epStep = 0
    self.lastAction = np.zeros(self.actSpace-1)
    self.missed_grasp = False


    
    self.pegPos = np.random.uniform(0.5,0.6)

    self.grasped = 0
    self.graspedThisEp = 0
    
    self.x = np.random.uniform(self.bounds[0]+0.01,self.bounds[1]-0.01)
    self.xVel = 0
    self.epStep = 0
    self.lastAction = np.zeros(self.actSpace-1)
    self.episode += 1

    if eval and not self.lastEval:
      self.showGraspDistribution()
    self.lastEval = eval

    return self._obs()


  def _obs(self):
    obs = np.array([self.x])
    if self.useVel:
      obs = np.concatenate([obs,np.array([self.xVel/self.stepScale])])
    obs = np.concatenate([obs,np.array([self.pegPos,self.grasped])])
    
    return obs

  def reward(self,type = "total"):
    if self.rewardType == "dense":
      reachDist = abs(self.x - self.pegPos)
      clipDist = abs(self.pegPos-self.clipPos)
      clipRew = 0.5 * math.exp(-5*clipDist) + 0.5 * (clipDist < self.clipDist) * (not self.grasped)
      allRew = {"reach":math.exp(-5*reachDist),
                "clip":clipRew}
      totalRew = np.sum([allRew[r] for r in allRew])
    else:
      clipDist = abs(self.pegPos-self.clipPos)
      clipRew = (clipDist < self.clipDist) * (not self.grasped)
      allRew = {"reach":1*self.graspedThisEp,
                "clip":clipRew}
      totalRew = np.sum([allRew[r] for r in allRew])
      
    if type == "total":
      return totalRew
    elif type == "all":
      return allRew

  def _checkOutBounds(self):
    if self.graspedThisEp:
      if self.x < self.placeBounds[0] or self.x > self.placeBounds[1]:
        return True
    else:
      if self.x < self.bounds[0] or self.x > self.bounds[1]:
        return True
    return False

  def smooth_action(self,act):
    accel_constraint = self.acceleration_constraint/self.hz
    action = smooth(act,self.lastAction,accel_constraint)
    self.lastAction = action
    return action

  def grasp(self, grasp = True):
    if ((self.x > (self.pegPos - self.pegSize[0])) and (self.x < (self.pegPos + self.pegSize[1]))):
      self.grasped = int(grasp)
      if self.grasped and not self.graspedThisEp:
        self.graspedLocations.append([self.pegPos-self.x,False])
      self.graspedThisEp = int(self.graspedThisEp or self.grasped)



  def showGraspDistribution(self):
    increment = 0.005
    print("Grasp Distribution")
    for point in np.arange(-0.05,0.05,increment):
      success = np.sum([1 if loc[1] else 0 for loc in self.graspedLocations if np.linalg.norm(loc[0]-point) < increment/2])
      failure = np.sum([0 if loc[1] else 1 for loc in self.graspedLocations if np.linalg.norm(loc[0]-point) < increment/2])
      print(f"   Point {point:.3f}: Successes - {success} | Failures - {failure} | Success Rate - {success/(success+failure)*100:.1f}%")
    


  def step(self, act):

    if not self.graspAfter:
      if act[-1] > 0 and not self.graspedThisEp:
        self.grasp(True)
      elif act[-1] < 0 and self.graspedThisEp:
        self.grasp(False)
      
    for _ in range(int(self.hz/self.actHz)):
      subtask = 0 if not self.graspedThisEp else 1
      sm_act = self.smooth_action(np.array([act[0]*self.max_speed]))
      if not self.endOnOutOfBounds:
        if (self.graspedThisEp and self.x < self.placeBounds[0]) or (not self.graspedThisEp and self.x < self.bounds[0]):
          sm_act[0] = max(0,sm_act[0])
        elif (self.graspedThisEp and self.x > self.placeBounds[1]) or (not self.graspedThisEp and self.x > self.bounds[1]):
          sm_act[0] = min(0,sm_act[0])
      self.xVel = sm_act[0]*self.stepScale # Arbitrary scaling to get it closer to the real environment
      self.x += self.xVel
      if self.grasped:
        self.pegPos += self.xVel
      if not self.headless:
        self.render()
    
    if self.graspAfter:
      if act[-1] > 0 and not self.graspedThisEp:
        self.grasp(True)
      elif act[-1] < 0 and self.graspedThisEp:
        self.grasp(False)
    subtask = 0 if not self.graspedThisEp else 1

    self.epStep += 1

    obs = self._obs()
    rews = list(self.reward("all").values())
    done = self._checkDone()
    info = {}
    info['overallReward'] = self.reward()
    info['reachReward'] = self.reward("all")["reach"]
    info['clipReward'] = self.reward("all")["clip"]
    info['subtask'] = subtask
    info['success'] = self._success()

    if self._success():
      self.graspedLocations[-1][1] = True

    return obs, rews[subtask], done, info


  def render(self):
    scaling = 1200
    offset = -600
    def w2p(point):
      return int(point * scaling + self.w/2 + offset)
    image = self.bg_image.copy()

    # insert target
    targetImage = cv2.rectangle(image,(0,w2p(self.clipPos-self.clipDist)),(self.h,w2p(self.clipPos+self.clipDist)),(200,250,200),-1)
    image = cv2.addWeighted(image,0.9,targetImage,0.1,0)
    image = cv2.line(image,(0,w2p(self.clipPos)),(self.h,w2p(self.clipPos)),(100,250,100),3)

    # insert bounds
    image = cv2.line(image,(0,w2p(self.bounds[0])),(self.h,w2p(self.bounds[0])),(100,100,250),2)
    if not self.graspedThisEp:
      image = cv2.line(image,(0,w2p(self.bounds[1])),(self.h,w2p(self.bounds[1])),(100,100,250),2)
    else:
      image = cv2.line(image,(0,w2p(self.placeBounds[1])),(self.h,w2p(self.placeBounds[1])),(100,100,250),2)


    # draw peg
    image = cv2.rectangle(image,(self.h//2+10,w2p(self.pegPos-self.pegSize[0])),(self.h//2-10,w2p(self.pegPos+self.pegSize[1])),(200,100,100),-1)
    image = cv2.circle(image,(self.h//2,w2p(self.pegPos)),5,(200,0,0),-1)

    # draw gripper
    colour = (200,250,200) if self.grasped else (200,200,250)
    height = 12 if self.grasped else 20
    image = cv2.rectangle(image,(self.h//2+height,w2p(self.x)+5),(self.h//2-height,w2p(self.x)-5),colour,-1)
    image = cv2.rectangle(image,(self.h//2+height,w2p(self.x)+5),(self.h//2-height,w2p(self.x)-5),(50,50,50),2)

    image = cv2.rotate(image,cv2.ROTATE_90_COUNTERCLOCKWISE)

    image = cv2.putText(image,f"Timestep: {self.epStep}",(10,10),cv2.FONT_HERSHEY_PLAIN,1,(100,100,100),2)

    if self._success():
      image = cv2.putText(image,"Successful",(10,25),cv2.FONT_HERSHEY_PLAIN,1,(0,0,0),2)

    cv2.imshow("ClothesPeg-Sim",image)
    cv2.waitKey(30)


  def _success(self):
    clipDist = abs(self.pegPos-self.clipPos)
    return clipDist < self.clipDist and not self.grasped

  def _checkDone(self):
    return (self.epStep >= self._max_episode_steps or self.error > 0 or (self._checkOutBounds() and self.endOnOutOfBounds))
    

if __name__ == "__main__":
  env = Peg(_max_episode_steps = 100,endOnOutOfBounds = 0,clipDist=0.003)

  exp = Expert()
  nEpisodes = 10
  for n in range(nEpisodes):
    
    done = False
    obs = env.reset()
    length = 0
    totalRew = 0
    while not done:

      # action = actor(torch.Tensor(obs))
      # print(action)
      action = exp.get_action(obs)
      nobs, rew, done, info = env.step(action)
      env.render()
      totalRew += rew
      print("action: ",action)
      print(f"position: {nobs[0]}, peg position: {nobs[-2]}")
      print(f"Success: {info['success']}")

      length += 1
      if length >= 200:
        done = True

      obs = nobs
      # print(length)

    print("Episode {}: \nOut of Bounds at termination: {}\nLength: {} \nAverage Reward: {}\n------".format(n,env._checkOutBounds(),length,totalRew/length))

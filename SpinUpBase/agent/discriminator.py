import torch.nn as nn

class Discriminator(nn.Module):
    def __init__(self, obs_dim, action_dim, hidden_dim, hidden_depth, use_action, alpha=0.5, lambd=10):
        super().__init__()
        self.obs_dim = obs_dim
        self.alpha = alpha
        self.lambd = lambd
        self.action_dim = action_dim
        self.use_action = use_action
        input_dim = obs_dim + (action_dim if use_action else 0)
        self.model = nn.Sequential(
            nn.Linear(input_dim, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, 1)
        )

    def forward(self, input, with_sig = True):
        outputs = self.model(input)
        sig = nn.Sigmoid()
        self.outputs = sig(outputs)
        if with_sig:
            return self.outputs
        else:
            return outputs

    def log(self, logger, step):
        logger.log_histogram(f'train_discrim/hist', self.outputs, step)

        for i, m in enumerate(self.model):
            if type(m) is nn.Linear:
                logger.log_param(f'train_discrim{i}', m, step)

    def return_state_dict(self):
        state_dict = {}
        state_dict['discrim'] = self.model.state_dict()
        return state_dict

    def load_model(self, state_dict):
        self.model.load_state_dict(state_dict['discrim'])
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import time

from agent import Agent
import utils

import hydra


class DDPGAgent(Agent):
    """DDPG algorithm."""
    def __init__(self, obs_dim, action_dim, action_range, device, critic_cfg,
                 actor_cfg, discount, actor_lr, actor_betas, actor_update_frequency, critic_lr,
                 critic_betas, tau, critic_target_update_frequency,
                 noiseReduction, noiseRandom,
                 batch_size,done_no_max=True):
        super().__init__()

        self.action_range = action_range
        self.device = torch.device(device)
        self.discount = discount
        self.tau = tau
        self.actor_update_frequency = actor_update_frequency
        self.critic_target_update_frequency = critic_target_update_frequency
        self.batch_size = batch_size
        self.done_no_max = done_no_max

        self.critic = hydra.utils.instantiate(critic_cfg).to(self.device)
        self.critic_target = hydra.utils.instantiate(critic_cfg).to(
            self.device)
        self.critic_target.load_state_dict(self.critic.state_dict())

        self.actor = hydra.utils.instantiate(actor_cfg).to(self.device)
        self.actor_target = hydra.utils.instantiate(actor_cfg).to(
            self.device)
        self.actor_target.load_state_dict(self.actor.state_dict())



        # optimizers
        self.actor_optimizer = torch.optim.Adam(self.actor.parameters(),
                                                lr=actor_lr,
                                                betas=actor_betas)

        self.critic_optimizer = torch.optim.Adam(self.critic.parameters(),
                                                 lr=critic_lr,
                                                 betas=critic_betas)

        self.noiseReduction = noiseReduction
        self.noiseRandom = noiseRandom
        self.noise = 0

        self.train()
        self.critic_target.train()

        self.times = dict(sample = [], actor_update = [], critic_update = [])

    def reset(self, eval = False):
        self.noise = 0
        super().reset()

    
    def train(self, training=True):
        self.training = training
        self.actor.train(training)
        self.critic.train(training)


    def act(self, obs, sample=False, env_info = None):
        obs = torch.FloatTensor(obs).to(self.device)
        obs = obs.unsqueeze(0)
        act = self.actor(obs)
        if sample:
            self.noise = self.noise * self.noiseReduction + self.noiseRandom*np.random.uniform(-1,1)
            action = act+self.noise
        else:
            action = act
        action = action.clamp(*self.action_range)
        assert action.ndim == 2 and action.shape[0] == 1
        return utils.to_np(action[0])

    def update_critic(self, obs, action, reward, next_obs, not_done, logger,
                      step):

        next_Q = self.critic_target(next_obs,self.actor_target(next_obs))
        target_Q = reward + (not_done * self.discount * next_Q)
        target_Q = target_Q.detach()

        # get current Q estimates
        current_Q = self.critic(obs, action)
        critic_loss = F.mse_loss(current_Q, target_Q)
        logger.log('train_critic/loss', critic_loss, step)

        # Optimize the critic
        self.critic_optimizer.zero_grad()
        critic_loss.backward()
        self.critic_optimizer.step()

        self.critic.log(logger, step)

    def update_actor(self, obs, logger, step):

        actor_loss = -torch.mean(self.critic(obs,self.actor(obs)))

        logger.log('train_actor/loss', actor_loss, step)

        # optimize the actor
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()

        self.actor.log(logger, step)



    def update(self, replay_buffer, logger, step):
        timer = time.time()
        obs, action, reward, next_obs, not_done, not_done_no_max, env_infos = replay_buffer.sample(
            self.batch_size, return_env_info = True)
        if 'overallReward' in env_infos[0]:
            env_infos = {k: [dic[k] for dic in env_infos] for k in env_infos[0]}
            reward = torch.Tensor(env_infos['overallReward']).unsqueeze(1).to(replay_buffer.device)
            if obs.device == "cuda":
                reward = reward.cuda()
        self.times['sample'].append(time.time()-timer)
        timer = time.time()

        logger.log('train/batch_reward', reward.mean(), step)

        dones = not_done_no_max if self.done_no_max else not_done

        self.update_critic(obs, action, reward, next_obs, dones,
                           logger, step)
        self.times['critic_update'].append(time.time()-timer)
        timer = time.time()

        self.update_actor(obs, logger, step)
        self.times['actor_update'].append(time.time()-timer)
        timer = time.time()

        if step % self.critic_target_update_frequency == 0:
            utils.soft_update_params(self.critic, self.critic_target,
                                     self.tau)
            utils.soft_update_params(self.actor, self.actor_target,
                                     self.tau)

    def reset_times(self):
        self.times = dict(sample = [], actor_update = [], critic_update = [])

    def print_times(self):
        print('---')
        for elem in self.times:
            print(f'{elem}: {np.mean(self.times[elem])}')

    def log_times(self, logger, step):
        for elem in self.times:
            logger.log(f'timing/{elem}', np.mean(self.times[elem]), step)

    def save_model(self, logger, step):
        logger.save_model('Actor',self.actor.return_state_dict(),step)
        logger.save_model('Critic',self.critic.return_state_dict(),step)

    def load_model(self, location):
        self.actor.load_model(torch.load(f'{location}/Actor.pt'))
        self.critic.load_model(torch.load(f'{location}/Critic.pt'))



import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import time

from agent import Agent
from sac import SACAgent
import utils

import hydra

class singleSAC(SACAgent):
    def __init__(self,*args, subtask_id):
        super().__init__(*args)
        self.subtask_id = subtask_id

        self.times = dict(sample = [], actor_update = [], critic_update = [])

    def update_critic(self, obs, action, reward, next_obs, not_done, logger,
                      step):
        dist = self.actor(next_obs)
        next_action = dist.rsample()
        log_prob = dist.log_prob(next_action).sum(-1, keepdim=True)
        target_Q1, target_Q2 = self.critic_target(next_obs, next_action)
        target_V = torch.min(target_Q1,
                             target_Q2) - self.alpha.detach() * log_prob
        target_Q = reward + (not_done * self.discount * target_V)
        target_Q = target_Q.detach()

        # get current Q estimates
        current_Q1, current_Q2 = self.critic(obs, action)
        critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
            current_Q2, target_Q)
        logger.log(f'train/critic_{self.subtask_id}/loss', critic_loss, step)

        # Optimize the critic
        self.critic_optimizer.zero_grad()
        critic_loss.backward()
        self.critic_optimizer.step()

        self.critic.log(logger, step)

    def update_actor_and_alpha(self, obs, logger, step):
        dist = self.actor(obs)
        action = dist.rsample()
        log_prob = dist.log_prob(action).sum(-1, keepdim=True)
        actor_Q1, actor_Q2 = self.critic(obs, action)

        actor_Q = torch.min(actor_Q1, actor_Q2)
        actor_loss = (self.alpha.detach() * log_prob - actor_Q).mean()

        logger.log(f'train/actor_{self.subtask_id}/loss', actor_loss, step)
        logger.log(f'train/actor_{self.subtask_id}/target_entropy', self.target_entropy, step)
        logger.log(f'train/actor_{self.subtask_id}/entropy', -log_prob.mean(), step)

        # optimize the actor
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()

        self.actor.log(logger, step)

        if self.learnable_temperature:
            self.log_alpha_optimizer.zero_grad()
            alpha_loss = (self.alpha *
                          (-log_prob - self.target_entropy).detach()).mean()
            logger.log(f'train/alpha_{self.subtask_id}/loss', alpha_loss, step)
            logger.log(f'train/alpha_{self.subtask_id}/value', self.alpha, step)
            alpha_loss.backward()
            self.log_alpha_optimizer.step()

    def update(self, replay_buffer, logger, step):
        timer = time.time()
        try:
            obs, action, reward, next_obs, not_done, not_done_no_max = replay_buffer.subtask_sample(
                self.batch_size, subtask_id = self.subtask_id)
        except:
            return None

        self.times['sample'].append(time.time()-timer)
        timer = time.time()

        logger.log(f'train/agent_{self.subtask_id}/batch_reward', reward.mean(), step)

        self.update_critic(obs, action, reward, next_obs, not_done_no_max,
                           logger, step)
        
        self.times['critic_update'].append(time.time()-timer)
        timer = time.time()

        if step % self.actor_update_frequency == 0:
            self.update_actor_and_alpha(obs, logger, step)
            self.times['actor_update'].append(time.time()-timer)

        if step % self.critic_target_update_frequency == 0:
            utils.soft_update_params(self.critic, self.critic_target,
                                     self.critic_tau)


class uncoopSAC(SACAgent):
    def __init__(self, obs_dim, action_dim, action_range, device, critic_cfg,
                 actor_cfg, discount, init_temperature, alpha_lr, alpha_betas,
                 actor_lr, actor_betas, actor_update_frequency, critic_lr,
                 critic_betas, critic_tau, critic_target_update_frequency,
                 batch_size, learnable_temperature, 
                 nSubtasks, done_no_max = True):


        args = [obs_dim, action_dim, action_range, device, critic_cfg,
                 actor_cfg, discount, init_temperature, alpha_lr, alpha_betas,
                 actor_lr, actor_betas, actor_update_frequency, critic_lr,
                 critic_betas, critic_tau, critic_target_update_frequency,
                 batch_size, learnable_temperature,done_no_max]

        self.agents = [singleSAC(*args,subtask_id=n) for n in range(nSubtasks)]
        super().__init__(*args)


        

    def train(self, training = True):
        self.training = training
        for agent in self.agents:
            agent.train(training)

    def act(self, obs, sample = False, env_info = None):
        # obs = torch.FloatTensor(obs).to(self.device)
        # obs = obs.unsqueeze(0)
        subtask = env_info['subtask'] if env_info is not None else 0
        action = self.agents[subtask].act(obs, sample)
        return action

    def update(self, replay_buffer, logger, step):
        for agent in self.agents:
            agent.update(replay_buffer, logger, step)

    def reset_times(self):
        for agent in self.agents:
            agent.times = dict(sample = [], actor_update = [], critic_update = [])

    def print_times(self):
        print('---')
        for n, agent in enumerate(self.agents):
            print(f'Agent {n}')
            for elem in agent.times:
                print(f'   {elem}: {np.mean(agent.times[elem])}')

    def log_times(self, logger, step):
        for n, agent in enumerate(self.agents):
            for elem in agent.times:
                logger.log(f'timing/agent{n}/{elem}', np.mean(agent.times[elem]), step)

    def save_model(self, logger, step):
        for n,agent in enumerate(self.agents):
            logger.save_model(f'Agent{n}/Actor',agent.actor.return_state_dict(),step)
            critic_state = agent.critic.return_state_dict()
            for q in critic_state:
                logger.save_model(f'Agent{n}/Critic/{q}',critic_state[q],step)

    def load_model(self, location):
        for n, agent in enumerate(self.agents):
            agent.actor.load_model(torch.load(f'{location}/Agent{n}/Actor.pt'))
            critic_state_dict = {}
            critic_state_dict['Q1'] = torch.load(f'{location}/Agent{n}/Critic/Q1.pt')
            critic_state_dict['Q2'] = torch.load(f'{location}/Agent{n}/Critic/Q2.pt')
            agent.critic.load_model(critic_state_dict)
from spinup.algos.pytorch.ppo.ppo import ppo as spinupPPO
from spinup.algos.pytorch.ppo.core import MLPActorCritic
import hydra


class ppo(spinupPPO):
    def __init__(self, **kwargs):
        ac = MLPActorCritic(observation_space = kwargs['obs_dim'], action_space=kwargs['action_dim'])
        self.envCfg = kwargs['envInfo']
        kwargs.remove('obs_dim')
        kwargs.remove('action_dim')
        super.__init__(self.createEnv,ac,**kwargs)

    def createEnv(self):
        env = hydra.utils.instantiate(self.envCfg)
        env._max_episode_steps = self.envCfg.params._max_episode_steps
        return env


if __name__ == "__main__":
    agent = ppo(obs_dim = 3, action_dim = 3)
    
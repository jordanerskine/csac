import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import time
import cv2
from copy import deepcopy as copy


from replay_buffer import ReplayBuffer

from agent import Agent
from sac import SACAgent
import utils

import hydra


class singleSAC(SACAgent):
    def __init__(self, *args, subtask_id, coop_ratio, rectify_norm, limitedDomain):
        super().__init__(*args)
        self.subtask_id = subtask_id
        self.coop_ratio = coop_ratio
        self.rectify_norm = rectify_norm
        self.limitedDomain = limitedDomain

        self.nextAgent = None
        self.previousAgent = None

        self.randomObs = None
        self.randomAct = None

        self.times = dict(sample = [], actor_update = [], critic_update = [])

    def set_next_agent(self,agent):
        self.nextAgent = agent

    def set_previous_agent(self,agent):
        self.previousAgent = agent

    def normalise(self, batch):
        mn = torch.min(batch).detach()
        mx = torch.max(batch).detach()
        return (batch-mn)/(mx-mn)

    def batch_range(self, batch):
        return torch.max(batch).detach()-torch.min(batch).detach()

    def update_actor_and_alpha(self, obs, logger, step):
        # The n here represents the next domain. The actor needs to be 
        # trained with respect to the next domain and critic

        # Get the loss for the no future agent case
        if self.nextAgent is None:
            dist = self.actor(obs)
            action = dist.rsample()
            log_prob = dist.log_prob(action).sum(-1, keepdim=True)
            actor_Q1, actor_Q2 = self.critic(obs, action)

            actor_Q = torch.min(actor_Q1, actor_Q2)
            actor_loss = (self.alpha.detach() * log_prob - actor_Q).mean()

        # Get the loss for the case with a future agent
        else:
            dist = self.actor(obs)
            action = dist.rsample()
            log_prob = dist.log_prob(action).sum(-1, keepdim=True)
            n_actor_Q1, n_actor_Q2 = self.nextAgent.critic(obs, action)
            actor_Q1, actor_Q2 = self.critic(obs, action)

            n_actor_Q = torch.min(n_actor_Q1, n_actor_Q2)
            actor_Q = torch.min(actor_Q1, actor_Q2)

            norm_n_Q = self.normalise(n_actor_Q)
            norm_Q = self.normalise(actor_Q)
            # norm_n_Q = n_actor_Q
            # norm_Q = actor_Q
            if self.rectify_norm:
                scaling_factor = self.coop_ratio*self.batch_range(actor_Q)+(1-self.coop_ratio)*self.batch_range(n_actor_Q)
            else:
                scaling_factor = 1

            actor_loss = (self.alpha.detach()*log_prob - scaling_factor*(self.coop_ratio*norm_Q + (1-self.coop_ratio)*norm_n_Q)).mean()

        logger.log(f'train/actor_{self.subtask_id}/loss', actor_loss, step)
        logger.log(f'train/actor_{self.subtask_id}/target_entropy', self.target_entropy, step)
        logger.log(f'train/actor_{self.subtask_id}/entropy', -log_prob.mean(), step)

        # optimize the actor
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()

        self.actor.log(logger, step, suffix=f'_{self.subtask_id}')

        if self.learnable_temperature:
            self.log_alpha_optimizer.zero_grad()
            alpha_loss = (self.alpha *
                          (-log_prob - self.target_entropy).detach()).mean()
            logger.log(f'train/alpha_{self.subtask_id}/loss', alpha_loss, step)
            logger.log(f'train/alpha_{self.subtask_id}/value', self.alpha, step)
            alpha_loss.backward()
            self.log_alpha_optimizer.step()

    def update_critic(self, obs, action, reward, next_obs, not_done,  
                      logger, step,
                      p_obs = None, p_action = None, p_reward = None, p_next_obs = None, p_not_done = None):
        # The p refers to the previous domain. The critic needs to be trained
        # with respect to the current and the previous domain
        
        # Get loss for the current domain
        if obs is None:
            noCurrent = True
        else:
            noCurrent = False
            dist = self.actor(next_obs)
            next_action = dist.rsample()
            log_prob = dist.log_prob(next_action).sum(-1, keepdim=True)
            target_Q1, target_Q2 = self.critic_target(next_obs, next_action)
            target_V = torch.min(target_Q1,
                                target_Q2) - self.alpha.detach() * log_prob
            target_Q = reward + (not_done * self.discount * target_V)
            target_Q = target_Q.detach()

            # get current Q estimates
            current_Q1, current_Q2 = self.critic(obs, action)
            current_critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
                current_Q2, target_Q)

            # if torch.argmax(reward) != torch.argmax(target_Q) and \
            #         (torch.max(reward)-reward[torch.argmax(target_Q)]) > 5:
            #     rewPoint = torch.argmax(reward)
            #     targPoint = torch.argmax(target_Q)
            #     print(f"Reward: {reward[rewPoint]}\nTarget Q: {target_Q[rewPoint]}")
            #     print(f"Reward: {reward[targPoint]}\nTarget Q: {target_Q[targPoint]}")
            #     print("Disjoint")


        
        # Get loss for the previous domain
        if self.previousAgent is not None:
            dist = self.previousAgent.actor(p_next_obs)
            next_action = dist.rsample()
            log_prob = dist.log_prob(next_action).sum(-1, keepdim=True)
            target_Q1, target_Q2 = self.critic_target(p_next_obs, next_action)
            target_V = torch.min(target_Q1,
                                target_Q2) - self.alpha.detach() * log_prob
            target_Q = p_reward + (p_not_done * self.discount * target_V)
            target_Q = target_Q.detach()

            # get current Q estimates
            current_Q1, current_Q2 = self.critic(p_obs, p_action)
            previous_critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
                current_Q2, target_Q)

            if noCurrent:
                critic_loss = previous_critic_loss
            else:
                critic_loss = current_critic_loss + previous_critic_loss
        else:
            if noCurrent:
                critic_loss = None
            else:
                critic_loss = current_critic_loss

        # if not noCurrent:
        #     logger.log(f'train_critic_{self.subtask_id}/loss_current_domain', current_critic_loss, step)
        # if self.previousAgent is not None:
        #     logger.log(f'train_critic_{self.subtask_id}/loss_previous_domain', previous_critic_loss, step)
        if critic_loss is not None:
            logger.log(f'train/critic_{self.subtask_id}/loss', critic_loss, step)


        # Optimize the critic
        if critic_loss is not None:
            self.critic_optimizer.zero_grad()
            critic_loss.backward()
            self.critic_optimizer.step()

            self.critic.log(logger, step, suffix=f"_{self.subtask_id}")
    
    def update(self, replay_buffer, logger, step):
        # Get data from this agent's domain
        timer = time.time()
        if self.limitedDomain:
            try:
                obs, action, reward, next_obs, not_done, not_done_no_max = replay_buffer.subtask_sample(
                    self.batch_size, subtask_id = self.subtask_id)
            except:
                obs, action, reward, next_obs, not_done, not_done_no_max = None, None, None, None, None, None

            # Get data from the previous agent's domain
            p_obs, p_action, p_reward, p_next_obs, p_not_done, p_not_done_no_max, p_env_infos = None, None, None, None, None, None, None
            try:
                if self.previousAgent is not None:
                    p_obs, p_action, p_reward, p_next_obs, p_not_done, p_not_done_no_max, p_env_infos = replay_buffer.subtask_sample(
                        self.batch_size, subtask_id = self.subtask_id-1, return_env_info=True)
                    p_reward = torch.Tensor(np.array([info[f'reward{self.subtask_id}'] for info in p_env_infos])).unsqueeze(1).to(replay_buffer.device)
            except:
                pass
        else:
            obs, action, _, next_obs, not_done, not_done_no_max, env_infos = replay_buffer.sample(self.batch_size, return_env_info = True)
            reward = torch.Tensor([timestep[f'reward{self.subtask_id}'] for timestep in env_infos]).to(replay_buffer.device)
            if self.previousAgent is not None:
                p_reward = torch.as_tensor([timestep[f'reward{self.subtask_id-1}'] for timestep in env_infos]).to(replay_buffer.device)
                p_obs, p_action, p_next_obs, p_not_done, p_not_done_no_max = obs, action, next_obs, not_done, not_done_no_max

        if obs is not None:
            self.randomObs = obs[0]
            self.randomAct = action[0]
        self.times['sample'].append(time.time()-timer)
        timer = time.time()
        if reward is not None:
            logger.log(f'train/agent_{self.subtask_id}/batch_reward', reward.mean(), step)

        if obs is not None:
            if self.previousAgent is not None and p_obs is not None:
                self.update_critic(obs, action, reward, next_obs, not_done_no_max,
                            logger, step,
                            p_obs, p_action, p_reward, p_next_obs, p_not_done_no_max)    
            else:
                self.update_critic(obs, action, reward, next_obs, not_done_no_max,
                                logger, step)
            self.times['critic_update'].append(time.time()-timer)
            timer = time.time()

        if step % self.actor_update_frequency == 0 and obs is not None:
            self.update_actor_and_alpha(obs, logger, step)
            self.times['actor_update'].append(time.time()-timer)

        if step % self.critic_target_update_frequency == 0:
            utils.soft_update_params(self.critic, self.critic_target,
                                     self.critic_tau)



class CSAC_G(SACAgent):
    def __init__(self, obs_dim, action_dim, action_range, device, critic_cfg,
                 actor_cfg, expert_cfg, discrim_cfg, discount, init_temperature, alpha_lr, alpha_betas,
                 actor_lr, actor_betas, actor_update_frequency, critic_lr, discrim_lr, discrim_betas,
                 critic_betas, critic_tau, critic_target_update_frequency,
                 batch_size, learnable_temperature, fill_all_subtasks,
                 nSubtasks, cooperative_ratio, rectify_norm,
                 expert_buffer_size, discrim_weight, 
                 limitedDomain = True, done_no_max = True):
        

        args = [obs_dim, action_dim, action_range, device, critic_cfg,
                 actor_cfg, discount, init_temperature, alpha_lr, alpha_betas,
                 actor_lr, actor_betas, actor_update_frequency, critic_lr,
                 critic_betas, critic_tau, critic_target_update_frequency,
                 batch_size, learnable_temperature, done_no_max]
        # print(args)
        
        if not isinstance(cooperative_ratio,list):
            cooperative_ratio = [cooperative_ratio for _ in range(nSubtasks)]

        self.batch_size = batch_size
        self.dWeight = hydra.utils.instantiate(discrim_weight)
        self.device = device
        self.discrim = hydra.utils.instantiate(discrim_cfg).to(self.device)
        self.fill_all_subtasks = fill_all_subtasks

        self.expert = hydra.utils.instantiate(expert_cfg)
        self.episode = 0


        self.discrim_optimizer = torch.optim.Adam(self.discrim.parameters(),
                                                lr=discrim_lr,
                                                betas=discrim_betas)

        self.expert_buffer = ReplayBuffer((actor_cfg.params.obs_dim,),
                                          (actor_cfg.params.action_dim,),
                                          int(expert_buffer_size), self.device,nSubtasks=nSubtasks)

        self.agents = [singleSAC(*args, subtask_id = i, coop_ratio=cooperative_ratio[i], rectify_norm=rectify_norm, limitedDomain=limitedDomain) 
                            for i in range(nSubtasks)]
        for n in range(1, len(self.agents)):
            self.agents[n-1].set_next_agent(self.agents[n])
            self.agents[n].set_previous_agent(self.agents[n-1])
        super().__init__(*args)


    
    def preload(self, env,replay_buffer = None):
        if self.fill_all_subtasks:
            while not np.all(np.array([subBuff.full for subBuff in self.expert_buffer.subtasks])):
                obs = env.reset()
                done = False
                print(f"Buffer fill: {int(np.average([(subBuff.idx if not subBuff.full else subBuff.capacity) for subBuff in self.expert_buffer.subtasks]))}/{self.expert_buffer.capacity}")
                while not done:
                    act = self.expert.get_action(obs)
                    nobs, rew, done, info = env.step(act)
                    # env.render()
                    self.expert_buffer.add(obs, act, rew, nobs,done,0, info)
                    obs = copy(nobs)

        else:
            while not self.expert_buffer.full:
                obs = env.reset()
                done = False
                print(f"Buffer fill: {self.expert_buffer.idx}/{self.expert_buffer.capacity}")
                while not done:
                    act = self.expert.get_action(obs)
                    nobs, rew, done, info = env.step(act)
                    self.expert_buffer.add(obs, act, rew, nobs,done,0, info)
                    obs = copy(nobs)

    def update_discrim(self,expert_obs, expert_action, policy_obs, policy_action, logger, step):
        if self.discrim.use_action:
            policy_input = torch.cat([policy_obs,policy_action],axis = -1)
            expert_input = torch.cat([expert_obs,expert_action],axis = -1)
        else:
            policy_input = policy_obs
            expert_input = expert_obs

        policy_output = self.discrim.forward(policy_input)
        expert_output = self.discrim.forward(expert_input)

        policy_labels = torch.zeros(policy_output.shape).to(self.device)
        expert_labels = torch.ones(expert_output.shape).to(self.device)

        policy_loss = F.binary_cross_entropy(policy_output,policy_labels)
        expert_loss = F.binary_cross_entropy(expert_output,expert_labels)

        ganLoss = policy_loss+expert_loss

        # This section is from the pytorch implementation within goalGAIL
        inter = self.discrim.alpha * policy_input + (1-self.discrim.alpha)*expert_input
        inter.requires_grad = True
        interOut = torch.mean(self.discrim(inter,False))
        interOut.backward()
        grad = inter.grad
        grad_penalty = torch.mean((torch.norm(grad)-1)**2)
        loss = ganLoss + self.discrim.lambd*grad_penalty


        logger.log('train_discrim/loss', ganLoss, step)
        logger.log('train_discrim/expert_loss', expert_loss, step)
        logger.log('train_discrim/policy_loss', policy_loss, step)

        self.discrim_optimizer.zero_grad()
        loss.backward()
        self.discrim_optimizer.step()
        self.discrim.log(logger, step)

    def reset(self, eval = False):
        if not eval:
            self.episode += 1
        self.dWeight.update(self.episode)
        self.discrim_weight = self.dWeight.get()

        
    def train(self, training = True):
        self.training = training
        for agent in self.agents:
            agent.train(training)

    def act(self, obs, sample = False, env_info = None):
        # obs = torch.FloatTensor(obs).to(self.device)
        # obs = obs.unsqueeze(0)
        subtask = env_info['subtask'] if env_info is not None else 0
        action = self.agents[subtask].act(obs, sample)
        # action = np.array([0,0,-1,0])
        return action

    
    def augmentReward(self, reward, obs, action):
        input = np.concatenate([obs,action])
        discrim1 = self.discrim(torch.Tensor(input).to(self.device))
        discrim = -math.log(1-discrim1+1e-8)
        outReward = (1-self.discrim_weight)*reward + self.discrim_weight*discrim
        return outReward

    def update(self, replay_buffer, logger, step):
        for agent in self.agents:
            agent.update(replay_buffer, logger, step)

            if replay_buffer.subtasks[agent.subtask_id].idx > self.batch_size or replay_buffer.subtasks[agent.subtask_id].full:
                expert_obs, expert_action, _,_,_,_ = self.expert_buffer.subtask_sample(self.batch_size,agent.subtask_id)
                policy_obs, policy_action, _,_,_,_ = replay_buffer.subtask_sample(self.batch_size,agent.subtask_id)

                self.update_discrim(expert_obs,expert_action,policy_obs,policy_action,logger,step)


    def reset_times(self):
        for agent in self.agents:
            agent.times = dict(sample = [], actor_update = [], critic_update = [])

    def print_times(self):
        print('---')
        for n, agent in enumerate(self.agents):
            print(f'Agent {n}')
            for elem in agent.times:
                print(f'   {elem}: {np.mean(agent.times[elem])}')

    def log_times(self, logger, step):
        for n, agent in enumerate(self.agents):
            for elem in agent.times:
                logger.log(f'timing/agent{n}/{elem}', np.mean(agent.times[elem]), step)
                

    def save_model(self, logger, step):
        for n,agent in enumerate(self.agents):
            logger.save_model(f'Agent{n}/Actor',agent.actor.return_state_dict(),step)
            critic_state = agent.critic.return_state_dict()
            for q in critic_state:
                logger.save_model(f'Agent{n}/Critic/{q}',critic_state[q],step)
        
        logger.save_model('Discriminator',self.discrim.return_state_dict(),step)

    def load_model(self, location):
        for n, agent in enumerate(self.agents):
            agent.actor.load_model(torch.load(f'{location}/Agent{n}/Actor.pt'))
            critic_state_dict = {}
            critic_state_dict['Q1'] = torch.load(f'{location}/Agent{n}/Critic/Q1.pt')
            critic_state_dict['Q2'] = torch.load(f'{location}/Agent{n}/Critic/Q2.pt')
            agent.critic.load_model(critic_state_dict)
        self.discrim.load_model(torch.load(f'{location}/Discriminator.pt'))
        
        



# if __name__ == "__main__":

    
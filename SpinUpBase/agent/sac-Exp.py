import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import time

from agent import Agent
import utils

import hydra


class SAC_E(Agent):
    """SAC algorithm."""
    def __init__(self, obs_dim, action_dim, action_range, device, critic_cfg,
                 actor_cfg, discount, init_temperature, alpha_lr, alpha_betas,
                 actor_lr, actor_betas, actor_update_frequency, critic_lr,
                 critic_betas, critic_tau, critic_target_update_frequency,
                 batch_size, learnable_temperature,
                 expert_sample,expert_cfg,
                  done_no_max=True, 
                 prefill_replay_buffer = 0, prefill_agent_cfg = None):
        super().__init__()

        self.action_range = action_range
        self.device = torch.device(device)
        self.discount = discount
        self.critic_tau = critic_tau
        self.actor_update_frequency = actor_update_frequency
        self.critic_target_update_frequency = critic_target_update_frequency
        self.batch_size = batch_size
        self.learnable_temperature = learnable_temperature
        self.done_no_max = done_no_max
        self.prefill_replay_buffer = prefill_replay_buffer
        self.prefill_agent_cfg = prefill_agent_cfg

        self.episode = 0

        self.critic = hydra.utils.instantiate(critic_cfg).to(self.device)
        self.critic_target = hydra.utils.instantiate(critic_cfg).to(
            self.device)
        self.critic_target.load_state_dict(self.critic.state_dict())

        self.actor = hydra.utils.instantiate(actor_cfg).to(self.device)

        self.log_alpha = torch.tensor(np.log(init_temperature)).to(self.device)
        self.log_alpha.requires_grad = True
        # set target entropy to -|A|
        self.target_entropy = -action_dim


        self.expert_sample = hydra.utils.instantiate(expert_sample)
        self.use_expert = 0
        self.expert = hydra.utils.instantiate(expert_cfg)

        # optimizers
        self.actor_optimizer = torch.optim.Adam(self.actor.parameters(),
                                                lr=actor_lr,
                                                betas=actor_betas)

        self.critic_optimizer = torch.optim.Adam(self.critic.parameters(),
                                                 lr=critic_lr,
                                                 betas=critic_betas)

        self.log_alpha_optimizer = torch.optim.Adam([self.log_alpha],
                                                    lr=alpha_lr,
                                                    betas=alpha_betas)

        self.train()
        self.critic_target.train()

        self.times = dict(sample = [], actor_update = [], critic_update = [])

    def reset(self, eval = False):
        if not eval:
            self.episode += 1
        self.expert_sample.update(self.episode)
        e_s = self.expert_sample.get()
        if eval:
            self.use_expert = False
        elif isinstance(e_s,int):
            self.use_expert = self.episode % e_s
        else:
            self.use_expert  = np.random.rand() < e_s
        super().reset()

    
    def preload(self,env,replay_buffer=None):
        if self.prefill_agent_cfg is not None and self.prefill_replay_buffer > 0:
            prefill_agent = hydra.utils.instantiate(self.prefill_agent_cfg)
            while replay_buffer.idx < self.prefill_replay_buffer and not replay_buffer.full:
                obs = env.reset()
                done = False
                print(f"Buffer fill: {replay_buffer.idx}/{self.prefill_replay_buffer}")
                while not done:
                    act = prefill_agent.get_action(obs)
                    nobs, rew, done, info = env.step(act)
                    replay_buffer.add(obs, act, rew, nobs,done,0, info,{})


    def train(self, training=True):
        self.training = training
        self.actor.train(training)
        self.critic.train(training)

    @property
    def alpha(self):
        return self.log_alpha.exp()

    def act(self, obs, sample=False, env_info = None):
        if self.use_expert:
            action = self.expert.get_action(obs)
        else:
            obs = torch.FloatTensor(obs).to(self.device)
            obs = obs.unsqueeze(0)
            dist = self.actor(obs)
            action = dist.sample() if sample else dist.mean
            action = action.clamp(*self.action_range)
            assert action.ndim == 2 and action.shape[0] == 1
            action = utils.to_np(action[0])
        return action

    def update_critic(self, obs, action, reward, next_obs, not_done, logger,
                      step):
        dist = self.actor(next_obs)
        next_action = dist.rsample()
        log_prob = dist.log_prob(next_action).sum(-1, keepdim=True)
        target_Q1, target_Q2 = self.critic_target(next_obs, next_action)
        target_V = torch.min(target_Q1,
                             target_Q2) - self.alpha.detach() * log_prob
        target_Q = reward + (not_done * self.discount * target_V)
        target_Q = target_Q.detach()

        # get current Q estimates
        current_Q1, current_Q2 = self.critic(obs, action)
        critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
            current_Q2, target_Q)
        logger.log('train_critic/loss', critic_loss, step)

        # Optimize the critic
        self.critic_optimizer.zero_grad()
        critic_loss.backward()
        self.critic_optimizer.step()

        self.critic.log(logger, step)

    def update_actor_and_alpha(self, obs, logger, step):
        dist = self.actor(obs)
        action = dist.rsample()
        log_prob = dist.log_prob(action).sum(-1, keepdim=True)
        actor_Q1, actor_Q2 = self.critic(obs, action)

        actor_Q = torch.min(actor_Q1, actor_Q2)
        actor_loss = (self.alpha.detach() * log_prob - (actor_Q)).mean()

        logger.log('train_actor/loss', actor_loss, step)
        logger.log('train_actor/target_entropy', self.target_entropy, step)
        logger.log('train_actor/entropy', -log_prob.mean(), step)

        # optimize the actor
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()

        self.actor.log(logger, step)

        if self.learnable_temperature:
            self.log_alpha_optimizer.zero_grad()
            alpha_loss = (self.alpha *
                          (-log_prob - self.target_entropy).detach()).mean()
            logger.log('train_alpha/loss', alpha_loss, step)
            logger.log('train_alpha/value', self.alpha, step)
            alpha_loss.backward()
            self.log_alpha_optimizer.step()


    def update(self, replay_buffer, logger, step):
        timer = time.time()
        obs, action, reward, next_obs, not_done, not_done_no_max, env_infos = replay_buffer.sample(
            self.batch_size, return_env_info = True)
        if 'overallReward' in env_infos[0]:
            env_infos = {k: [dic[k] for dic in env_infos] for k in env_infos[0]}
            reward = torch.Tensor(env_infos['overallReward']).unsqueeze(1).to(replay_buffer.device)
            if obs.device == "cuda":
                reward = reward.cuda()
        self.times['sample'].append(time.time()-timer)
        timer = time.time()

        logger.log('train/batch_reward', reward.mean(), step)

        dones = not_done_no_max if self.done_no_max else not_done

        self.update_critic(obs, action, reward, next_obs, dones,
                           logger, step)
        self.times['critic_update'].append(time.time()-timer)
        timer = time.time()

        if step % self.actor_update_frequency == 0:
            self.update_actor_and_alpha(obs, logger, step)
            self.times['actor_update'].append(time.time()-timer)
            timer = time.time()

        if step % self.critic_target_update_frequency == 0:
            utils.soft_update_params(self.critic, self.critic_target,
                                     self.critic_tau)

    def reset_times(self):
        self.times = dict(sample = [], actor_update = [], critic_update = [])

    def print_times(self):
        print('---')
        for elem in self.times:
            print(f'{elem}: {np.mean(self.times[elem])}')

    def log_times(self, logger, step):
        for elem in self.times:
            logger.log(f'timing/{elem}', np.mean(self.times[elem]), step)

    def save_model(self, logger, step):
        logger.save_model('Actor',self.actor.return_state_dict(),step)
        critic_state = self.critic.return_state_dict()
        for q in critic_state:
            logger.save_model(f'Critic/{q}',critic_state[q],step)

    def load_model(self, location):
        self.actor.load_model(torch.load(f'{location}/Actor.pt'))
        critic_state_dict = {}
        critic_state_dict['Q1'] = torch.load(f'{location}/Critic/Q1.pt')
        critic_state_dict['Q2'] = torch.load(f'{location}/Critic/Q2.pt')
        self.critic.load_model(critic_state_dict)



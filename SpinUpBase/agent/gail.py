import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import time

from replay_buffer import ReplayBuffer

from agent import Agent
import utils

import hydra



class GAILAgent(Agent):
    """GAIL algorithm."""
    def __init__(self, obs_dim, action_dim, action_range, device, critic_cfg, discrim_cfg, expert_cfg, 
                 exploit_only_expert, use_live_expert, expert_buffer_size,
                 actor_cfg, discount, init_temperature, alpha_lr, alpha_betas,
                 actor_lr, actor_betas, actor_update_frequency, discrim_lr,discrim_betas,critic_lr,
                 critic_betas, critic_tau, critic_target_update_frequency,
                 batch_size, learnable_temperature, done_no_max, actor_weight=1, discrim_weight=1, augment_per_batch=0,**args):
        super().__init__()

        self.action_range = action_range
        self.device = torch.device(device)
        self.discount = discount
        self.critic_tau = critic_tau
        self.actor_update_frequency = actor_update_frequency
        self.critic_target_update_frequency = critic_target_update_frequency
        self.batch_size = batch_size
        self.learnable_temperature = learnable_temperature
        self.done_no_max = done_no_max
        self.actor_weight = actor_weight
        self.discrim_weight = discrim_weight
        self.exploit_only_expert = exploit_only_expert
        self.use_live_expert = use_live_expert
        self.augment_per_batch = augment_per_batch

        self.expert_buffer = ReplayBuffer((actor_cfg.params.obs_dim,),
                                          (actor_cfg.params.action_dim,),
                                          int(expert_buffer_size), self.device)

        self.critic = hydra.utils.instantiate(critic_cfg).to(self.device)
        self.critic_target = hydra.utils.instantiate(critic_cfg).to(
            self.device)
        self.critic_target.load_state_dict(self.critic.state_dict())

        self.discrim = hydra.utils.instantiate(discrim_cfg).to(self.device)

        self.expert = hydra.utils.instantiate(expert_cfg)

        self.actor = hydra.utils.instantiate(actor_cfg).to(self.device)

        self.log_alpha = torch.tensor(np.log(init_temperature)).to(self.device)
        self.log_alpha.requires_grad = True
        # set target entropy to -|A|
        self.target_entropy = -action_dim

        # optimizers
        self.actor_optimizer = torch.optim.Adam(self.actor.parameters(),
                                                lr=actor_lr,
                                                betas=actor_betas)

        self.discrim_optimizer = torch.optim.Adam(self.discrim.parameters(),
                                                lr=discrim_lr,
                                                betas=discrim_betas)

        self.critic_optimizer = torch.optim.Adam(self.critic.parameters(),
                                                 lr=critic_lr,
                                                 betas=critic_betas)

        self.log_alpha_optimizer = torch.optim.Adam([self.log_alpha],
                                                    lr=alpha_lr,
                                                    betas=alpha_betas)
        self.times = dict(sample = [], actor_update = [], critic_update = [], discriminator_update = [])

        self.train()
        self.critic_target.train()


    def preload(self, env,replay_buffer = None):
        if not self.use_live_expert:
            while not self.expert_buffer.full:
                obs = env.reset()
                done = False
                print(f"Buffer fill: {self.expert_buffer.idx}/{self.expert_buffer.capacity}")
                while not done:
                    act = self.expert.get_action(obs)
                    nobs, rew, done, info = env.step(act)
                    self.expert_buffer.add(obs, act, rew, nobs,done,0, info)
                    obs = nobs

    def train(self, training=True):
        self.training = training
        self.actor.train(training)
        self.critic.train(training)
        self.discrim.train(training)

    @property
    def alpha(self):
        return self.log_alpha.exp()

    def act(self, obs, sample=False, env_info = None):
        obs = torch.FloatTensor(obs).to(self.device)
        obs = obs.unsqueeze(0)
        if self.exploit_only_expert:
            action = self.expert.get_actions(obs)
        else:
            dist = self.actor(obs)
            action = dist.sample() if sample else dist.mean
            action = action.clamp(*self.action_range)
        assert action.ndim == 2 and action.shape[0] == 1
        return utils.to_np(action[0])

    def update_discrim(self,expert_obs, expert_action, policy_obs, policy_action, logger, step):
        if self.discrim.use_action:
            policy_input = torch.cat([policy_obs,policy_action],axis = -1)
            expert_input = torch.cat([expert_obs,expert_action],axis = -1)
        else:
            policy_input = policy_obs
            expert_input = expert_obs

        policy_output = self.discrim.forward(policy_input)
        expert_output = self.discrim.forward(expert_input)

        policy_labels = torch.zeros(policy_output.shape).to(self.device)
        expert_labels = torch.ones(expert_output.shape).to(self.device)

        policy_loss = F.binary_cross_entropy(policy_output,policy_labels)
        expert_loss = F.binary_cross_entropy(expert_output,expert_labels)

        ganLoss = policy_loss+expert_loss

        # This section is from the pytorch implementation within goalGAIL
        inter = self.discrim.alpha * policy_input + (1-self.discrim.alpha)*expert_input
        inter.requires_grad = True
        interOut = torch.mean(self.discrim(inter,False))
        interOut.backward()
        grad = inter.grad
        grad_penalty = torch.mean((torch.norm(grad)-1)**2)
        loss = ganLoss + self.discrim.lambd*grad_penalty


        logger.log('train_discrim/loss', ganLoss, step)
        logger.log('train_discrim/expert_loss', expert_loss, step)
        logger.log('train_discrim/policy_loss', policy_loss, step)

        self.discrim_optimizer.zero_grad()
        loss.backward()
        self.discrim_optimizer.step()
        self.discrim.log(logger, step)

    def update_critic(self, obs, action, reward, next_obs, not_done, logger,
                      step):
        dist = self.actor(next_obs)
        next_action = dist.rsample()
        log_prob = dist.log_prob(next_action).sum(-1, keepdim=True)
        target_Q1, target_Q2 = self.critic_target(next_obs, next_action)
        target_V = torch.min(target_Q1,
                             target_Q2) - self.alpha.detach() * log_prob
        target_Q = reward + (not_done * self.discount * target_V)
        target_Q = target_Q.detach()

        # get current Q estimates
        current_Q1, current_Q2 = self.critic(obs, action)
        critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
            current_Q2, target_Q)
        logger.log('train_critic/loss', critic_loss, step)

        # Optimize the critic
        self.critic_optimizer.zero_grad()
        critic_loss.backward()
        self.critic_optimizer.step()

        self.critic.log(logger, step)

    def update_actor_and_alpha(self, obs, logger, step):
        dist = self.actor(obs)
        action = dist.rsample()
        log_prob = dist.log_prob(action).sum(-1, keepdim=True)
        actor_Q1, actor_Q2 = self.critic(obs, action)

        actor_Q = torch.min(actor_Q1, actor_Q2)
        actor_loss = (self.alpha.detach() * log_prob - (actor_Q)).mean()


        loss = actor_loss

        # logger.log('train_actor/discrim_loss',discrim_loss, step)
        logger.log('train_actor/actor_loss', actor_loss, step)
        logger.log('train_actor/loss', loss, step)
        logger.log('train_actor/target_entropy', self.target_entropy, step)
        logger.log('train_actor/entropy', -log_prob.mean(), step)

        # optimize the actor
        self.actor_optimizer.zero_grad()
        loss.backward()
        self.actor_optimizer.step()

        self.actor.log(logger, step)

        if self.learnable_temperature:
            self.log_alpha_optimizer.zero_grad()
            alpha_loss = (self.alpha *
                          (-log_prob - self.target_entropy).detach()).mean()
            logger.log('train_alpha/loss', alpha_loss, step)
            logger.log('train_alpha/value', self.alpha, step)
            alpha_loss.backward()
            self.log_alpha_optimizer.step()

    def update(self, replay_buffer, logger, step):
        if self.exploit_only_expert:
            return None
        timer = time.time()
        obs, action, reward, next_obs, not_done, not_done_no_max, env_infos = replay_buffer.sample(
            self.batch_size, return_env_info = True)
        if 'overallReward' in env_infos[0] and self.augment_per_batch:
            env_infos = {k: [dic[k] for dic in env_infos] for k in env_infos[0]}
            reward = torch.Tensor(env_infos['overallReward']).unsqueeze(1).to(replay_buffer.device)
            if obs.device == "cuda":
                reward = reward.cuda()
            reward = self.augmentReward(reward,obs,action)
        self.times['sample'].append(time.time()-timer)
        timer = time.time()

        logger.log('train/batch_reward', reward.mean(), step)

        dones = not_done_no_max if self.done_no_max else not_done

        self.update_critic(obs, action, reward, next_obs, dones,
                           logger, step)
        self.times['critic_update'].append(time.time()-timer)
        timer = time.time()

        if self.use_live_expert:
            expert_obs = obs
            expert_actions = self.expert.get_actions(obs)
        else:
            assert self.expert_buffer is not None
            expert_obs, expert_actions, _, _,_,_ = self.expert_buffer.sample(self.batch_size)

        self.update_discrim(expert_obs,expert_actions,obs,action,logger,step)
        self.times['discriminator_update'].append(time.time()-timer)
        timer = time.time()

        if step % self.actor_update_frequency == 0:
            self.update_actor_and_alpha(obs, logger, step)
            self.times['actor_update'].append(time.time()-timer)
            timer = time.time()

        if step % self.critic_target_update_frequency == 0:
            utils.soft_update_params(self.critic, self.critic_target,
                                     self.critic_tau)

    def augmentReward(self, reward, obs, action):
        input = np.concatenate([obs,action])
        discrim1 = self.discrim(torch.Tensor(input).cuda())
        discrim = -math.log(1-discrim1+1e-8)
        outReward = self.actor_weight*reward + self.discrim_weight*discrim
        return outReward

    def reset_times(self):
        self.times = dict(sample = [], actor_update = [], critic_update = [],discriminator_update = [])

    def print_times(self):
        print('---')
        for elem in self.times:
            print(f'{elem}: {np.mean(self.times[elem])}')

    def log_times(self, logger, step):
        for elem in self.times:
            logger.log(f'timing/{elem}', np.mean(self.times[elem]), step)

    def save_model(self, logger, step):
        logger.save_model('Actor',self.actor.return_state_dict(),step)
        logger.save_model('Discriminator',self.discrim.return_state_dict(),step)
        critic_state = self.critic.return_state_dict()
        
        for q in critic_state:
            logger.save_model(f'Critic/{q}',critic_state[q],step)

    def load_model(self, location):
        self.actor.load_model(torch.load(f'{location}/Actor.pt'))
        self.discrim.load_model(torch.load(f'{location}/Discriminator.pt'))
        critic_state_dict = {}
        critic_state_dict['Q1'] = torch.load(f'{location}/Critic/Q1.pt')
        critic_state_dict['Q2'] = torch.load(f'{location}/Critic/Q2.pt')
        self.critic.load_model(critic_state_dict)



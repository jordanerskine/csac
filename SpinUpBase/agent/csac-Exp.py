import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import time
import cv2
from copy import deepcopy as copy


from replay_buffer import ReplayBuffer

from agent import Agent
from sac import SACAgent
import utils

import hydra


class singleSAC(SACAgent):
    def __init__(self, *args, subtask_id, coop_ratio, rectify_norm, limitedDomain):
        super().__init__(*args)
        self.subtask_id = subtask_id
        self.coop_ratio = coop_ratio
        self.rectify_norm = rectify_norm
        self.limitedDomain = limitedDomain

        self.nextAgent = None
        self.previousAgent = None

        self.randomObs = None
        self.randomAct = None

        self.times = dict(sample = [], actor_update = [], critic_update = [])

    def set_next_agent(self,agent):
        self.nextAgent = agent

    def set_previous_agent(self,agent):
        self.previousAgent = agent

    def normalise(self, batch):
        mn = torch.min(batch).detach()
        mx = torch.max(batch).detach()
        return (batch-mn)/(mx-mn)

    def batch_range(self, batch):
        return torch.max(batch).detach()-torch.min(batch).detach()

    def update_actor_and_alpha(self, obs, logger, step):
        # The n here represents the next domain. The actor needs to be 
        # trained with respect to the next domain and critic

        # Get the loss for the no future agent case
        if self.nextAgent is None:
            dist = self.actor(obs)
            action = dist.rsample()
            log_prob = dist.log_prob(action).sum(-1, keepdim=True)
            actor_Q1, actor_Q2 = self.critic(obs, action)

            actor_Q = torch.min(actor_Q1, actor_Q2)
            actor_loss = (self.alpha.detach() * log_prob - actor_Q).mean()

        # Get the loss for the case with a future agent
        else:
            dist = self.actor(obs)
            action = dist.rsample()
            log_prob = dist.log_prob(action).sum(-1, keepdim=True)
            n_actor_Q1, n_actor_Q2 = self.nextAgent.critic(obs, action)
            actor_Q1, actor_Q2 = self.critic(obs, action)

            n_actor_Q = torch.min(n_actor_Q1, n_actor_Q2)
            actor_Q = torch.min(actor_Q1, actor_Q2)

            norm_n_Q = self.normalise(n_actor_Q)
            norm_Q = self.normalise(actor_Q)
            # norm_n_Q = n_actor_Q
            # norm_Q = actor_Q
            if self.rectify_norm:
                scaling_factor = self.coop_ratio*self.batch_range(actor_Q)+(1-self.coop_ratio)*self.batch_range(n_actor_Q)
            else:
                scaling_factor = 1

            actor_loss = (self.alpha.detach()*log_prob - scaling_factor*(self.coop_ratio*norm_Q + (1-self.coop_ratio)*norm_n_Q)).mean()

        logger.log(f'train/actor_{self.subtask_id}/loss', actor_loss, step)
        logger.log(f'train/actor_{self.subtask_id}/target_entropy', self.target_entropy, step)
        logger.log(f'train/actor_{self.subtask_id}/entropy', -log_prob.mean(), step)

        # optimize the actor
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()

        self.actor.log(logger, step, suffix=f'_{self.subtask_id}')

        if self.learnable_temperature:
            self.log_alpha_optimizer.zero_grad()
            alpha_loss = (self.alpha *
                          (-log_prob - self.target_entropy).detach()).mean()
            logger.log(f'train/alpha_{self.subtask_id}/loss', alpha_loss, step)
            logger.log(f'train/alpha_{self.subtask_id}/value', self.alpha, step)
            alpha_loss.backward()
            self.log_alpha_optimizer.step()

    def update_critic(self, obs, action, reward, next_obs, not_done,  
                      logger, step,
                      p_obs = None, p_action = None, p_reward = None, p_next_obs = None, p_not_done = None):
        # The p refers to the previous domain. The critic needs to be trained
        # with respect to the current and the previous domain
        
        # Get loss for the current domain
        if obs is None:
            noCurrent = True
        else:
            noCurrent = False
            dist = self.actor(next_obs)
            next_action = dist.rsample()
            log_prob = dist.log_prob(next_action).sum(-1, keepdim=True)
            target_Q1, target_Q2 = self.critic_target(next_obs, next_action)
            target_V = torch.min(target_Q1,
                                target_Q2) - self.alpha.detach() * log_prob
            target_Q = reward + (not_done * self.discount * target_V)
            target_Q = target_Q.detach()

            # get current Q estimates
            current_Q1, current_Q2 = self.critic(obs, action)
            current_critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
                current_Q2, target_Q)

            # if torch.argmax(reward) != torch.argmax(target_Q) and \
            #         (torch.max(reward)-reward[torch.argmax(target_Q)]) > 5:
            #     rewPoint = torch.argmax(reward)
            #     targPoint = torch.argmax(target_Q)
            #     print(f"Reward: {reward[rewPoint]}\nTarget Q: {target_Q[rewPoint]}")
            #     print(f"Reward: {reward[targPoint]}\nTarget Q: {target_Q[targPoint]}")
            #     print("Disjoint")


        
        # Get loss for the previous domain
        if self.previousAgent is not None:
            dist = self.previousAgent.actor(p_next_obs)
            next_action = dist.rsample()
            log_prob = dist.log_prob(next_action).sum(-1, keepdim=True)
            target_Q1, target_Q2 = self.critic_target(p_next_obs, next_action)
            target_V = torch.min(target_Q1,
                                target_Q2) - self.alpha.detach() * log_prob
            target_Q = p_reward + (p_not_done * self.discount * target_V)
            target_Q = target_Q.detach()

            # get current Q estimates
            current_Q1, current_Q2 = self.critic(p_obs, p_action)
            previous_critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(
                current_Q2, target_Q)

            if noCurrent:
                critic_loss = previous_critic_loss
            else:
                critic_loss = current_critic_loss + previous_critic_loss
        else:
            if noCurrent:
                critic_loss = None
            else:
                critic_loss = current_critic_loss

        # if not noCurrent:
        #     logger.log(f'train_critic_{self.subtask_id}/loss_current_domain', current_critic_loss, step)
        # if self.previousAgent is not None:
        #     logger.log(f'train_critic_{self.subtask_id}/loss_previous_domain', previous_critic_loss, step)
        if critic_loss is not None:
            logger.log(f'train/critic_{self.subtask_id}/loss', critic_loss, step)


        # Optimize the critic
        if critic_loss is not None:
            self.critic_optimizer.zero_grad()
            critic_loss.backward()
            self.critic_optimizer.step()

            self.critic.log(logger, step, suffix=f"_{self.subtask_id}")
    
    def update(self, replay_buffer, logger, step):
        # Get data from this agent's domain
        timer = time.time()
        if self.limitedDomain:
            try:
                obs, action, reward, next_obs, not_done, not_done_no_max = replay_buffer.subtask_sample(
                    self.batch_size, subtask_id = self.subtask_id)
            except:
                obs, action, reward, next_obs, not_done, not_done_no_max = None, None, None, None, None, None

            # Get data from the previous agent's domain
            p_obs, p_action, p_reward, p_next_obs, p_not_done, p_not_done_no_max, p_env_infos = None, None, None, None, None, None, None
            try:
                if self.previousAgent is not None:
                    p_obs, p_action, p_reward, p_next_obs, p_not_done, p_not_done_no_max, p_env_infos = replay_buffer.subtask_sample(
                        self.batch_size, subtask_id = self.subtask_id-1, return_env_info=True)
                    p_reward = torch.Tensor(np.array([info[f'reward{self.subtask_id}'] for info in p_env_infos])).unsqueeze(1).to(replay_buffer.device)
            except:
                pass
        else:
            obs, action, _, next_obs, not_done, not_done_no_max, env_infos = replay_buffer.sample(self.batch_size, return_env_info = True)
            reward = torch.Tensor([timestep[f'reward{self.subtask_id}'] for timestep in env_infos]).to(replay_buffer.device)
            if self.previousAgent is not None:
                p_reward = torch.as_tensor([timestep[f'reward{self.subtask_id-1}'] for timestep in env_infos]).to(replay_buffer.device)
                p_obs, p_action, p_next_obs, p_not_done, p_not_done_no_max = obs, action, next_obs, not_done, not_done_no_max

        if obs is not None:
            self.randomObs = obs[0]
            self.randomAct = action[0]
        self.times['sample'].append(time.time()-timer)
        timer = time.time()
        if reward is not None:
            logger.log(f'train/agent_{self.subtask_id}/batch_reward', reward.mean(), step)

        if obs is not None:
            if self.previousAgent is not None and p_obs is not None:
                self.update_critic(obs, action, reward, next_obs, not_done_no_max,
                            logger, step,
                            p_obs, p_action, p_reward, p_next_obs, p_not_done_no_max)    
            else:
                self.update_critic(obs, action, reward, next_obs, not_done_no_max,
                                logger, step)
            self.times['critic_update'].append(time.time()-timer)
            timer = time.time()

        if step % self.actor_update_frequency == 0 and obs is not None:
            self.update_actor_and_alpha(obs, logger, step)
            self.times['actor_update'].append(time.time()-timer)

        if step % self.critic_target_update_frequency == 0:
            utils.soft_update_params(self.critic, self.critic_target,
                                     self.critic_tau)



class CSAC_E(SACAgent):
    def __init__(self, obs_dim, action_dim, action_range, device, critic_cfg,
                 actor_cfg, expert_cfg, discount, init_temperature, alpha_lr, alpha_betas,
                 actor_lr, actor_betas, actor_update_frequency, critic_lr,
                 critic_betas, critic_tau, critic_target_update_frequency,
                 batch_size, learnable_temperature, expert_sample,
                 nSubtasks, cooperative_ratio, rectify_norm,
                 limitedDomain = True, done_no_max = True):
        

        args = [obs_dim, action_dim, action_range, device, critic_cfg,
                 actor_cfg, discount, init_temperature, alpha_lr, alpha_betas,
                 actor_lr, actor_betas, actor_update_frequency, critic_lr,
                 critic_betas, critic_tau, critic_target_update_frequency,
                 batch_size, learnable_temperature, done_no_max]
        # print(args)
        
        if not isinstance(cooperative_ratio,list):
            cooperative_ratio = [cooperative_ratio for _ in range(nSubtasks)]

        self.batch_size = batch_size
        self.device = device
        self.episode = 0

        self.expert_sample = hydra.utils.instantiate(expert_sample)
        self.use_expert = 0
        self.expert = hydra.utils.instantiate(expert_cfg)


        self.agents = [singleSAC(*args, subtask_id = i, coop_ratio=cooperative_ratio[i], rectify_norm=rectify_norm, limitedDomain=limitedDomain) 
                            for i in range(nSubtasks)]
        for n in range(1, len(self.agents)):
            self.agents[n-1].set_next_agent(self.agents[n])
            self.agents[n].set_previous_agent(self.agents[n-1])
        super().__init__(*args)

    def reset(self,eval = False):
        self.episode += 1
        self.expert_sample.update(self.episode)
        e_s = self.expert_sample.get()
        if eval:
            self.use_expert = False
        elif isinstance(e_s,int):
            self.use_expert = self.episode % e_s
        else:
            self.use_expert  = np.random.rand() < e_s
        super().reset()


        
    def train(self, training = True):
        self.training = training
        for agent in self.agents:
            agent.train(training)

    def act(self, obs, sample = False, env_info = None):
        # obs = torch.FloatTensor(obs).to(self.device)
        # obs = obs.unsqueeze(0)
        if self.use_expert:
            action = self.expert.get_action(obs)
        else:
            subtask = env_info['subtask'] if env_info is not None else 0
            action = self.agents[subtask].act(obs, sample)
        # action = np.array([0,0,-1,0])
        return action

    
    def update(self, replay_buffer, logger, step):
        for agent in self.agents:
            agent.update(replay_buffer, logger, step)


    def reset_times(self):
        for agent in self.agents:
            agent.times = dict(sample = [], actor_update = [], critic_update = [])

    def print_times(self):
        print('---')
        for n, agent in enumerate(self.agents):
            print(f'Agent {n}')
            for elem in agent.times:
                print(f'   {elem}: {np.mean(agent.times[elem])}')

    def log_times(self, logger, step):
        for n, agent in enumerate(self.agents):
            for elem in agent.times:
                logger.log(f'timing/agent{n}/{elem}', np.mean(agent.times[elem]), step)
                

    def save_model(self, logger, step):
        for n,agent in enumerate(self.agents):
            logger.save_model(f'Agent{n}/Actor',agent.actor.return_state_dict(),step)
            critic_state = agent.critic.return_state_dict()
            for q in critic_state:
                logger.save_model(f'Agent{n}/Critic/{q}',critic_state[q],step)
        

    def load_model(self, location):
        for n, agent in enumerate(self.agents):
            agent.actor.load_model(torch.load(f'{location}/Agent{n}/Actor.pt'))
            critic_state_dict = {}
            critic_state_dict['Q1'] = torch.load(f'{location}/Agent{n}/Critic/Q1.pt')
            critic_state_dict['Q2'] = torch.load(f'{location}/Agent{n}/Critic/Q2.pt')
            agent.critic.load_model(critic_state_dict)
        
        



# if __name__ == "__main__":

    
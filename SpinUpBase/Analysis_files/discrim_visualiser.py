from test_model import Workspace, getArgs
import numpy as np
import torch, utils, cv2, sys

class discrim(Workspace):
    def __init__(self, path, load='latest', progression = False, render = False,kwargUpdates = None):
        super().__init__(path, load, progression, render,kwargUpdates)

    def run_eps(self, sample, episodes, print_progress, trajectoryRender):

        for ep in range(int(episodes)):
            obs = self.env.reset()
            self.agent.reset(eval = True)
            done = False
            episode_reward = 0
            step = 0
            env_info = None
            success = []
            discrim = []
            while not done:
                with utils.eval_mode(self.agent):
                    action = self.agent.act(obs, sample=sample, env_info=env_info)
                    oL = list(obs)
                    actObs = torch.tensor([[[oL+[x]+[y]+[z] for x in np.arange(-1,1,0.02)] for y in np.arange(-1,1,0.02)] for z in np.arange(-1,1,0.1)])
                    d = self.agent.discrim(actObs.cuda().float())
                dMin = torch.min(d,0)[0].detach().cpu().numpy()
                obs, reward, done, env_info = self.env.step(action)
                # if self.render:
                #     time.sleep(0.02)
                if 'overallReward' in env_info:
                    episode_reward += env_info['overallReward']
                    success.append(env_info['success'])
                    subtask = env_info['subtask']
                else:
                    episode_reward += reward
                    success.append(0)
                    subtask = 0
                if not trajectoryRender and self.render:
                    self.env.render()
                    normD = (dMin-np.min(dMin))/(np.max(dMin)-np.min(dMin))
                    normD = cv2.resize(normD,(400,400))
                    D3 = cv2.merge((normD,normD,normD))
                    statImage = cv2.line(D3,(200,0),(200,400),(200,200,200),1)
                    statImage = cv2.line(statImage,(0,200),(400,200),(200,200,200),1)
                    statImage = cv2.putText(statImage,f"Min:{np.min(dMin):5e}, Max:{np.max(dMin):5e}",(10,10),cv2.FONT_HERSHEY_PLAIN,1,(100,100,200),2)
                    statImage = cv2.circle(statImage,tuple((action[:2]*200+200).astype(int)),5,(200,0,0),-1)
                    expertAction = self.agent.expert.get_action(obs)
                    statImage = cv2.circle(statImage,tuple((expertAction[:2]*200+200).astype(int)),5,(0,200,0),-1)
                    cv2.imshow(f"Action Discrimination - Noise:{self.agent.expert.noise}", statImage)
                    cv2.waitKey(10)
                step += 1
                if print_progress:
                    print(f"\nStep: {step}")
                    print(f"Action: {action}")
                    out = self.evaluate_critic(obs,action)
                    # print(f"Critic: {out.cpu().detach().numpy()}")
                    print(f"Subtask: {subtask}")
            if len(discrim) == 0:
                print(f'Episode {ep} \n\
                        Overall Reward: {episode_reward}\n\
                        Final subtask: {subtask}\n\
                        Success Rate: {np.mean(success)}')
            else:
                print(f'Episode {ep} \n\
                        Overall Reward: {episode_reward}\n\
                        Final subtask: {subtask}\n\
                        Success Rate: {np.mean(success)}\n\
                        Discrimination Rate: {np.mean(discrim)}')
        if trajectoryRender:
            img = self.env.trajectoryRender(False)
            cv2.namedWindow("Trajectory Render", cv2.WINDOW_NORMAL)
            cv2.imshow("Trajectory Render", img)
            cv2.waitKey(0)


def main():
    workArgs = getArgs(['--path','--load', '--progression', '--render'])
    workArgs['kwargUpdates'] = {a.split("=")[0]:a.split("=")[1] for a in sys.argv if "=" in a}
    workspace = discrim(**workArgs)
    runArgs = getArgs(['--sample','--episodes','--print_progress', '--trajectoryRender'])
    workspace.run(**runArgs)


if __name__ == '__main__':
    main()
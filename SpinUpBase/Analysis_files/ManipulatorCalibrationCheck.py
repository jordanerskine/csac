import rospy
from Envs.PegInHole_Sim2Real import PegInHole
from jordon_msgs.msg import Transition
from collections import deque
import time, numpy as np

class Trans:
    def __init__(self,obs, act, nobs, simNobs = None):
        self.obs = obs
        self.act = act
        self.nobs = nobs
        self.simNobs = simNobs

class Calibrate:
    def __init__(self):
        rospy.init_node("Manipulator_Calibration")
        self.env = PegInHole(randomPeg = False)
        self.transitionListener = rospy.Subscriber('/PegInHoleEnv/transition',Transition, self.logTransition)
        self.transitions = []
        self.errors = []

    
    def logTransition(self,msg):
        self.transitions.append(Trans(msg.obs,msg.act,msg.nobs))
        print(f"Appending transition {len(self.transitions)}")

    def checkTransition(self, transition):
        obs = self.env._get_state()
        while np.all(abs(obs[0]-transition.obs[0]) > 0.01):
            obs,_,_,_ = self.env.step([obs[0]-transition.obs[0],0])
        
        nobs = self.env.step(transition)
        transition.simNobs = nobs

    def checkAllTransitions(self):
        done = False
        index = 0
        waitLim = 20
        waited = 0
        while not done:
            if len(self.transitions) > index:
                print(f"Checking transition {index}")
                self.checkTransition(self.transitions[index])
                print(self.transitions[index].nobs,self.transitions[index].simNobs)
                index += 1
                waited = 0
            else:
                time.sleep(1)
                waited += 1
                print(f"Waited {waited} seconds")
                if waited >= waitLim:
                    done = True



if __name__ == "__main__":
    cal = Calibrate()
    cal.checkAllTransitions()
# Cooperative methods for multi-stage domains

This is based on the PyTorch implementation of Soft Actor-Critic (SAC) [[ArXiv]](https://arxiv.org/abs/1812.05905). This builds on the pytorch implementation designed by Denis Yarats (https://github.com/denisyarats/pytorch_sac)

This repository contains several different methods of utilising cooperative policies.

To run a trial it is a matter of calling the train.py file. Parameters can be added through command line
```
python train.py env.class=CheckpointMazeEnv.CheckpointMaze agent=csac cRatio=0.2 env.params._max_episode_steps=100
```

The default configurations for the algorithms are stored in config yaml files.

While running the progress and saved models will be stored in exp folder in a folder made for the day.

#!/usr/bin/env python3
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import copy
import math
import os
import sys
import time
import pickle as pkl
import yaml
import json
from omegaconf.dictconfig import DictConfig
from omegaconf.listconfig import ListConfig



from video import VideoRecorder
from logger import Logger
from replay_buffer import ReplayBuffer
import utils

import dmc2gym
import hydra
import git

from train import Workspace

def arg(tag, default):
    if tag in sys.argv:
        val = sys.argv[sys.argv.index(tag)+1]
    else:
        val = default
    return val

def main(path):
    # Importing workspace
    path = f"{os.getcwd()}/{path}"
    with open(f"{path}/config.yaml","r") as stream:
        cfg = yaml.safe_load(stream)
        cfg = DictConfig(cfg)
    os.chdir(path)
    w = Workspace(cfg,True)
    iter = np.max([int(f) for f in os.listdir(f"{path}/model")])
    print(f"Resuming from iter: {iter}")
    w.agent.load_model(f"{path}/model/{iter}")
    w.step = iter

    # Resuming Workspace
    w.run(iter)

if __name__ == "__main__":
    """Resumes the training of an experiment.
        args: 
            --path: the path of the save location of the experiment"""
    
    path = arg("--path",'./')
    main(path)
    




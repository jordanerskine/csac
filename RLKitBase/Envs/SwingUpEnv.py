import numpy as np, sys, time, gym


class SwingUpEnv:
    def __init__(self, angle_threshold = 0.8, reward_type = "dense", _max_episode_steps=None):
        self.env = gym.make('Pendulum-v0')
        self.action_space = self.env.action_space
        self.observation_space = self.env.observation_space
        self.nSubtasks = 2
        self.env.reset()
        self.angle_threshold = angle_threshold
        self.reward_type = reward_type
        self._max_episode_steps = _max_episode_steps

    def reset(self):
        self.ep_step = 0
        return self.env.reset()

    def step(self,action):
        obs, rew, done, _ = self.env.step(action)
        st = self._subtask(obs)
        rew = self._genReward(obs)
        info = {}
        info['subtask'] = st
        info['reward0'] = rew
        info['reward1'] = rew
        info['overallReward'] = rew
        self.ep_step += 1
        done = done or self.ep_step >= self._max_episode_steps
        return obs, rew, done, info

    def render(self):
        self.env.render()

    def _genReward(self, obs):
        if obs[0] > self.angle_threshold:
            return 1
        else:
            return (obs[0]-1) if self.reward_type == "dense" else 0

    def _subtask(self, obs):
        if obs[0] > self.angle_threshold:
            return 1
        else:
            return 0


if __name__ == "__main__":
    env = SwingUpEnv()
    for _ in range(200):
        obs,_,_,_ = env.step(np.random.rand(1))
        env.render()

from os.path import dirname, join, abspath
from pyrep import PyRep
from pyrep.robots.arms.panda import Panda
from pyrep.robots.end_effectors.panda_gripper import PandaGripper
from pyrep.objects.shape import Shape
import numpy as np
from gym import spaces

SCENE_FILE = join(dirname(abspath(__file__))[:-4],
                  'Models/PegShoveTask.ttt')
POS_MIN, POS_MAX = [0.8, -0.2], [1.0, 0.2]
EPISODES = 20
EPISODE_LENGTH = 200


class PegEnv(object):

    def __init__(self,**kwargs):
        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=False)
        self.pr.start()
        self.agent = Panda()
        self.gripper = PandaGripper()
        self.agent.set_control_loop_enabled(False)
        self.agent.set_motor_locked_at_zero_velocity(True)
        self.peg = Shape('Peg')
        self.agent_ee_tip = self.agent.get_tip()
        self.initial_joint_positions = self.agent.get_joint_positions()
        self.action_space = spaces.Box(low=np.array([-1, -1, -1, -1, -1, -1, -1, 0]), high=np.array([1, 1, 1, 1, 1, 1, 1, 1]), dtype=np.float64)
        self.observation_space = spaces.Box(low=-1, high=1, shape=[20])
        self.nSubtasks = 2

    def _get_state(self):
        # Return state containing arm joint angles/velocities & target position
        return np.concatenate([self.agent.get_joint_positions(),
                               self.agent.get_joint_velocities(),
                               self.peg.get_position(),
                               self.peg.get_orientation()])

    def _grasped(self):
        if self.peg in self.gripper._grasped_objects:
            return True
        else:
            return False

    def reset(self):
        # Get a random position within a cuboid and set the target position
        pos = list(np.random.uniform(POS_MIN, POS_MAX))
        pos.append(0.765)
        self.peg.set_position(pos)
        self.peg.set_orientation([0,0,np.random.uniform(-0.2,0.2)])
        self.agent.set_joint_positions(self.initial_joint_positions)
        self.gripper.release()
        return self._get_state()

    def step(self, action):
        self.agent.set_joint_target_velocities(action[:-1])  # Execute action on arm
        self.gripper.actuate(action[-1],1)
        if action[-1] < 0.5:
            self.gripper.grasp(self.peg)
        else:
            self.gripper.release()
        self.pr.step()  # Step the physics simulation
        ax, ay, az = self.agent_ee_tip.get_position()
        px, py, pz = self.peg.get_position()
        # Reward is negative distance to target
        rewards = []
        rewards.append(-np.sqrt((ax - px) ** 2 + (ay - py) ** 2 + (az - pz) ** 2))
        rewards.append(py)

        subtask = 1 if self._grasped() else 0

        info = {"reward0":rewards[0],
                "reward1":rewards[1],
                "overallReward":np.sum(rewards),
                "subtask":subtask,
                "success":1 if rewards[1] > 0.8 else 0}
        return self._get_state(), rewards[subtask], False, info

    def shutdown(self):
        self.pr.stop()
        self.pr.shutdown()


class Agent(object):

    def act(self, state):
        del state
        action = list(np.random.uniform(-1.0, 1.0, size=(7,)))
        action.append(np.random.uniform(0.0, 1.0))
        return action

    def learn(self, replay_buffer):
        del replay_buffer
        pass

if __name__ == '__main__':
    env = PegEnv()
    agent = Agent()
    replay_buffer = []

    for e in range(EPISODES):

        print('Starting episode %d' % e)
        state = env.reset()
        for i in range(EPISODE_LENGTH):
            action = agent.act(state)
            reward, next_state,_,info = env.step(action)
            replay_buffer.append((state, action, reward, next_state))
            if info['subtask'] == 1: print('grasped')
            state = next_state
            agent.learn(replay_buffer)

    print('Done!')
    env.shutdown()
"""
A Franka Panda moves using delta end effector pose control.
This script contains examples of:
    - IK calculations.
    - Joint movement by setting joint target positions.
"""
from os.path import dirname, join, abspath
from pyrep import PyRep
from pyrep.robots.arms.panda import Panda
from pyrep.objects.shape import Shape
import numpy as np
from gym import spaces




class CartPanda:
    def __init__(self,**kwargs):
        SCENE_FILE = join(dirname(abspath(__file__)), '../Models/scene_panda_reach_target.ttt')
        print(kwargs)
        headless = True if 'headless' not in kwargs or kwargs['headless'] == 1 else False
        self.dim = 2 if 'dim' not in kwargs else kwargs['dim']
        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=headless)
        self.pr.start()
        self.agent = Panda()
        self.targetObj = Shape('Target')
        self.action_space = spaces.Box(low=np.array([-1 for _ in range(self.dim)]),
                                         high=np.array([1 for _ in range(self.dim)]), dtype=np.float64) # 3 action length
        self.observation_space = spaces.Box(low=-1, high=1, shape=[14+self.dim]) #9 
        self.nSubtasks = 1

        POS_MIN, POS_MAX = [0.8, -0.2, 0.8], [1.2, 0.2, 1.2]
        self.POS_MAX = POS_MAX[:self.dim]
        self.POS_MIN = POS_MIN[:self.dim]

        self.defaultPos = np.array([1.0, 0, 0.8])

        self._max_episode_steps = 1000
        
        self.starting_joint_positions = self.agent.get_joint_positions()
        self.reset()
        

    def reset(self):
        self.agent.set_joint_positions(self.starting_joint_positions)
        self.pos, self.quat = self.agent.get_tip().get_position(), self.agent.get_tip().get_quaternion()
        self.target = list(np.random.uniform(self.POS_MIN, self.POS_MAX))
        # self.targetObj.set_position(np.array(self.target))
        self.targetObj.set_position(np.concatenate((np.array(self.target),self.defaultPos[self.dim:])))
        self.ep_step = 0
        return self._get_state()

    
    def _get_state(self):
        return np.concatenate([self.agent.get_joint_positions(),
                               self.agent.get_joint_velocities(),
                               self.target])   # 19 dim
        # return np.concatenate([self.agent.get_tip().get_position(),
        #                        self.agent.get_tip().get_velocity()[0], 
        #                        self.target]) # 8 dim

    def _get_reward(self):
        agentPose = self.agent.get_tip().get_position()

        dist = np.linalg.norm(agentPose[:self.dim]-self.target)
        return np.exp(-dist)

    def step(self,action):
        
        self.pos += 0.03*np.concatenate((np.array(action),np.array([0 for _ in range(3-self.dim)])))
        self.pos[self.dim:] = self.defaultPos[self.dim:]
        # print(pos,quat)
        try:
            new_joint_angles = self.agent.solve_ik(self.pos, quaternion=self.quat)
        except:
            rew = self._get_reward()
            info = {"reward0":rew,
                    "overallReward":rew,
                    "subtask":0,
                    "success":1 if rew > 0.9 else 0}
            # print("Failed to move")
            return self._get_state(), rew, True, info
        self.agent.set_joint_target_positions(new_joint_angles)
        self.pr.step()
        self.ep_step += 1
        self.pos, self.quat = self.agent.get_tip().get_position(), self.agent.get_tip().get_quaternion()
        rew = self._get_reward()
        done = True if self.ep_step > self._max_episode_steps else False
        # print(rew)
        info = {"reward0":rew,
                "overallReward":rew,
                "subtask":0,
                "success":1 if rew > 0.9 else 0}
        return self._get_state(), rew, done, info


if __name__ == "__main__":

    env = CartPanda()

    # [env.step(np.random.uniform(-1,1,size=[3,])) for _ in range(20)]
    nAttempts = 200
    step = 1

    for axis in range(2):
        for direction in [-step,step]:
            env.reset()
            move = np.array([0.0,0.0,0.0])
            move[axis] = direction
            # print(move)
            results = [env.step(move) for _ in range(nAttempts)]
            success = 0
            for res in results:
                success += 1 if not res[2] else 0
            # print("Success: {}/{}".format(success,nAttempts))

    env.pr.stop()
    env.pr.shutdown()

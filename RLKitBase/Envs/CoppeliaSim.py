from  pyrep import PyRep 
# import pyrep.robots.arms
import pyrep, cv2, time, sys
from pyrep.robots.arms.arm import Arm
from pyrep.robots.arms.panda import Panda


class UArm(Arm):

    def __init__(self, count: int = 0):
        super().__init__(count, 'uarm', 3)


p = PyRep()
name = 'uArm' if "--name" not in sys.argv else str(sys.argv[sys.argv.index("--name")+1])
blocking = 0 if "--blocking" not in sys.argv else int(sys.argv[sys.argv.index("--blocking")+1])
path = "/media/jordan/HardDrive2TB/Documents/Repos/RL/CooperativePolicies/Maze/"
p.launch(path+'Models/{}.ttt'.format(name),blocking=blocking)

# if name == 'uArm':
#     arm = UArm()
# else:
#     arm = Panda()

p.set_simulation_timestep(0.005)
p.start()
start = time.time()
lastTime = 0
duration = 60 if "--duration" not in sys.argv else int(sys.argv[sys.argv.index("--duration")+1])
while (time.time()-start)<duration:
    # arm.set_joint_target_velocities([1.,1.,1.])
    p.step()
    if (time.time()-start)>(lastTime+1):
        lastTime += 1
        print("{}/{}".format(lastTime,duration))
p.shutdown()
    
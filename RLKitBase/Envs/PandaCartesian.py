from os.path import dirname, join, abspath
from pyrep import PyRep
from pyrep.robots.arms.panda import Panda
from pyrep.robots.end_effectors.panda_gripper import PandaGripper
from pyrep.objects.shape import Shape
from pyrep.objects.dummy import Dummy
import numpy as np
from gym import spaces
from copy import deepcopy as copy

SCENE_FILE = join(dirname(abspath(__file__)),
                  '../Models/scene_panda_reach_target.ttt')
POS_MIN, POS_MAX = [0.8, -0.2, 1.0], [1.0, 0.2, 1.4]
EPISODES = 5
EPISODE_LENGTH = 200


class ReacherEnv(object):

    def __init__(self,**kwargs):
        self.pr = PyRep()
        self.pr.launch(SCENE_FILE, headless=False)
        self.pr.start()
        self.agent = Panda()
        self.gripper = PandaGripper()
        # self.agent.set_control_loop_enabled(False)
        # self.agent.set_motor_locked_at_zero_velocity(True)
        self.target = Dummy('Panda_target')
        self.agent_ee_tip = self.agent.get_tip()
        self.initial_joint_positions = copy(self.agent.get_joint_positions())
        self.action_space = spaces.Box(low=np.array([-1, -1, -1, 0]), high=np.array([1, 1, 1, 1]), dtype=np.float64)
        self.observation_space = spaces.Box(low=-1, high=1, shape=[17])
        self.nSubtasks = 1

    def _get_state(self):
        # Return state containing arm joint angles/velocities & target position
        return np.concatenate([self.agent.get_joint_positions(),
                               self.agent.get_joint_velocities()])

    def reset(self):
        # Get a random position within a cuboid and set the target position
        pos = list(np.random.uniform(POS_MIN, POS_MAX))
        # self.target.set_position(pos)
        self.agent.set_joint_positions(self.initial_joint_positions)
        self.get_pq()
        return self._get_state()

    def get_pq(self):
        self.pos = self.agent_ee_tip.get_position()
        self.quat = self.agent_ee_tip.get_quaternion()
        # self.quat = 0
        

    def step(self, action):
        self.pos += 0.01*np.array(action[:-1])
        # print(pos,self.pos,self.quat)
        new_joint_angles = self.agent.solve_ik(self.pos, quaternion=self.quat)
        self.agent.set_joint_target_positions(new_joint_angles)
        # self.gripper.actuate(action[-1],1)
        self.pr.step()  # Step the physics simulation
        # self.get_pq()
        ax, ay, az = self.agent_ee_tip.get_position()
        # tx, ty, tz = self.target.get_position()
        # Reward is negative distance to target
        # reward = -np.sqrt((ax - tx) ** 2 + (ay - ty) ** 2 + (az - tz) ** 2)
        reward = 0

        info = {"reward0":reward,
                "overallReward":reward,
                "subtask":0,
                "success":1 if reward > -0.5 else 0}
        return self._get_state(), reward, False, info

    def shutdown(self):
        self.pr.stop()
        self.pr.shutdown()


class Agent(object):
    def __init__(self):
        self.lastAct = np.array([0.0,0.0,0.0])

    def act(self, state):
        del state
        # action = list(np.random.uniform(-0.5, 0.5, size=(3,)))
        action = [0.0,0.0,1.0]
        # self.lastAct = 0.5*self.lastAct+action
        # action = list(copy(self.lastAct))
        action.append([0.0])
        return action

    def learn(self, replay_buffer):
        del replay_buffer
        pass

if __name__ == '__main__':
    env = ReacherEnv(render = True)
    agent = Agent()
    replay_buffer = []

    for e in range(EPISODES):

        print('Starting episode %d' % e)
        state = env.reset()
        for i in range(EPISODE_LENGTH):
            action = agent.act(state)
            
            reward, next_state,_,_ = env.step(action)
            replay_buffer.append((state, action, reward, next_state))
            state = next_state
            agent.learn(replay_buffer)

    print('Done!')
    env.shutdown()
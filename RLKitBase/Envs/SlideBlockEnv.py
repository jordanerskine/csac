
import numpy as np, cv2, sys, os, math, random, time, torch
from gym import spaces
from Box2D import *
import os
from copy import deepcopy as copy


def rnd(mn,mx): return np.random.random()*(mx-mn)+mn

class Agent:
    def __init__(self,workArea):
        self.workArea = workArea
        self.pos = workArea.generatePointInArea()
        self.grasped = False

    def step(self,dx,dy):
        self.pos[0] += dx
        self.pos[1] += dy
        if self.pos[0] > self.workArea.xmx:
            self.pos[0] = self.workArea.xmx
        if self.pos[0] < self.workArea.xmn:
            self.pos[0] = self.workArea.xmn
        if self.pos[1] > self.workArea.ymx:
            self.pos[1] = self.workArea.ymx
        if self.pos[1] < self.workArea.ymn:
            self.pos[1] = self.workArea.ymn

class WorkArea:
    def __init__(self,xmn,xmx,ymn,ymx):
        self.xmn = xmn
        self.xmx = xmx
        self.ymn = ymn
        self.ymx = ymx
    
    def checkPointInArea(self,x,y):
        if x < self.xmx and x > self.xmn and y < self.ymx and y > self.ymn:
            return True
        else:
            return False

    def generatePointInArea(self):
        return [rnd(self.xmn,self.xmx),rnd(self.ymn,self.ymx)]



class RaycastCallback(b2RayCastCallback):
    def __init__(self, **kwargs): 
        super(RaycastCallback, self).__init__(**kwargs)
        self.hit = False
        self.fixture = None
        self.points  = []
        self.normals = []
    def ReportFixture(self, fixture, point, normal, fraction): 
        self.hit = True
        self.fixture = fixture
        self.points.append(point)
        self.normals.append(normal)
        return 1.0


class SlideBlock:
    def __init__(self,**kwargs):
        self.w = 2000
        self.h = 1000
        self.bg_img = cv2.resize(cv2.cvtColor(np.random.randint(225,256,(self.h//8,self.w//8)).astype(np.uint8), cv2.COLOR_GRAY2BGR), dsize=(self.w,self.h), interpolation=cv2.INTER_NEAREST)
        self.xmn = -1
        self.xmx = 1
        self.ymn = -0.5
        self.ymx = 0.5
        self.workArea = WorkArea(-0.9,0.5,-0.4,0.4)
        self.action_space = spaces.Box(low=np.array([-1, -1, -1, -1]), high=np.array([1, 1, 1, 1]), dtype=np.float64)
        self.observation_space = spaces.Box(low=-1, high=1, shape=[5])
        self.nSubtasks = 2
        if '_max_episode_step' in kwargs:
            self._max_episode_step = kwargs['_max_episode_step']
        else:
            self._max_episode_step = 1000

        self.reset()

    def w2p(self,x,y): return (int((x-self.xmn)/(self.xmx-self.xmn)*self.w), int(self.h-(y-self.ymn)/(self.ymx-self.ymn)*self.h))
    def p2w(self,x,y): return (float(x)/self.w*(self.xmx-self.xmn)+self.xmn), (float(y)-self.h)/-self.h*(self.ymx-self.ymn)+self.ymn
    def w2r(self,r)  : return  int(r/(self.xmx-self.xmn)*self.w)



    def reset(self):
        self.world = b2World(gravity=(0,0))
        blockInArea = False
        while not blockInArea:
            blockInArea = True
            position = self.workArea.generatePointInArea()
            angle = rnd(-np.pi,np.pi)
            self.block = self.world.CreateDynamicBody(position=tuple(position), shapes=b2PolygonShape(box=(0.05,0.2)), angle=angle, angularVelocity=0, linearDamping=20.0, angularDamping=30.0)
            for localPoint in self.block.fixtures[0].shape.vertices:
                world_point = self.block.GetWorldPoint(localPoint)
                if not self.workArea.checkPointInArea(world_point.x,world_point.y):
                    blockInArea = False
                    break
                
        self.agent = Agent(self.workArea)

        return self._obs()

    def _paintBlock(self,img):
        points = []
        for localPoint in self.block.fixtures[0].shape.vertices:
            world_point = self.block.GetWorldPoint(localPoint)
            pix_point = self.w2p(*world_point)
            points.append(pix_point)
        cv2.fillConvexPoly(img, points=np.array(points), color=(64,64,64))
        cv2.polylines(img, pts=[np.array(points)], isClosed=True, color=(0,0,0), thickness=8)

        centre = self.block.GetWorldPoint((0,0))
        triangle = [self.w2p(*centre)]
        triangle.append(points[0])
        triangle.append(points[1])
        cv2.polylines(img, pts=[np.array(triangle)], isClosed=True, color=(0,255,0), thickness=2)
        return img

    def render(self,show = True):
        img = self.bg_img.copy()
        prePoints = [(-0.9,-0.4),(-0.9,0.4),(0.5,0.4),(0.5,-0.4)]
        points = []
        for point in prePoints:
            points.append(self.w2p(*point))
        cv2.polylines(img,pts=[np.array(points)],isClosed=True,color=(0,0,0),thickness=8)

        prePoints = [(0.55,-0.4),(0.55,0.4),(0.9,0.4),(0.9,-0.4)]
        points = []
        for point in prePoints:
            points.append(self.w2p(*point))
        cv2.polylines(img,pts=[np.array(points)],isClosed=True,color=(0,255,0),thickness=8)

        img = self._paintBlock(img)
        

        agent_color = (255,0,0) if not self.agent.grasped else (0,0,255)
        cv2.circle(img, (self.w2p(self.agent.pos[0],self.agent.pos[1])), 30, agent_color, -1)
        if show:
            cv2.namedWindow("BlockSlide", cv2.WINDOW_NORMAL)
            cv2.imshow("BlockSlide", img)
            cv2.waitKey(1)
        else:
            return img

    def _get_position_data(self):
        block_angle = np.arctan2(np.sin(self.block.angle), np.cos(self.block.angle))
        block_loc = np.array(self.block.GetWorldPoint(self.block.position))
        block_corners = []
        for localPoint in self.block.fixtures[0].shape.vertices:
            block_corners.append(self.block.GetWorldPoint(localPoint))
        return block_loc,block_angle,block_corners

    def _genReward(self):
        reward = []
        block_loc,_,block_corners = self._get_position_data()
        agent_loc = self.agent.pos
        a2bDist = np.linalg.norm(agent_loc-block_loc)
        grasped = self.agent.grasped

        reward.append(-a2bDist+grasped)

        b2gDist = -1000
        for point in block_corners:
            if point[0] > b2gDist:
                b2gDist = point[0]
        success = self._success()
        reward.append(b2gDist+success)

        return reward

    def _getCheckpoint(self):
        return 0 if not self.agent.grasped else 1

    def _success(self):
        _,_,block_corners = self._get_position_data()
        b2gDist = -1000
        for point in block_corners[:2]:
            if point[0] > b2gDist:
                b2gDist = point[0]
        return 1 if b2gDist > 0.5 else 0
        

    def _obs(self):
        obs = [np.array(self.agent.pos)]
        block_loc,block_angle,_ = self._get_position_data()
        obs.append(block_loc)
        obs.append(np.array([block_angle]))
        return np.concatenate(obs)


    def checkGrippable(self):
        grippable = True
        for y in [self.workArea.ymn,self.workArea.ymx]:
            for x in [self.workArea.xmn,self.workArea.xmx]:
                try:
                    if [x,y] != self.agent.pos:
                        callback = RaycastCallback()
                        self.world.RayCast(callback, [x,y],self.agent.pos)
                        if len(callback.points) == 0:
                            grippable = False
                            break
                except:
                    print('Error in checking graspability')

        return grippable 

    def _fixAngle(self):
        angle = self.block.angle % (2*np.pi)
        if angle > np.pi:
            angle -= 2*np.pi
        self.block.angle = angle  

    def step(self,act):
        dx,dy,dTheta,grasp = act
        dx /= 100
        dy /= 100
        dTheta /=50
        prevAgentPos = copy(self.agent.pos)
        self.agent.step(dx,dy)

        if self.agent.grasped:
            self.block.position[0] += (self.agent.pos[0]-prevAgentPos[0])
            self.block.position[1] += (self.agent.pos[1]-prevAgentPos[1])

            blockAngle = self.block.angle
            relativeAngle = np.arctan2(self.block.position[0]-self.agent.pos[0],self.block.position[1]-self.agent.pos[1])
            relativeDist = np.linalg.norm([self.block.position[0]-self.agent.pos[0],self.block.position[1]-self.agent.pos[1]])

            dx = relativeDist * (np.sin(relativeAngle)-np.sin(relativeAngle+dTheta))
            dy = relativeDist * (np.cos(relativeAngle)-np.cos(relativeAngle+dTheta))

            self.block.angle += dTheta
            self._fixAngle()
            self.block.position[0] += dx
            self.block.position[1] += dy
        else:
            relativeDist = 2

        if grasp > 0 and not self.agent.grasped and self.checkGrippable():
            self.agent.grasped = True
        if (grasp <=0 and self.agent.grasped) or not self.checkGrippable():
            self.agent.grasped = False

        rews = self._genReward()
        check = self._getCheckpoint()

        info = {"reward{}".format(n):rews[n] for n in range(len(rews))}
        info["overallReward"] = np.sum(rews)
        info["subtask"] = check
        info["success"] = self._success()
        info["graspDistance"] = relativeDist

        obs = self._obs()
        if 

        return obs,rews[check],False,info
            


if __name__ == "__main__":
    env = SlideBlock()
    step = 0
    while True:
        step += 1
        print(step)
        env.step([rnd(-1,1),rnd(-1,1),rnd(-1,1),1])
        env.render()
        key = cv2.waitKey(10)
        if key == ord('q'):
            break
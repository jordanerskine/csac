import cv2, numpy as np, colorsys
from colour import Color



def visualiseData(env, dataPoints, show = True, n = 0):
    done = False
    gradient = list(Color("red").range_to(Color("yellow"),100))+list(Color("yellow").range_to(Color("green"),100))
    showAll = True
    size = 10
    while not done:
        cv2.destroyAllWindows()
        img = env.bg_img.copy()
        img = env._drawWalls(img)
        name = 'QF{}'.format(n)
        if len(dataPoints) > 0:
            minQ = min([min([point[name] for point in run if (point['subtask'] in [n-1,n] or showAll)]) for run in dataPoints])
            maxQ = max([max([point[name] for point in run if (point['subtask'] in [n-1,n] or showAll)]) for run in dataPoints])
            for run in dataPoints:
                
                
                for point in run:
                    if showAll or point['subtask'] in [n-1,n]:
                    
                        pose = env.w2p(point['o'][-3],point['o'][-2])
                        y = pose[0]
                        x = pose[1]
                        

                        normVal = (point[name]-minQ)/(maxQ-minQ)
                        c = gradient[int(normVal*199)]
                        c = (np.array(c.rgb)*255).astype(np.uint8)
                        img[max(0,x-size):x+size,y-size:y+size] = c

        if show: 
            cv2.namedWindow(name,cv2.WINDOW_NORMAL)
            cv2.resizeWindow(name,600,600)
        
            cv2.imshow(name,img)
            key = cv2.waitKey()
            if key == ord('q'):
                done = True
            elif key == ord('s'):
                showAll = not showAll
            elif int(chr(key)) in [n for n in range(10)]:
                n = int(chr(key))
            
        else:
            return img


            
            

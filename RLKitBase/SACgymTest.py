import gym
from SAC import *

for name in ['Hopper-v3','Walker2d-v3','HalfCheetah-v3','Ant-v3','Humanoid-v3']:

    env = gym.make(name)
    params = {'actSize':env.action_space.shape[0],
                'obsSize':env.observation_space.shape[0],
                'gamma':0.99,
                'nNets':1}
    agent = SacAgent(params)
    timestep = 0

    time = datetime.datetime.now()
    date = '{}-{}={}:{}:{}'.format(time.month, time.day, time.hour, time.minute, time.second)
    writer = SummaryWriter(logdir="./Runs/BaselineTest/{}/{}".format(date,name),comment=date)
    ep = 0
    while timestep < 1000000:
        length = 0
        done = False
        obs = env.reset()
        totalReward = 0
        ep+= 1
        while not done:
            act = agent.getAction(torch.Tensor(obs))
            nobs,rew,done,_ = env.step(np.array(act))
            totalReward += rew
            
            agent.appendExp(obs,act,rew,nobs,done)
            agent.train()
            obs = nobs

            length += 1
            timestep += 1
        if ep % 20 == 0:
            print("Ep {} - Timestep: {}".format(ep,timestep))
        
        writer.add_scalar("Progress/TotalReward",totalReward,timestep)
        writer.add_scalar("Progress/Episode",ep,timestep)
        writer.add_scalar("Progress/Length",length,timestep)
        


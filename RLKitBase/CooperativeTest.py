from SAC import *

import numpy as np, sys, time, inspect, threading as T
from copy import deepcopy as copy
from rlbench.environment import Environment
from rlbench.action_modes import ActionMode, ArmActionMode
import rlbench.tasks as Tasks

def getObs(task,obs):
    nobs = []
    nobs.append(obs.joint_positions)
    nobs.append(obs.joint_velocities)
    #Add cup poses
    nobs.append(np.array(task._task.cup_source.get_position()))
    nobs.append(np.array(task._task.cup_source.get_orientation()))
    nobs.append(np.array(task._task.cup_target.get_position()))

    nobs = np.concatenate(nobs)
    return nobs

def shapeReward(task,obs,goalRange = 1):
    gripper = np.array(obs.gripper_pose[:3])
    goal = np.array(task._task.cup_source.get_position())
    dist = np.linalg.norm(gripper-goal)
    raised = 1 if goal[2] > 0.8 else 0
    rew = max((1-dist/goalRange),0)+raised
    return rew

def step(agent,task,obs,shapeRew = True,goalRange = 1):
    act = agent.getAction(torch.Tensor(obs))
    global trueRew
    global trueObs
    trueRew = 0
    
    try:
        nobs,trueRew,_ = task.step(act)
        
        if shapeRew:
            rew = trueRew + shapeReward(task,nobs,goalRange)
        else:
            rew = copy(trueRew)
        trueObs = copy(nobs)
        nobs = getObs(task,nobs)
        agent.appendExp(obs,act,rew,nobs,0)
    except:
        agent.appendExp(obs,act,-1,obs,1)

def saveModel(agent,ep):
    global name
    global taskName
    try:
        os.mkdir("./Runs/{}/{}/SavedModels".format(taskName,name))
    except:
        pass
    location = "./Runs/{}/{}/SavedModels/".format(taskName,name)
    agent.saveAgent(location,"Ep{}".format(ep))


def gatherVideo(vid):
    vid = np.stack(vid)
    vid = np.expand_dims(vid,0)
    vid = np.transpose(vid,[0,1,4,2,3])
    vid = torch.from_numpy(vid)
    return vid


hypers = {}
def arg(tag, default,delete = False):
    if tag in sys.argv:
        val = sys.argv[sys.argv.index(tag)+1]
        if "," in val:
            val = re.split(",",val)
        else:
            val = [val]
        hypers[tag] = [type(default)((t)) for t in val]
    else:
        hypers[tag] = [default]
    if delete:
        ans = copy(hypers[tag])
        del hypers[tag]
        return ans
    return hypers[tag]


headless = arg('--headless',1,True)[0]
timesteps = arg('--timesteps',int(1e6))[0]
debug = arg('--debug',0,True)[0]
episodeLength = arg('--length',200)[0]
shapeRew = arg('--shapeRew',1)[0]
goalRange = arg('--goalRange',1.0)[0]
recordRuns = arg('--recordRuns',1)[0]
loadModel = arg('--loadModel',"None")[0]

actionMode = ActionMode(ArmActionMode.ABS_EE_VELOCITY)
env = Environment(actionMode, headless = headless)
env.launch()
taskName = 'PourFromCupToCup'
task = getattr(Tasks,taskName)
task = env.get_task(task)
d, obs = task.reset()
obs = getObs(task,obs)
params = {'actSize':actionMode.action_size,
          'obsSize':len(obs)}

print(params)
agent = SacAgent(params)
if loadModel != "None":
    try:
        agent.loadAgent(loadModel)
    except:
        print("Failed to load model: {}".format(loadModel))
if not debug:
    time = datetime.datetime.now()
    name = 'SinglePolicyTest:{}-{}={}:{}:{}'.format(time.month, time.day, time.hour, time.minute, time.second)
    writer = SummaryWriter(logdir="./Runs/{}/{}".format(taskName,name),comment=name)
    for elem in agent.params:
        writer.add_text("AgentHyperparameters/{}".format(elem),str(agent.params[elem]),0)
    for elem in hypers:
        writer.add_text("OverallHyperparameters/{}".format(elem[2:]),str(hypers[elem][0]),0)

timestep = 0
ep = 0
while timestep < timesteps:
    reset = False
    failedResets = 0
    while not reset and failedResets < 20:
        try:
            _,trueObs = task.reset()
            reset = True
        except:
            failedResets += 1
            print("Ep {} failed to reset {} time{}".format(str(ep),str(failedResets),'s' if failedResets > 1 else ''))
    if failedResets >= 20:
        break
    obs = getObs(task,trueObs)
    agent.lossDict = {}
    length = 0
    done = False
    ep += 1
    if ep % 20 == 0:
        print(ep)
    totalRew = []
    totalTrueRew = []
    nTrains = []
    acts = []
    vid = []
    while not done:
        stepThread = T.Thread(target=step,args=(agent,task,obs,shapeRew,goalRange))
        nTrains.append(0)                
        stepThread.start()

        while stepThread.is_alive():
            if len(agent.buffer) > agent.params['batchSize']:
                nTrains[-1] += 1
            agent.train()
        
        stepThread.join()

        if recordRuns and ep%100 ==0:
            vid.append(trueObs.right_shoulder_rgb)
        base = agent.policy.base(torch.Tensor(obs))
        acts.append([agent.policy.mu(base).data.numpy(),agent.policy.sig(base).data.numpy()])

        done = agent.buffer[-1].done or length > episodeLength
        totalTrueRew.append(trueRew)
        totalRew.append(agent.buffer[-1].rew)
        length += 1
        timestep += 1
        obs=copy(agent.buffer[-1].nobs)
    
    if not debug:
        writer.add_scalar("Progress/Length",length,timestep)
        writer.add_scalar("Progress/TotalReward",sum(totalRew),timestep)
        writer.add_scalar("Progress/TrueTotalReward",sum(totalTrueRew),timestep)
        writer.add_scalar("Progress/Episode",ep,timestep)
        writer.add_scalar("Progress/RaisedCup",sum(np.array(totalRew)>=1),timestep)
        writer.add_scalar("Monitoring/TrainsPerStep",np.mean(nTrains),timestep)
        writer.add_scalar("Monitoring/ActionMean",np.mean(np.array(acts)[:,0]),timestep)
        writer.add_scalar("Monitoring/ActionSigma",np.mean(np.array(acts)[:,1]),timestep)
        if len(agent.lossDict)>0:
            for elem in agent.lossDict:
                writer.add_scalar("Losses/{}Mean".format(elem),torch.mean(agent.lossDict[elem]),timestep)
                if not torch.isnan(torch.std(agent.lossDict[elem])):
                    writer.add_scalar("Losses/{}Std".format(elem),torch.std(agent.lossDict[elem]),timestep)
        
        if ep%100 == 0:
            if len(vid)>5:
                video = gatherVideo(vid)
                writer.add_video("Runs/ep{}".format(ep),vid_tensor=video,fps=20)
            saveModel(agent,ep)
                    


    

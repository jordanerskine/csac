import abc
from collections import OrderedDict, deque

from typing import Iterable
import torch.optim as optim
from torch import nn as nn
import gtimer as gt
import torch
import rlkit.torch.pytorch_util as ptu
import numpy as np

from rlkit.core.trainer import Trainer
from rlkit.core import logger, eval_util
from rlkit.torch.core import np_to_pytorch_batch
from rlkit.data_management.replay_buffer import ReplayBuffer
from rlkit.samplers.data_collector import PathCollector, DataCollector
from rlkit.torch.torch_rl_algorithm import TorchTrainer
from rlkit.core.eval_util import create_stats_ordered_dict


class CooperativeBaseRLAlgorithm(object, metaclass=abc.ABCMeta):
    def __init__(
            self,
            trainers,                                               # ---- CHANGED ----
            exploration_env,
            evaluation_env,
            exploration_data_collector: DataCollector,
            evaluation_data_collector: DataCollector,
            replay_buffers: [ReplayBuffer],                         # ---- CHANGED ----
    ):
        self.trainers = trainers                                    # ---- CHANGED ----
        self.expl_env = exploration_env
        self.eval_env = evaluation_env
        self.expl_data_collector = exploration_data_collector
        self.eval_data_collector = evaluation_data_collector
        self.replay_buffers = replay_buffers                        # ---- CHANGED ----
        self._start_epoch = 0

        self.post_epoch_funcs = []

    def train(self, start_epoch=0):
        self._start_epoch = start_epoch
        self._train()

    def _train(self):
        """
        Train model.
        """
        raise NotImplementedError('_train must implemented by inherited class')

    def set_log_iterations(self,iteration_step=100):
        logger.set_snapshot_gap(iteration_step)
        logger.set_snapshot_mode("gap_and_last")


    def _end_epoch(self, epoch):
        snapshot = self._get_snapshot()
        self.set_log_iterations()
        logger.save_itr_params(epoch, snapshot)
        gt.stamp('saving')
        self._log_stats(epoch)

        self.expl_data_collector.end_epoch(epoch)
        self.eval_data_collector.end_epoch(epoch)
        for replay_buffer in self.replay_buffers:                   # ---- CHANGED ----
            replay_buffer.end_epoch(epoch)                          # ---- CHANGED ----
        for trainer in self.trainers:                               # ---- CHANGED ----
            trainer.end_epoch(epoch)                                # ---- CHANGED ----

        for post_epoch_func in self.post_epoch_funcs:
            post_epoch_func(self, epoch)

    def _get_snapshot(self):
        snapshot = {}
        for n, trainer in enumerate(self.trainers):                     # ---- CHANGED ----
            for k, v in trainer.get_snapshot().items():                 # ---- CHANGED ----
                snapshot['trainer{}/'.format(n) + k] = v                # ---- CHANGED ----
        for k, v in self.expl_data_collector.get_snapshot().items():
            snapshot['exploration/' + k] = v
        for k, v in self.eval_data_collector.get_snapshot().items():
            snapshot['evaluation/' + k] = v
        for n, replay_buffer in enumerate(self.replay_buffers):         # ---- CHANGED ----
            for k, v in replay_buffer.get_snapshot().items():           # ---- CHANGED ----
                snapshot['replay_buffer{}/'.format(n) + k] = v          # ---- CHANGED ----
        return snapshot

    def _log_stats(self, epoch):
        logger.log("Epoch {} finished".format(epoch), with_timestamp=True)

        """
        Replay Buffer
        """
        for n, replay_buffer in enumerate(self.replay_buffers):
            logger.record_dict(
                replay_buffer.get_diagnostics(),
                prefix='replay_buffer{}/'.format(n)
            )

        """
        Trainer
        """
        for n, trainer in enumerate(self.trainers):
            logger.record_dict(trainer.get_diagnostics(), prefix='trainer{}/'.format(n))

        """
        Exploration
        """
        logger.record_dict(
            self.expl_data_collector.get_diagnostics(),
            prefix='exploration/'
        )
        expl_paths = self.expl_data_collector.get_epoch_paths()
        if hasattr(self.expl_env, 'get_diagnostics'):
            logger.record_dict(
                self.expl_env.get_diagnostics(expl_paths),
                prefix='exploration/',
            )
        if len(expl_paths) > 0:
            logger.record_dict(
                eval_util.get_generic_path_information(expl_paths),
                prefix="exploration/",
            )
        """
        Evaluation
        """
        logger.record_dict(
            self.eval_data_collector.get_diagnostics(),
            prefix='evaluation/',
        )
        eval_paths = self.eval_data_collector.get_epoch_paths()
        if hasattr(self.eval_env, 'get_diagnostics'):
            logger.record_dict(
                self.eval_env.get_diagnostics(eval_paths),
                prefix='evaluation/',
            )
        if len(eval_paths) > 0:
            logger.record_dict(
                eval_util.get_generic_path_information(eval_paths),
                prefix="evaluation/",
            )

        """
        Misc
        """
        gt.stamp('logging')
        logger.record_dict(self._get_epoch_timings())
        logger.record_tabular('Epoch', epoch)
        logger.dump_tabular(with_prefix=False, with_timestamp=False)

    def _get_epoch_timings(self):
        times_itrs = gt.get_times().stamps.itrs
        times = OrderedDict()
        epoch_time = 0
        for key in sorted(times_itrs):
            time = times_itrs[key][-1]
            epoch_time += time
            times['time/{} (s)'.format(key)] = time
        times['time/epoch (s)'] = epoch_time
        times['time/total (s)'] = gt.get_times().total
        return times

    @abc.abstractmethod
    def training_mode(self, mode):
        """
        Set training mode to `mode`.
        :param mode: If True, training will happen (e.g. set the dropout
        probabilities to not all ones).
        """
        pass

    

class CooperativeBatchRLAlgorithm(CooperativeBaseRLAlgorithm, metaclass=abc.ABCMeta):  # ---- CHANGED ----
    def __init__(
            self,
            trainers,                                   # ---- CHANGED ----
            exploration_env,
            evaluation_env,
            exploration_data_collector: PathCollector,
            evaluation_data_collector: PathCollector,
            replay_buffers: [ReplayBuffer],             # ---- CHANGED ----
            mode: 'cooperative',
            batch_size,
            max_path_length,
            num_epochs,
            num_eval_steps_per_epoch,
            num_expl_steps_per_train_loop,
            num_trains_per_train_loop,
            num_train_loops_per_epoch=1,
            min_num_steps_before_training=0,
    ):
        self.trainers = trainers                                    # ---- CHANGED ----
        self.expl_env = exploration_env                             # ---- CHANGED ----
        self.eval_env = evaluation_env                              # ---- CHANGED ----
        self.expl_data_collector = exploration_data_collector       # ---- CHANGED ----
        self.eval_data_collector = evaluation_data_collector        # ---- CHANGED ----
        self.replay_buffers = replay_buffers                        # ---- CHANGED ----
        self._start_epoch = 0                                       # ---- CHANGED ----
        self.mode = mode

        self.post_epoch_funcs = []
        self.batch_size = batch_size
        self.max_path_length = max_path_length
        self.num_epochs = num_epochs
        self.num_eval_steps_per_epoch = num_eval_steps_per_epoch
        self.num_trains_per_train_loop = num_trains_per_train_loop
        self.num_train_loops_per_epoch = num_train_loops_per_epoch
        self.num_expl_steps_per_train_loop = num_expl_steps_per_train_loop
        self.min_num_steps_before_training = min_num_steps_before_training

    def _train(self):
        if self.min_num_steps_before_training > 0:
            init_expl_paths = self.expl_data_collector.collect_new_paths(
                self.max_path_length,
                self.min_num_steps_before_training,
                discard_incomplete_paths=False,
            )
            self._appendToReplayBuffers(init_expl_paths)                        # ---- CHANGED ----
            self.expl_data_collector.end_epoch(-1)

        for epoch in gt.timed_for(
                range(self._start_epoch, self.num_epochs),
                save_itrs=True,
        ):
            self.eval_data_collector.collect_new_paths(
                self.max_path_length,
                self.num_eval_steps_per_epoch,
                discard_incomplete_paths=True
            )
            gt.stamp('evaluation sampling')

            for _ in range(self.num_train_loops_per_epoch):
                new_expl_paths = self.expl_data_collector.collect_new_paths(
                    self.max_path_length,
                    self.num_expl_steps_per_train_loop,
                    discard_incomplete_paths=False,
                )
                gt.stamp('exploration sampling', unique=False)

                self._appendToReplayBuffers(new_expl_paths)                                 # ---- CHANGED ----
                gt.stamp('data storing', unique=False)

                self.training_mode(True)
                for n in range(self.num_trains_per_train_loop):
                    for i, trainer in enumerate(self.trainers):
                        j = max(i-n%2,0) if self.mode == 'cooperative' else i
                        if self.replay_buffers[j]._size > self.batch_size:                                       # ---- CHANGED ----
                            train_data = self.replay_buffers[j].random_batch(        # ---- CHANGED ----
                                self.batch_size)
                            if j != i:
                                train_data['rewards'] = train_data['reward{}'.format(i)]                                 # ---- CHANGED ----
                                trainer.train(train_data,trainPolicy=False)
                            else:
                                trainer.train(train_data)                                               # ---- CHANGED ----
                gt.stamp('training', unique=False)
                self.training_mode(False)

            self._end_epoch(epoch)

    def _appendToReplayBuffers(self,paths):                                                 # ---- CHANGED ----
        sorted_paths = [[] for _ in range(len(self.replay_buffers))]                        # ---- CHANGED ----
        for path in paths:                                                                                          # ---- CHANGED ----
            for st in range(len(self.replay_buffers)):                                                              # ---- CHANGED ----
                ids = [n for n in range(len(path['env_infos'])) if path['env_infos'][n]['subtask'] == st]           # ---- CHANGED ----
                sorted_paths[st].append({tag:np.array(path[tag])[ids] for tag in path})                             # ---- CHANGED ----
        for rb, sp in zip(self.replay_buffers,sorted_paths):                                 # ---- CHANGED ----
            rb.add_paths(sp)                                                                # ---- CHANGED ----


class CooperativeTorchBatchRLAlgorithm(CooperativeBatchRLAlgorithm):
    def to(self, device):
        for trainer in self.trainers:                                                       # ---- CHANGED ----
            for net in trainer.networks:                                                    # ---- CHANGED ----
                net.to(device)

    def training_mode(self, mode):
        for trainer in self.trainers:                                                       # ---- CHANGED ----
            for net in trainer.networks:                                                    # ---- CHANGED ----
                net.train(mode)

class CooperativeTorchTrainer(Trainer, metaclass=abc.ABCMeta):
    def __init__(self):
        self._num_train_steps = 0

    def train(self, np_batch, trainPolicy = True):
        self._num_train_steps += 1
        batch = np_to_pytorch_batch(np_batch)
        self.train_from_torch(batch, trainPolicy = trainPolicy)

    def get_diagnostics(self):
        return OrderedDict([
            ('num train calls', self._num_train_steps),
        ])

    @abc.abstractmethod
    def train_from_torch(self, batch):
        pass

    @property
    @abc.abstractmethod
    def networks(self) -> Iterable[nn.Module]:
        pass

class CooperativeSACTrainer(CooperativeTorchTrainer):
    def __init__(
            self,
            env,
            policy,
            qf1,
            qf2,
            target_qf1,
            target_qf2,

            next_qf1 = None,            # ---- CHANGED ----
            next_qf2 = None,            # ---- CHANGED ----
            prev_pol = None,

            cooperative = True,         # ---- CHANGED ----
            cooperativeRatio = 0.5,
            trainableCRatio = False,

            batchNorm = True,           # ---- CHANGED ----

            discount=0.99,
            reward_scale=1.0,

            policy_lr=1e-3,
            cRatio_lr = 1e-4,
            qf_lr=1e-3,
            optimizer_class=optim.Adam,

            soft_target_tau=1e-2,
            target_update_period=1,
            plotter=None,
            render_eval_paths=False,

            use_automatic_entropy_tuning=True,
            target_entropy=None,
    ):
        super().__init__()
        self.env = env
        self.policy = policy
        self.qf1 = qf1
        self.qf2 = qf2
        self.target_qf1 = target_qf1                
        self.target_qf2 = target_qf2
        self.prev_pol = prev_pol
        self.next_qf1 = next_qf1                # ---- CHANGED ----
        self.next_qf2 = next_qf2                # ---- CHANGED ----
        self.cooperative = cooperative          # ---- CHANGED ----
        self.cooperativeRatio = cooperativeRatio
        self.trainCRatio = trainableCRatio
        self.batchNorm = batchNorm              # ---- CHANGED ----
        self.soft_target_tau = soft_target_tau
        self.target_update_period = target_update_period

        self.use_automatic_entropy_tuning = use_automatic_entropy_tuning
        if self.use_automatic_entropy_tuning:
            if target_entropy:
                self.target_entropy = target_entropy
            else:
                self.target_entropy = -np.prod(self.env.action_space.shape).item()  # heuristic value from Tuomas
            self.log_alpha = ptu.zeros(1, requires_grad=True)
            self.alpha_optimizer = optimizer_class(
                [self.log_alpha],
                lr=policy_lr,
            )
        
        if self.trainCRatio:
            self.cooperativeRatio = ptu.zeros(1,requires_grad=True)
            self.cRatioOptim = optimizer_class(
                [self.cooperativeRatio],
                lr = cRatio_lr)

        self.plotter = plotter
        self.render_eval_paths = render_eval_paths

        self.qf_criterion = nn.MSELoss()
        self.vf_criterion = nn.MSELoss()

        self.policy_optimizer = optimizer_class(
            self.policy.parameters(),
            lr=policy_lr,
        )
        self.qf1_optimizer = optimizer_class(
            self.qf1.parameters(),
            lr=qf_lr,
        )
        self.qf2_optimizer = optimizer_class(
            self.qf2.parameters(),
            lr=qf_lr,
        )

        self.discount = discount
        self.reward_scale = reward_scale
        self.eval_statistics = OrderedDict()
        for tag in ['QF1 Loss', 'QF2 Loss', 'Policy Loss', 'Q1 Predictions',
                    'Q2 Predictions', 'Q Targets', 'Log Pis', 'Policy mu',
                    'Policy log', 'Alpha', 'Alpha Loss', 'Policy log std']:
            if tag in ['Q1 Predictions', 'Q2 Predictions', 'Q Targets', 'Log Pis', 
                        'Policy mu', 'Policy log', 'Policy log std']:
                for extension in ['Mean','Std','Max','Min']:
                    self.eval_statistics['{} {}'.format(tag,extension)] = 0
            else:
                self.eval_statistics[tag] = 0
        self._n_train_steps_total = 0
        self._need_to_update_eval_statistics = True

        

    def train_from_torch(self, batch, trainPolicy = True):
        rewards = batch['rewards']
        terminals = batch['terminals']
        obs = batch['observations']
        actions = batch['actions']
        next_obs = batch['next_observations']

        """
        Policy and Alpha Loss
        """
        new_obs_actions, policy_mean, policy_log_std, log_pi, *_ = self.policy(
            obs, reparameterize=True, return_log_prob=True,
        )
        if self.use_automatic_entropy_tuning:
            alpha_loss = -(self.log_alpha * (log_pi + self.target_entropy).detach()).mean()
            self.alpha_optimizer.zero_grad()
            alpha_loss.backward()
            self.alpha_optimizer.step()
            alpha = self.log_alpha.exp()
        else:
            alpha_loss = 0
            alpha = 1 if self.target_entropy is None else self.target_entropy

        

                
        if trainPolicy:

            q_new_actions = torch.min(
                self.qf1(obs, new_obs_actions),
                self.qf2(obs, new_obs_actions),
            )
            if self.batchNorm: 
                q_new_actions = normaliseTensor(q_new_actions)
            if self.next_qf1 is not None and self.cooperative:          # ---- CHANGED ----
                q_next = torch.min(                                     # ---- CHANGED ----
                    self.next_qf1(obs, new_obs_actions),                # ---- CHANGED ----
                    self.next_qf2(obs, new_obs_actions),                # ---- CHANGED ----
                )                                                       # ---- CHANGED ----
                if self.batchNorm:
                    q_next = normaliseTensor(q_next)
                if self.trainCRatio:
                    sig = nn.Sigmoid()
                    cRatio = sig(self.cooperativeRatio)
                else:
                    cRatio = self.cooperativeRatio
                q_new_actions = cRatio*q_new_actions + (1-cRatio)*q_next

            policy_loss = (alpha*log_pi - q_new_actions).mean()

        """
        QF Loss
        """
        q1_pred = self.qf1(obs, actions)
        q2_pred = self.qf2(obs, actions)
        # Make sure policy accounts for squashing functions like tanh correctly!
        if trainPolicy:
            new_next_actions, _, _, new_log_pi, *_ = self.policy(
                next_obs, reparameterize=True, return_log_prob=True,
            )
        else:
            new_next_actions, _, _, new_log_pi, *_ = self.prev_pol(
                next_obs, reparameterize=True, return_log_prob=True,
            )

        target_q_values = torch.min(
            self.target_qf1(next_obs, new_next_actions),
            self.target_qf2(next_obs, new_next_actions),
        ) - alpha * new_log_pi

        q_target = self.reward_scale * rewards + (1. - terminals) * self.discount * target_q_values
        maxExpectedRew = 5/(1-self.discount)  # Arbitrary max rew
        q_target = torch.clamp(q_target,min=-maxExpectedRew,max=maxExpectedRew)
        qf1_loss = self.qf_criterion(q1_pred, q_target.detach())
        qf2_loss = self.qf_criterion(q2_pred, q_target.detach())

        """
        Update networks
        """
        self.qf1_optimizer.zero_grad()
        qf1_loss.backward()
        self.qf1_optimizer.step()

        self.qf2_optimizer.zero_grad()
        qf2_loss.backward()
        self.qf2_optimizer.step()

        if trainPolicy:
            if self.trainCRatio:
                self.cRatioOptim.zero_grad()
            self.policy_optimizer.zero_grad()
            policy_loss.backward()
            self.policy_optimizer.step()
            if self.trainCRatio:
                self.cRatioOptim.step()

            

        """
        Soft Updates
        """
        if self._n_train_steps_total % self.target_update_period == 0:
            ptu.soft_update_from_to(
                self.qf1, self.target_qf1, self.soft_target_tau
            )
            ptu.soft_update_from_to(
                self.qf2, self.target_qf2, self.soft_target_tau
            )

        """
        Save some statistics for eval
        """
        if self._need_to_update_eval_statistics:
            self._need_to_update_eval_statistics = False
            """
            Eval should set this to None.
            This way, these statistics are only computed for one batch.
            """
            if trainPolicy:
                policy_loss = (alpha*log_pi - q_new_actions).mean()

            self.eval_statistics['QF1 Loss'] = np.mean(ptu.get_numpy(qf1_loss))
            self.eval_statistics['QF2 Loss'] = np.mean(ptu.get_numpy(qf2_loss))
            if trainPolicy:
                self.eval_statistics['Policy Loss'] = np.mean(ptu.get_numpy(
                    policy_loss
                ))
                if self.next_qf1 is not None and self.cooperative:
                    print(cRatio)
                    if isinstance(cRatio,torch.Tensor):
                        cRatio = np.mean(ptu.get_numpy(cRatio))
                    self.eval_statistics['cooperativeRatio'] = cRatio
            self.eval_statistics.update(create_stats_ordered_dict(
                'Q1 Predictions',
                ptu.get_numpy(q1_pred),
            ))
            self.eval_statistics.update(create_stats_ordered_dict(
                'Q2 Predictions',
                ptu.get_numpy(q2_pred),
            ))
            self.eval_statistics.update(create_stats_ordered_dict(
                'Q Targets',
                ptu.get_numpy(q_target),
            ))
            if trainPolicy:
                self.eval_statistics.update(create_stats_ordered_dict(
                    'Log Pis',
                    ptu.get_numpy(log_pi),
                ))
                self.eval_statistics.update(create_stats_ordered_dict(
                    'Policy mu',
                    ptu.get_numpy(policy_mean),
                ))
                self.eval_statistics.update(create_stats_ordered_dict(
                    'Policy log std',
                    ptu.get_numpy(policy_log_std),
                ))
            if self.use_automatic_entropy_tuning:
                self.eval_statistics['Alpha'] = alpha.item()
                self.eval_statistics['Alpha Loss'] = alpha_loss.item()
        self._n_train_steps_total += 1

    def get_diagnostics(self):
        return self.eval_statistics

    def end_epoch(self, epoch):
        self._need_to_update_eval_statistics = True

    @property
    def networks(self):
        return [
            self.policy,
            self.qf1,
            self.qf2,
            self.target_qf1,
            self.target_qf2,
        ]

    def get_snapshot(self):
        return dict(
            policy=self.policy,
            qf1=self.qf1,
            qf2=self.qf2,
            target_qf1=self.qf1,
            target_qf2=self.qf2,
        )

class CooperativeMdpPathCollector(PathCollector):
    def __init__(
            self,
            env,
            policies,                                   # ---- CHANGED ----
            max_num_epoch_paths_saved=None,
            render=False,
            render_kwargs=None,
    ):
        if render_kwargs is None:
            render_kwargs = {}
        self._env = env
        self._policies = policies                       # ---- CHANGED ----
        self._max_num_epoch_paths_saved = max_num_epoch_paths_saved
        self._epoch_paths = deque(maxlen=self._max_num_epoch_paths_saved)
        self._render = render
        self._render_kwargs = render_kwargs

        self._num_steps_total = 0
        self._num_paths_total = 0

    def collect_new_paths(
            self,
            max_path_length,
            num_steps,
            discard_incomplete_paths,
    ):
        paths = []
        num_steps_collected = 0
        while num_steps_collected < num_steps:
            max_path_length_this_loop = min(  # Do not go over num_steps
                max_path_length,
                num_steps - num_steps_collected,
            )
            path = rollout(
                self._env,
                self._policies,                     # ---- CHANGED ----
                max_path_length=max_path_length_this_loop,
                render=self._render                 # ---- CHANGED ----
            )
            path_len = len(path['actions'])
            if (
                    path_len != max_path_length
                    and not path['terminals'][-1]
                    and discard_incomplete_paths
            ):
                break
            num_steps_collected += path_len
            paths.append(path)
        self._num_paths_total += len(paths)
        self._num_steps_total += num_steps_collected
        self._epoch_paths.extend(paths)
        return paths

    def get_epoch_paths(self):
        return self._epoch_paths

    def end_epoch(self, epoch):
        self._epoch_paths = deque(maxlen=self._max_num_epoch_paths_saved)

    def get_diagnostics(self):
        path_lens = [len(path['actions']) for path in self._epoch_paths]
        stats = OrderedDict([
            ('num steps total', self._num_steps_total),
            ('num paths total', self._num_paths_total),
        ])
        stats.update(create_stats_ordered_dict(
            "path length",
            path_lens,
            always_show_all_stats=True,
        ))
        return stats

    def get_snapshot(self):
        return dict(
            #env=self._env,
            policy=self._policies,                  # ---- CHANGED ----
        )

def rollout(
        env,
        agents,
        max_path_length=np.inf,
        render=False,
        render_kwargs=None,):
    """
    The following value for the following keys will be a 2D array, with the
    first dimension corresponding to the time dimension.
     - observations
     - actions
     - rewards
     - next_observations
     - terminals

    The next two elements will be lists of dictionaries, with the index into
    the list being the index into the time
     - agent_infos
     - env_infos
    """
    if render_kwargs is None:
        render_kwargs = {}
    observations = []
    actions = []
    rewards = []
    terminals = []
    agent_infos = []
    env_infos = []
    o = env.reset()
    agent = agents[0]                                       # ---- CHANGED ----
    # agent.reset()                                           # ---- CHANGED ----
    next_o = None
    path_length = 0
    if render:
        env.render(**render_kwargs)
    while path_length < max_path_length:
        a, agent_info = agent.get_action(o)
        next_o, r, d, env_info = env.step(a)
        agent = agents[env_info["subtask"]]                 # ---- CHANGED ----
        observations.append(o)
        rewards.append(r)
        terminals.append(d)
        actions.append(a)
        agent_infos.append(agent_info)
        env_infos.append(env_info)
        path_length += 1
        if d:
            break
        o = next_o
        if render:
            env.render(**render_kwargs)

    actions = np.array(actions)
    if len(actions.shape) == 1:
        actions = np.expand_dims(actions, 1)
    observations = np.array(observations)
    if len(observations.shape) == 1:
        observations = np.expand_dims(observations, 1)
        next_o = np.array([next_o])
    next_observations = np.vstack(
        (
            observations[1:, :],
            np.expand_dims(next_o, 0)
        )
    )
    return dict(
        observations=observations,
        actions=actions,
        rewards=np.array(rewards).reshape(-1, 1),
        next_observations=next_observations,
        terminals=np.array(terminals).reshape(-1, 1),
        agent_infos=agent_infos,
        env_infos=env_infos,
    )

def normaliseTensor(tensor):
    mx = torch.max(tensor).detach()
    mn = torch.min(tensor).detach()
    return (tensor-mn)/(mx-mn)
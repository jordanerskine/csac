# from gym.envs.mujoco import HalfCheetahEnv, HopperEnv, Walker2dEnv, AntEnv, HumanoidEnv
# from gym.envs.robotics import FetchPushEnv
from tensorboardX import SummaryWriter
from apscheduler.schedulers.background import BackgroundScheduler
import csv, datetime, sys, threading as T, re
# from PourCupsEnv import PourCupsEnv

import rlkit.torch.pytorch_util as ptu
from rlkit.data_management.env_replay_buffer import EnvReplayBuffer
from rlkit.envs.wrappers import NormalizedBoxEnv
from rlkit.launchers.launcher_util import setup_logger
from rlkit.samplers.data_collector import MdpPathCollector
from rlkit.torch.sac.policies import TanhGaussianPolicy, MakeDeterministic
from rlkit.torch.sac.sac import SACTrainer
from rlkit.torch.networks import FlattenMlp
from rlkit.torch.torch_rl_algorithm import TorchBatchRLAlgorithm
from CooperativeSACImplementation import *
# from FetchDomain import *
# from CheckpointMazeEnv import *
from SlideBlockEnv import SlideBlock
from PandaReacher import ReacherEnv
from PandaPegTask import PegEnv
from CartesianPandav2 import CartPanda
from Reacher import GridReacher
# from PushDomain import *

# from PointGoalNavigationEnv_v0 import *



# from SwingUpEnv import *



def experiment(variant,env = "Hopper"):
    if env == "HalfCheetah":
        expl_env = NormalizedBoxEnv(HalfCheetahEnv())
        eval_env = NormalizedBoxEnv(HalfCheetahEnv())
    elif env == "Hopper":
        expl_env = NormalizedBoxEnv(HopperEnv())
        eval_env = NormalizedBoxEnv(HopperEnv())
    elif env == "Walker":
        expl_env = NormalizedBoxEnv(Walker2dEnv())
        eval_env = NormalizedBoxEnv(Walker2dEnv())
    elif env == "Ant":
        expl_env = NormalizedBoxEnv(AntEnv())
        eval_env = NormalizedBoxEnv(AntEnv())
    elif env == "Humanoid":
        expl_env = NormalizedBoxEnv(HumanoidEnv())
        eval_env = NormalizedBoxEnv(HumanoidEnv())
    elif env == "Cups":
        expl_env = NormalizedBoxEnv(PourCupsEnv())
        eval_env = expl_env
    elif env == "Swingup":
        expl_env = NormalizedBoxEnv(SwingUpEnv(reward_type=variant["reward_type"]))
        eval_env = NormalizedBoxEnv(SwingUpEnv(reward_type="sparse"))

    elif env == "Maze":
        if variant['mode'] == 'singleAgent':
            singleAgent = True
        else:
            singleAgent = False
        explVariant = variant.copy()
        explVariant['randomStartsAcrossMap'] = 1
        expl_env = NormalizedBoxEnv(CheckpointMaze(**explVariant))
        eval_env = NormalizedBoxEnv(CheckpointMaze(**variant))
    elif env == "Slide":
        expl_env = NormalizedBoxEnv(SlideBlock(**variant))
        eval_env = NormalizedBoxEnv(SlideBlock(**variant))
    elif env == "PandaPeg":
        expl_env = NormalizedBoxEnv(PegEnv())
        eval_env = expl_env
    elif env == "PointGoalNav":
        expl_env = NormalizedBoxEnv(PointGoalNavigation(reward_type="dense"))
        eval_env = NormalizedBoxEnv(PointGoalNavigation())
    elif env == "Fetch":
        if variant['mode'] == 'singleAgent':
            singleAgent = True
        else:
            singleAgent = False
        expl_env = NormalizedBoxEnv(FetchDomain(reward_type="distance",
                                                singleAgent=singleAgent))
        eval_env = NormalizedBoxEnv(FetchDomain(singleAgent=singleAgent))
    elif env == "Push":
        if variant['mode'] == 'singleAgent':
            singleAgent = True
        else:
            singleAgent = False
        expl_env = NormalizedBoxEnv(PushDomain(reward_type=variant['rewardType'] if 'rewardType' in variant else 'distance',
                                                singleAgent=singleAgent))
        eval_env = NormalizedBoxEnv(PushDomain(singleAgent=singleAgent))

    elif env == "Reach":
        expl_env = NormalizedBoxEnv(ReacherEnv(**variant))
        eval_env = expl_env

    elif env == "GridReach":
        expl_env = NormalizedBoxEnv(GridReacher(**variant['env_kwargs']))
        eval_env = NormalizedBoxEnv(GridReacher(**variant['env_kwargs']))
    
    elif env == "CartReach":
        expl_env = NormalizedBoxEnv(CartPanda())
        eval_env = expl_env
    
    if variant['mode'] == 'singleAgent':
        obs_dim = expl_env.observation_space.low.size
        action_dim = eval_env.action_space.low.size

        M = variant['layer_size']
        qf1 = FlattenMlp(
            input_size=obs_dim + action_dim,
            output_size=1,
            hidden_sizes=[M, M],
        )
        qf2 = FlattenMlp(
            input_size=obs_dim + action_dim,
            output_size=1,
            hidden_sizes=[M, M],
        )
        target_qf1 = FlattenMlp(
            input_size=obs_dim + action_dim,
            output_size=1,
            hidden_sizes=[M, M],
        )
        target_qf2 = FlattenMlp(
            input_size=obs_dim + action_dim,
            output_size=1,
            hidden_sizes=[M, M],
        )
        policy = TanhGaussianPolicy(
            obs_dim=obs_dim,
            action_dim=action_dim,
            hidden_sizes=[M, M],
        )
        eval_policy = MakeDeterministic(policy)
            
        replay_buffer = EnvReplayBuffer(
            variant['replay_buffer_size'],
            expl_env,
        )

        trainer = SACTrainer(
            env=eval_env,
            policy=policy,
            qf1=qf1,
            qf2=qf2,
            target_qf1=target_qf1,
            target_qf2=target_qf2,
            **variant['trainer_kwargs']
        )
        
        eval_path_collector = MdpPathCollector(
            eval_env,
            eval_policy,
        )
        expl_path_collector = MdpPathCollector(
            expl_env,
            policy,
        )
        algorithm = TorchBatchRLAlgorithm(
            trainer=trainer,
            exploration_env=expl_env,
            evaluation_env=eval_env,
            exploration_data_collector=expl_path_collector,
            evaluation_data_collector=eval_path_collector,
            replay_buffer=replay_buffer,
            **variant['algorithm_kwargs']
        )
    else:
        try:
            nSubtasks = eval_env.nSubtasks
        except:
            nSubtasks = 1
        obs_dim = expl_env.observation_space.low.size
        action_dim = eval_env.action_space.low.size
        _,_,_,info = eval_env.step(np.zeros(action_dim))
        env_info_sizes = {}
        for key in info:
            try:
                env_info_sizes[key] = len(info[key])
            except:
                env_info_sizes[key] = 1

        M = variant['layer_size']
        trainers = []
        replay_buffers = []
        policies = []
        eval_policies = []
        for _ in range(nSubtasks):
            qf1 = FlattenMlp(
                input_size=obs_dim + action_dim,
                output_size=1,
                hidden_sizes=[M, M],
            )
            qf2 = FlattenMlp(
                input_size=obs_dim + action_dim,
                output_size=1,
                hidden_sizes=[M, M],
            )
            target_qf1 = FlattenMlp(
                input_size=obs_dim + action_dim,
                output_size=1,
                hidden_sizes=[M, M],
            )
            target_qf2 = FlattenMlp(
                input_size=obs_dim + action_dim,
                output_size=1,
                hidden_sizes=[M, M],
            )
            policy = TanhGaussianPolicy(
                obs_dim=obs_dim,
                action_dim=action_dim,
                hidden_sizes=[M, M],
            )
            policies.append(policy)
            eval_policies.append(MakeDeterministic(policy))
            
            replay_buffer = EnvReplayBuffer(
                int(variant['replay_buffer_size']),
                expl_env,
                env_info_sizes=env_info_sizes
            )
            replay_buffers.append(replay_buffer)

            trainer = CooperativeSACTrainer(
                env=eval_env,
                policy=policy,
                qf1=qf1,
                qf2=qf2,
                target_qf1=target_qf1,
                target_qf2=target_qf2,
                cooperative=1 if variant['mode'] == 'cooperative' else 0,
                **variant['trainer_kwargs']
            )
            trainers.append(trainer)

        if variant['mode'] == 'cooperative':
            cRatio = variant['trainer_kwargs']['cooperativeRatio']
            if isinstance(cRatio,str) and  '-' in cRatio:
                indCRatio = True
                cRatios = re.split('-',variant['trainer_kwargs']['cooperativeRatio'])
            else:
                indCRatio = False
            for n, trainer in enumerate(trainers):
                if n+1 < nSubtasks:
                    trainer.next_qf1 = trainers[n+1].qf1
                    trainer.next_qf2 = trainers[n+1].qf2
                    if indCRatio:
                        trainer.cooperativeRatio = float(cRatios[n])
                if n >= 1:
                    trainer.prev_pol = trainers[n-1].policy
        
        eval_path_collector = CooperativeMdpPathCollector(
            eval_env,
            eval_policies,
            render=variant['renderEval']
        )
        expl_path_collector = CooperativeMdpPathCollector(
            expl_env,
            policies,
        )
        algorithm = CooperativeTorchBatchRLAlgorithm(
            trainers=trainers,
            exploration_env=expl_env,
            evaluation_env=eval_env,
            exploration_data_collector=expl_path_collector,
            evaluation_data_collector=eval_path_collector,
            replay_buffers=replay_buffers,
            mode = variant['mode'],
            **variant['algorithm_kwargs']
        )
    algorithm.to(ptu.device)
    algorithm.train()


class Writer():
    def __init__(self,location):
        self.location = location
        self.writer = SummaryWriter(logdir=location,comment="progress")
        self.lastRow = 0
        self.tagDict = {}

    def update(self):
        time = 'Epoch'
        try:
            if self.lastRow == 0:
                with open("{}/progress.csv".format(location),'r',newline='') as f:
                    reader = csv.reader(f, delimiter=',')
                    for n, row in enumerate(reader):
                        if n == 0:
                            for m, elem in enumerate(row):
                                if elem == time:
                                    self.timeTag = m
                                else:
                                    self.tagDict[elem] = m
                        else:
                            for tag in self.tagDict:
                                self.writer.add_scalar(tag,float(row[self.tagDict[tag]]),int(row[self.timeTag]))
                        self.lastRow = n
            else:
                with open("{}/progress.csv".format(location),'r',newline='') as f:
                    reader = list(csv.reader(f, delimiter=','))
                    if (len(reader)-1) > self.lastRow:
                        for n in range(self.lastRow+1,len(reader)):
                            row = reader[n]
                            for tag in self.tagDict:
                                self.writer.add_scalar(tag,float(row[self.tagDict[tag]]),int(row[self.timeTag]))
                        self.lastRow = len(reader)-1
                        print("Updated Tensorboard File - up to epoch {}".format(row[self.timeTag]))
                    else:
                        print("Nothing to update")
        except:
            pass

    def writeVariant(self,variant):
        for key in variant:
            if type(variant[key]) is dict:
                newDict = variant[key]
                for deeperKey in newDict:
                    self.writer.add_text("{}/{}".format(key,deeperKey),str(newDict[deeperKey]),0)
            else:
                self.writer.add_text(key,str(variant[key]),0)
        
                        


    
                
def arg(tag, default):
    if tag in sys.argv:
        val = type(default)((sys.argv[sys.argv.index(tag)+1]))
    else:
        val = default
    return val

def addToVariant(variant):
    print(sys.argv)
    for tag in sys.argv:
        if tag[:6] == "--add:":
            for t in [int, float, str]:
                try: 
                    var = t(sys.argv[sys.argv.index(tag)+1])
                    break
                except:
                    pass
            print(var)
            if tag[6:10] == "akw:":
                variant['algorithm_kwargs'][tag[10:]] = var
            elif tag[6:10] == "tkw:":
                variant['trainer_kwargs'][tag[10:]] = var
            elif tag[6:10] == "ekw:":
                variant['env_kwargs'][tag[10:]] = var
            else:
                variant[tag[6:]] = var
    return variant




if __name__ == "__main__":
    # noinspection PyTypeChecker
    variant = dict(
        algorithm="SAC",
        version="normal",
        layer_size=256,
        replay_buffer_size=int(1E6),
        renderEval = 0,
        algorithm_kwargs=dict(
            num_epochs=3000,
            num_eval_steps_per_epoch=5000,
            num_trains_per_train_loop=1000,
            num_expl_steps_per_train_loop=5000,
            min_num_steps_before_training=0,
            max_path_length=1000,
            batch_size=256,
        ),
        trainer_kwargs=dict(
            discount=0.99,
            soft_target_tau=5e-3,
            target_update_period=1,
            policy_lr=3E-4,
            qf_lr=3E-4,
            reward_scale=1,
            use_automatic_entropy_tuning=True,
        ),
        env_kwargs=dict()
    )
    time = datetime.datetime.now()
    date = '{}-{}={}:{}:{}'.format(time.month, time.day, time.hour, time.minute, time.second)
    name = arg('--name','')
    env = arg('--env', 'Maze')  #'Hopper'
    folder = arg('--folder',env)
    mode = arg('--mode',0)
    modes = ['cooperative','uncooperative','singleAgent']
    variant['algorithm_kwargs']['num_epochs'] = arg('--nEpochs',variant['algorithm_kwargs']['num_epochs'])
      
    variant['mode'] = modes[mode]
    if mode == 0:
        variant['trainer_kwargs']['cooperativeRatio'] = 0.5

    variant = addToVariant(variant) 
    print(variant)

    name = name if name != '' else date
    location = "./Runs/{}/[{}]".format(folder,name)
    writer = Writer(location)
    writer.writeVariant(variant)

    sch = BackgroundScheduler()
    sch.start()
    sch.add_job(writer.update,'interval', seconds=20)

    setup_logger(env, variant=variant,log_dir=location)
    experiment(variant,env)

    sch.shutdown()
    # experimentThread = T.Thread(target=experiment,args = (variant,env))
    # experimentThread.start()

    # while experimentThread.is_alive():
    #     writer.update()
    # experimentThread.join()
    

import torch, sys
from CooperativeSACImplementation import rollout as coopRollout
from rlkit.samplers.rollout_functions import rollout as singleRollout
from rlkit.envs.wrappers import NormalizedBoxEnv
from CheckpointMazeEnv import *
import json
from visualiseRuns import visualiseData
from copy import deepcopy as copy
from PushDomain import *
from SlideBlockEnv import SlideBlock
from PandaReacher import ReacherEnv
# from CartesianPandav2 import CartPanda


def arg(tag, default):
    if tag in sys.argv:
        val = type(default)((sys.argv[sys.argv.index(tag)+1]))
    else:
        val = default
    return val

def printDict(d,startStr = ""):
    for elem in d:
        if isinstance(d[elem],dict):
            print("{}{}:".format(startStr,elem))
            printDict(d[elem],startStr=startStr+"    ")
        else:
            print("{}{}:{}".format(startStr,elem,d[elem]))


# path = arg("--path","Runs/OldMaze/[2-26=11:23:28]")
path = arg("--path","../../DataAnalysis/Data/modeSweep/mode:1-checks:3-iter:3-[4-11=20:20:10]")
it = arg("--iter",-100)
runs = arg("--nRuns",20)
render = arg("--render",0)
showFOV = arg("--showFOV",1)
visualise = arg("--viz",0)
envName = arg("--env","Maze")

if it > 0:
    try:
        data = torch.load("{}/itr_{}.pkl".format(path,it))
    except:
        print("That iteration doesn't exist")
else:
    data = torch.load("{}/params.pkl".format(path))

policies = data['exploration/policy']


with open("{}/variant.json".format(path),'r') as f:
    variant = json.load(f)
printDict(variant)
variant['randomStartsAcrossMap'] = 0
if envName == "Maze":
    env = NormalizedBoxEnv(CheckpointMaze(**variant))
elif envName == "Push":
    env = NormalizedBoxEnv(PushDomain())
elif envName == "Slide":
    env = NormalizedBoxEnv(SlideBlock())
elif envName == "Reach":
    env = NormalizedBoxEnv(ReacherEnv())
elif envName == "CartReach":
    env = NormalizedBoxEnv(CartPanda(headless=not render))
# if variant['mode'] == 'cooperative':
#     QFs = [data['trainer{}/qf1'.format(str(n))] for n in range(variant['checkpoints'])]
# QF = data['trainer0/qf1']
# policy = policies[0]
# env.paintQFunction(QF,policy)
# path_collector = CooperativeMdpPathCollector(
#     env = env,
#     policies = policies,
#     render = True
# )
singleAgent = variant['mode'] == 'singleAgent'
if singleAgent:
    QFs = data['trainer/qf1']
else:
    # QFs = [data['trainer{}/qf1'.format(n)] for n in range(variant['checkpoints'])]
    n = 0
    QFs = []
    while True:
        try:
            QFs.append(data['trainer{}/qf1'.format(n)])
            n += 1
        except:
            break

frames = []

visualisingData = []
for ep in range(runs):
    visualisingData.append([])
    o = env.reset()
    if singleAgent:
        agent = policies
        QF = QFs
    else:
        agent = policies[0]
        QF = QFs[0]
    t = 0
    print("Episode {}".format(ep))
    totalRew = 0
    d = False
    
    while t < 1000 and not d:
        dataPoint = {'o': o}
        a,_ = agent.get_action(o)
        n_o,r,d,info = env.step(a)
        totalRew += r
        if render: 
            if envName == 'Maze':
                env.render(showFOV=showFOV)
            else:
                env.render()
        if not singleAgent:
            agent = policies[info['subtask']]
            QF = QFs[info['subtask']]
        dataPoint['subtask'] = info['subtask']
        # env.render()
        
        if visualise:
            string = 'timestep: {} - subtask: {}'.format(t,info['subtask'])
            for n,Q in enumerate(QFs):
                val = Q(torch.unsqueeze(torch.cat((torch.Tensor(o),torch.Tensor(a))),0))
                strVal = '%.3e' % float(val)
                string += ' - QF{}: {}'.format(n,strVal)
                dataPoint['QF{}'.format(n)] = copy(val.detach().data.numpy())
            # print(string)
        

        o = n_o
        t += 1
        if visualise:
            visualisingData[-1].append(copy(dataPoint))
    
    print("---------------\n Average reward: {}\n Length: {}\n---------------\n".format(totalRew/t,t))

if visualise:
    visualiseData(env,visualisingData)


